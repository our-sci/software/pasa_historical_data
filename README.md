# PASA Historical Data Encoding

## Objectives

* Express PASA's historical dataset as FarmOS entities, accordint to the Common Farm Conventions.
* Control the format, coherency and schema compliance for each submission.
* Upload curated data into FarmOS instances.
* Mirror said instances in a mongo mirror.
* Build a data pipeline that gets data from the mirror provinding an interface and a format suitable to data analysis.

## Contents of this repo

### `upload_process`

* Based on the `farm_os_entitiy_uploader` Node library.
* Requires the *farm dataset*, which is not uploaded, to be served into the `./raw_data` folder. 
* Requires an *environment variable*, *FARMOS_KEY* in order to communicate with the **FarmOS Aggregator**.
