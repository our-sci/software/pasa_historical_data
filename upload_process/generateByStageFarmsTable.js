// Serena Xu provided a detailed CSV file listing all production farms and whether they belong to the first batch of farms that won't be accessed by farmers or the second batch of farms whose owners are expected to visit and edit the data. These stages are respectively called "first batch" and "main batch".
// Two previous stages are added: The "demo" stage is meant for final testing and review and includes 5 demo farms that mirror a diverse selection of actual farms and is accessed by PASA personnel for review and control. The "test" stage is the first stage of submission overall, meant for freely testing any procedure as a first stage during development.
// In November 2024, a third production batch of farmers added for the 2025 study is added.

const fs = require("fs");
const papa = require("papaparse");
const {farmDomainDict} = require("./submitters/artifacts");

let firstBatchFarms = [
    'PA00256', 'PA00194',
    'MD00108', 'PA00195',
    'MD00109', 'PA00065',
    'PA00001', 'PA00257',
    'PA00260', 'PA00013',
    'PA00197', 'PA00198',
    'PA00199', 'PA00210',
    'PA00201', 'PA00258'
];

const productionFarmsOriginal = papa.parse(fs.readFileSync( `${__dirname}/../raw_data/production_farms.csv`, "utf8" ), {header: true})
  .data
  .map( row => {
      let isFirstRound = firstBatchFarms.includes(row['Account: Research ID'] );
      row.production = true;
      row.stage = isFirstRound ? "production_first_batch" : "production_main_batch";
      return row;
  })
    ;

const originalListFarms = productionFarmsOriginal.map(d => d['Account: Research ID']);

const productionFarms = papa.parse(fs.readFileSync(`${__dirname}/../raw_data/production_farms_corrected.csv`, "utf8"), { header: true })
    .data
    .map(row => {
        row.production = true;
        if (!originalListFarms.includes(row['Account: Research ID'])) {
            row.stage = "production_maryland_batch";
        } else {
            let isFirstRound = firstBatchFarms.includes(row['Account: Research ID']);
            row.stage = isFirstRound ? "production_first_batch" : "production_main_batch";
        };
        return row;
    })
    ;
const productionFarms2025Fields = papa.parse(fs.readFileSync(`${__dirname}/../raw_data/geographic_data/summary_of_new_2025_fields.csv`, "utf8"), { header: true })
    .data;

const productionFarms2025 = papa.parse(fs.readFileSync(`${__dirname}/../raw_data/third_version_tables/batch_nov_24_farm_domains.csv`, "utf8"), { header: true })
    .data
    .filter(d => d.farmDomain)
    .map(farm => {
        let fieldEntry = productionFarms2025Fields.find(d => d.farm_name == farm.farm_name);
        let farm_id = fieldEntry.field_id.split("_")[0];
        farm.production = true;
        farm.stage = "production_main_batch";
        farm['farmOS ID'] = farm.farmDomain;
        farm['Account: Research ID'] = farm_id;
        farm['Account: Account Name'] = farm.farm_name;
          return farm;
      })
    .filter(d => !productionFarms.find(f => d['Account: Research ID'] == f['Account: Research ID']))
    ;

const new2025FieldsPreexistingFarm = productionFarms2025Fields
    .filter(d => productionFarms.find(f => d.field_id.split("_")[0] == f['Account: Research ID']))
;
fs.writeFileSync(`${__dirname}/../raw_data/geographic_data/newFieldPreexistingFarm2025.json`, JSON.stringify(new2025FieldsPreexistingFarm ));
// help identift the batch later
let nov2024FarmIDs = productionFarms2025.map(d => d['Account: Research ID']);
fs.writeFileSync(`${__dirname}/../raw_data/geographic_data/nov2024FieldIDs.json`, JSON.stringify(nov2024FarmIDs));

const testingFarms = [
    {
        'farmOS ID': "ourscitest.farmos.net",
        stage: "test_farm",
        production: false,
        'Account: Account Name': "Test farm"
    }
];

const demoFarms = Object.keys( farmDomainDict )
      .map( farmName => {
          let entry = productionFarms.find( entry => entry['Account: Account Name'] == farmName ); 
          if (entry) {
              let output = {};
              Object.assign(entry, output);
              output['farmOS ID'] = farmDomainDict[farmName];
              output.stage = "demo_farms";
              output['Account: Research ID'] = entry['Account: Research ID'];
              output['Account: Account Name'] = farmName;
              output.production = false;
              return output;
          } else {
              console.log(farmName);
              throw new Error(`No entry for ${farmName}`);
          };
      } )
;

const table = [
    ... productionFarms,
    ... testingFarms,
    ... demoFarms,
    ... productionFarms2025,
]
      .map( farm => {
          let output = {};
          output.farmDomain = farm['farmOS ID'];
          output.farm_id = farm['Account: Research ID'];
          output.stage = farm.stage;
          output.production = farm.production;
          output.farm_name = farm['Account: Account Name'];
          return output;
      } )
;

fs.writeFileSync( "./submitters/farmsStageAssignment.json", JSON.stringify(table) );
