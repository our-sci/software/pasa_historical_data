// This script should read data in the appropirate CSV and prepare it to be submitted by the submitters.
const papa = require('papaparse');
const fs = require('fs');
const {
    farmDomainDict
} = require('../submitters/artifacts.js');
const grazing_encoding = require('../encodings/grazing_data_encoding');
const { parsePasaDate, addIndexToRepeatedNames } = require("./preparation_utilities.js");

const farmStagesTable = JSON.parse(fs.readFileSync("../submitters/farmsStageAssignment.json"));

let sourceData = papa.parse(
    fs.readFileSync(`${__dirname}/../../raw_data/third_version_tables/grazing_records_2024-09-03.csv`, 'utf8'),
    {
        header:true,
        dynamicTyping:true,
        transform: ( value ) => { return value == 'NA' ? undefined : value; }
    } )
    ;
let sourceData2023 = papa.parse(
    fs.readFileSync(`${__dirname}/../../raw_data/third_version_tables/data_2023/Animals 2023 - bullet field names.csv`, 'utf8'),
    {
        header:true,
        dynamicTyping:true,
        transform: ( value ) => { return value == 'NA' ? undefined : value; }
    } )
;

// needed to estimate planting / amendment association using dates
let plantingsSourceData = JSON.parse( fs.readFileSync( "../operation_registers/finalPlantingData.json" ) )
    .filter( d => d.planting_date )
    .map( row => {
        // this is already processed data and does not require the addition of a timezone
        row.planting_date = new Date(row.planting_date);
        row.term_date = new Date(row.term_date);
        return row;
    } )
;

// needed to obtain field names, using when naming logs (we now do that when preparing data)
let fieldsFinalData = JSON.parse( fs.readFileSync( `${__dirname}/../operation_registers/finalFieldsData.json` ) );

let fullDataset = [ ... sourceData.data, ... sourceData2023.data ];

// encoding plan: one animal asset for each herd, number as a quantity on the log, which will be an activity log.

let grazingRepeated = fullDataset
    .filter( d => d.graze_start_date )
    .map( row => {
        // add a timezone to dates to avoid ambiguity when parsing 00 in UTC as previous day.
        row.graze_start_date = parsePasaDate({ string: row.graze_start_date });
        row.graze_end_date = parsePasaDate({ string: row.graze_end_date });
        let associatedPlanting = plantingsSourceData.filter( plantingRow => {
            // The criteria for planting association currently is: amendment happened BEFORE seeding, seeding is the first one after the amendment (applied later, when sorting).
            return plantingRow.term_date >= row.graze_start_date &&
                plantingRow.planting_date <= row.graze_start_date &&
                plantingRow.field_id == row.field_id
            ;
        } );
        if ( associatedPlanting.length == 1 ) {
            row.plantings_amount = associatedPlanting.length;
            row.planting = associatedPlanting[0];
            row.paired = true;
        } else if (associatedPlanting.length == 0) {
            row.paired = false;
        } else if ( associatedPlanting.length > 1 ) {
            row.planting = associatedPlanting
                .sort( (a,b) => b.planting_date.getTime() - a.planting_date.getTime() )
            [0]
            ;
            row.plantings_amount = associatedPlanting.length;
            row.planting_dates = associatedPlanting.map( d => d.term_date );
            row.paired = true;
        }
        ;
        row.season = row.survey_year;
        row.planting_name = row.planting?.plantingName;


        let fieldEntry = fieldsFinalData.find(f => f.field_id == row.field_id);
        if (!fieldEntry && !row.field_name) {
            row.field_name = "MISSING";
        } else {
            row.field_name = fieldEntry.field_name;
            row.farm_name = fieldEntry.farm_name;
            row.farm_id = fieldEntry.farm_id;
        };

        // create log name
        row.grazing_start_log_name = grazing_encoding.grazingStartNameGenerator({ field: row.field_name, date: row.graze_start_date });
        row.grazing_ending_log_name = grazing_encoding.grazingEndNameGenerator({ field: row.field_name, date: row.graze_end_date });

        // register length in days
        row.graze_days = (row.graze_end_date - row.graze_start_date) / (1000 * 3600 * 24) + 1;

        // if there's no "area percentage" but the unit in "area" is "percent", populate area percent
        if (!row.area_percent && ["percent", "percentage", "%"].includes( row.area_units )) {
            row.area_percent = row.area;
        };
        // If there's an area percentage, we will delete the other area quantities
        if (row.area_percent) {
            delete row.area;
            delete row.area_units;
        };

        // structure notes as a unique JSON document.
        if (row.farmer_notes?.length > 0 || row.research_notes?.length > 0 || row.farmer_animal?.length > 0) {
            let notes = {
                format: "default",
                value: "Observations for this grazing event:\n"
            };

            if (row.farmer_notes?.length) {
                row.farmer_notes = row.farmer_notes.replace(/\s*$/, ".");
                notes.value = notes.value.concat(`<blockquote> <bold>Farmer notes:</bold> ${row.farmer_notes} </blockquote>`);
            };
            if ( row.farmer_animal ) {
                row.farmer_animal = row.farmer_animal.replace(/\s*$/, "");
                notes.value = notes.value.concat(`<blockquote> <bold>Farmer animal name:</bold> ${row.farmer_animal } </blockquote>` );
            };
            if ( row.research_notes?.length ) {
                row.research_notes = row.research_notes.replace(/\s*$/, ".");
                notes.value = notes.value.concat(`<blockquote> <bold>Research notes:</bold> ${row.research_notes } </blockquote>` );
            };
            row.notes = notes;
        };
        row.animal_asset_name = `${row.animal_type?.replace("_", " ")} herd`;
        return row;
    })
    ;

let grazing = addIndexToRepeatedNames({ sourceArray: grazingRepeated, pkeyAttribute: "grazing_record_pkey", logNameAttribute: "grazing_start_log_name" });

fs.writeFileSync( `${__dirname}/../operation_registers/finalGrazingData.json`, JSON.stringify(grazing) );
fs.writeFileSync( `${__dirname}/../operation_registers/finalGrazingData.csv`, papa.unparse(grazing) );
