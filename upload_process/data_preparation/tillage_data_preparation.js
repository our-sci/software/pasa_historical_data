// This script should read data in the appropirate CSV and prepare it to be submitted by the submitters.
const papa = require('papaparse');
const fs = require('fs');
const {disturbNameGenerator} = require('../encodings/tillage_data_encoding');
const { parsePasaDate, addIndexToRepeatedNames } = require("./preparation_utilities.js");

const farmStagesTable = JSON.parse( fs.readFileSync("../submitters/farmsStageAssignment.json") );

let sourceData = papa.parse(
    fs.readFileSync( `${__dirname}/../../raw_data/third_version_tables/disturbance_records_2024-09-03.csv` , 'utf8' ),
    {
        header:true,
        dynamicTyping:true,
        transform: ( value ) => { return value == 'NA' ? undefined : value; }
    } )
    ;
let sourceData2023 = papa.parse(
    fs.readFileSync(`${__dirname}/../../raw_data/third_version_tables/data_2023/Disturbance 2023 - bullet field names.csv` , 'utf8' ),
    {
        header:true,
        dynamicTyping:true,
        transform: ( value ) => { return value == 'NA' ? undefined : value; }
    } )
;

let fullDataset = [ ... sourceData.data, ... sourceData2023.data ];

// needed to estimte planting / amendment association using dates
let plantingsSourceData = JSON.parse( fs.readFileSync( `${__dirname}/../operation_registers/finalPlantingData.json` ) )
    .filter( d => d.planting_date )
    .map( row => {
        row.planting_date = new Date(row.planting_date);
        row.term_date = new Date(row.term_date);
        return row;
    } )
;
// needed to obtain field names, using when naming logs (we now do that when preparing data)
let fieldsFinalData = JSON.parse( fs.readFileSync( `${__dirname}/../operation_registers/finalFieldsData.json` ) );


let tillageRepeated = fullDataset
    .filter( d => d.field_id )
    // omit empty entries
    .filter( d => d.implement_type != 'none' )
    .map( ( row, index ) => {
        // TODO assuming disturb_date
        row.planting_name= row.planting?.plantingName;
        row.farm_id= row.field_id.split("_")[0];
        row.farmDomain = farmStagesTable.find( farm => farm.farm_id == row.farm_id ).farmDomain;
        let dateSource = row.disturb_date ? row.disturb_date : `01-01-${ row.survey_year }`;
        row.disturb_date = parsePasaDate({string: dateSource});
        // Associate with a planting using the time range between seeding and harvest.
        let associatedPlanting = plantingsSourceData.filter( plantingRow => {
            // The criteria for planting association currently is: amendment happened BEFORE seeding, seeding is the first one after the amendment (applied later, when sorting).
            return plantingRow.planting_date  < row.disturb_date &&
                plantingRow.term_date  >= row.disturb_date &&
                plantingRow.field_id == row.field_id
            ;
        } );
        if ( associatedPlanting.length == 1 ) {
            row.plantings_amount = associatedPlanting.length;
            row.planting = associatedPlanting.sort( (a,b) => b.disturb_date - a.disturb_date )[0];
            row.paired = true;
        } else if (associatedPlanting.length == 0) {
            row.paired = false;
        } else if ( associatedPlanting.length > 1 ) {
            row.planting = associatedPlanting
                .sort( (a,b) => b.planting_date.getTime() - a.planting_date.getTime() )
            [0]
            ;
            row.plantings_amount = associatedPlanting.length;
            row.paired = true;
        }
        ;

        // if there's no "area percentage" but the unit in "area" is "percent", populate area percent
        if (!row.area_percent && ["percent", "percentage", "%"].includes( row.area_units )) {
            row.area_percent = row.area;
        };
        // If there's an area percentage, we will delete the other area quantities
        if (row.area_percent) {
            delete row.area;
            delete row.area_units;
        };


        if (row.planting) {
            row.farm_name = row.planting.farm_name;
        };
        let fieldEntry = fieldsFinalData.find(f => f.field_id == row.field_id);
        if (!fieldEntry && !row.field_name) {
            row.field_name = "MISSING";
        } else {
            row.field_name = fieldEntry.field_name;
        };

            row.disturb_log_name = disturbNameGenerator({ activity: row.implement_type, field: row.field_name, date: row.disturb_date });

        // structure notes as a unique JSON document.
        if (row.farmer_notes?.length > 0 || row.research_notes?.length > 0 || row.farmer_implement?.length > 0) {
            let notes = {
                format: "default",
                value: "Observations for this soil disturbing event:\n"
            };
            if (row.farmer_notes?.length) {
                notes.value = notes.value.concat(`<blockquote> <bold>Farmer notes:</bold> ${row.farmer_notes} </blockquote>`);
            };
            if ( row.farmer_implement ) {
                notes.value = notes.value.concat( `<blockquote> <bold>Farmer implement name:</bold> ${ row.farmer_implement } </blockquote>` );
            };
            if ( row.research_notes?.length ) {
                notes.value = notes.value.concat( `<blockquote> <bold>Research notes:</bold> ${ row.research_notes } </blockquote>` );
            };
            row.notes = notes;
        };
        delete row.research_notes;
        delete row.farmer_notes;
        return row;
    })
;

let tillage = addIndexToRepeatedNames({sourceArray: tillageRepeated, pkeyAttribute: "disturb_record_pkey", logNameAttribute: "disturb_log_name"});

fs.writeFileSync( `${__dirname}/../operation_registers/finalTillageData.json`, JSON.stringify(tillage) );
fs.writeFileSync( `${__dirname}/../operation_registers/finalTillageData.csv`, papa.unparse(tillage) );
