const pensilvaniaTimestampConstant = "T12:00:00.000-04:00";

// add a timezone to dates to avoid ambiguity when parsing 00 in UTC as previous day.
exports.parsePasaDate = function({string}) {
    let output;
    if (string) {
        output = new Date(string.concat(pensilvaniaTimestampConstant));
    };
    return output;
};

exports.addIndexToRepeatedNames = function addIndexToRepeatedNames({ sourceArray, pkeyAttribute, logNameAttribute }) {
    let dataArray = structuredClone(sourceArray);
    let existingNames = Array.from( new Set( dataArray.map( d => d[logNameAttribute] ) ) );

    let nameIterations = existingNames
        .forEach( name => {
            let iterations = dataArray.filter( d => d[logNameAttribute] == name );
            let output = {
                ids: iterations.map( d => d.pkeyAttribute ),
                ids_amount: iterations.length,
                name: name,
            };
            if (output.ids_amount > 1) {
                iterations.forEach( (d,i) => d[logNameAttribute] = `${d[logNameAttribute]} (${i+1} of ${output.ids_amount})` );
            };
            return output;
        } )
    ;
    return dataArray;
};
