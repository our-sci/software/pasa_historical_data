// This script should read data in the appropirate CSV and prepare it to be submitted by the submitters.
const papa = require('papaparse');
const fs = require('fs');
const {
    farmDomainDict
} = require('../submitters/artifacts.js');
const plantings_encoding = require('../encodings/plantings_data_encoding');
const { parsePasaDate, addIndexToRepeatedNames } = require("./preparation_utilities.js");


let fieldsSource = papa.parse(
    fs.readFileSync( `${__dirname}/../../raw_data/second_version_tables/shbs_enrolled_fields.2024-07-02.csv`, 'utf8' ),
    {
        header:true,
        dynamicTyping:true,
        transform: ( value ) => { return value == 'NA' ? undefined : value; }
    } )
    .data
;

let plantingsSource = `${__dirname}/../../raw_data/third_version_tables/planting_records_collapsed_2024-09-03.csv`;
let plantings2023Source = `${__dirname}/../../raw_data/third_version_tables/data_2023/Planting2023 - bullet field names.csv`;

let plantingsData = papa.parse(
    fs.readFileSync(plantingsSource, 'utf8'),
    {
        header: true,
        dynamicTyping: true,
        transform: (value) => { return value == 'NA' ? undefined : value; }
    })
    ;

let plantingsData2023 = papa.parse(
    fs.readFileSync(plantings2023Source, 'utf8'),
    {
        header: true,
        dynamicTyping: true,
        transform: (value) => { return value == 'NA' ? undefined : value; }
    })
    ;

let fullDataset = [...plantingsData.data, plantingsData2023.data];

let allPlantingsRepeated = fullDataset 
    .filter(d => d.field_id)
    // pair with farm names
    .map(plantingRow => {
        let output = structuredClone(plantingRow);
        // get field data from enrolled fields
        let fieldEntry = fieldsSource.find(field => field.field_id == output.field_id);
        if (fieldEntry) {
            output.farm_id = fieldEntry.farm_id;
            output.farm_name = fieldEntry.farm_name;
            output.field_name = fieldEntry.field_name;
        }
        let season = plantingRow.planting_date?.match(/20[0-9]{2}/);
        if (season) {
            output.season = season[0];
        };
        return output;
    } )
    .map( ( plantingRow, index ) => {
        // format dates as appropriate types
        plantingRow.planting_date = parsePasaDate({ string: plantingRow.planting_date });
        plantingRow.term_date = parsePasaDate({ string: plantingRow.term_date });
        plantingRow.harvest_date = parsePasaDate( { string: plantingRow.harvest_date } );
        // get the names we will use for our plantings and logs when encoding
        plantingRow.plantingName = plantings_encoding.plantingNameGenerator({crop_name: plantingRow.crop_name, planting_date: plantingRow.planting_date, field_name: plantingRow.field_name});
        plantingRow.seeding_log_name = plantings_encoding.seedingNameGenerator({crop_name: plantingRow.crop_name, field_name: plantingRow.field_name,  season: plantingRow.season});
        plantingRow.harvest_log_name = plantings_encoding.harvestNameGenerator({crop_name: plantingRow.crop_name, field_name: plantingRow.field_name,  season: plantingRow.season});
        plantingRow.termination_log_name = plantings_encoding.terminationNameGenerator({crop_name: plantingRow.crop_name, field_name: plantingRow.field_name,  season: plantingRow.season});
        plantingRow.crop_name = plantingRow.crop_name.split(", ").filter( d => d );
        plantingRow.crop_type = plantingRow.crop_type ? `crop type: ${plantingRow.crop_type}` : undefined;

        // remove farmer crop name if equal to crop name.
        if (plantingRow.crop_name.join(', ') == plantingRow.farmer_crop_name?.toLowerCase()) {
            delete plantingRow.farmer_crop_name;
        };

        // if there's no "area percentage" but the unit in "area" is "percent", populate area percent
        if (!plantingRow.area_percent && ["percent", "percentage", "%"].includes( plantingRow.area_units )) {
            plantingRow.area_percent = plantingRow.area;
        };
        // If there's an area percentage, we will delete the other area quantities
        if (plantingRow.area_percent) {
            delete plantingRow.area;
            delete plantingRow.area_units;
        };

        // structure notes as a unique JSON document.
        if (plantingRow.farmer_notes?.length > 0 || plantingRow.research_notes?.length > 0 || plantingRow.farmer_crop_name) {
            let notes = {
                value: "Observations for this planting:\n",
                format: "default"
            };
            if ( plantingRow.farmer_crop_name ) {
                notes.value = notes.value.concat( `<blockquote> <bold>Farmer crop name:</bold> ${ plantingRow.farmer_crop_name } </blockquote>` );
            };
            if ( plantingRow.farmer_notes?.length ) {
                notes.value = notes.value.concat( `<blockquote> <bold>Farmer notes:</bold> ${ plantingRow.farmer_notes } </blockquote>` );
            };
            if (plantingRow.research_notes?.length) {
                notes.value = notes.value.concat(`<blockquote> <bold>Research notes:</bold> ${plantingRow.research_notes} </blockquote>`);
            };
            plantingRow.notes = notes;
        };
        // fix the length of area percentage numbers
        if (plantingRow.area_percent) {
            plantingRow.area_percent = parseFloat(plantingRow.area_percent.toFixed(6));
        };
        delete plantingRow.research_notes;
        delete plantingRow.farmer_notes;

        return plantingRow;
    })
    ;

// example 4127, 4128

let plantings = addIndexToRepeatedNames({ sourceArray: allPlantingsRepeated, pkeyAttribute: "planting_record_pkey", logNameAttribute: "seeding_log_name" });
plantings = addIndexToRepeatedNames({ sourceArray: plantings, pkeyAttribute: "planting_record_pkey", logNameAttribute: "harvest_log_name" });
plantings = addIndexToRepeatedNames({ sourceArray: plantings, pkeyAttribute: "planting_record_pkey", logNameAttribute: "termination_log_name" });
plantings = addIndexToRepeatedNames({ sourceArray: plantings, pkeyAttribute: "planting_record_pkey", logNameAttribute: "seeding_log_name" });

plantings.forEach(plantingRow => {
        // plantings with yield informed had a harvest process, those required a harvest_date instead of a term_date, which will control our entity generation.
        if (plantingRow.yield || plantingRow.yield == 0) {
            plantingRow.harvest_date = plantingRow.term_date;
            delete plantingRow.term_date;
            delete plantingRow.termination_log_name;
        } else {
            delete plantingRow.harvest_date;
            delete plantingRow.harvest_log_name;
        };
} );

// plantings.filter( d => [4127, 4128].includes( d.planting_record_pkey ) )

let plantingNames = new Set(plantings.map(  d => d.plantingName ));
let cropTypesPerPlanting = Array.from( plantingNames ).map( name => { let crop_types = Array.from( new Set( plantings.filter( e => e.plantingName == name ).map( d => d.crop_type ) ) ); return { name: name, cropTypes:crop_types, amount: crop_types.length }; } );

plantings.forEach( plantingRow => {
    let typesEntry = cropTypesPerPlanting.find( e => e.name == plantingRow.plantingName );
    if (typesEntry.amount > 1) {
        plantingRow.crop_type = typesEntry.cropTypes;
    } else {
        plantingRow.crop_type = [ plantingRow.crop_type ];
    }
} );

fs.writeFileSync( `${__dirname}/../operation_registers/finalPlantingData.json`, JSON.stringify(plantings) );
fs.writeFileSync( `${__dirname}/../operation_registers/finalPlantingData.csv`, papa.unparse(plantings) );
