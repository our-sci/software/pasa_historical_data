const papa = require('papaparse');
const fs = require('fs');
const {
    selectedFarms,
    actualToDemoFarmDict,
    farmDomainDict,
    datePattern
} = require('../submitters/artifacts.js');

// we need to generate a farms data frame, using the fields data source.
let fieldsSource = papa.parse(
    fs.readFileSync( `${__dirname}/../../raw_data/second_version_tables/shbs_enrolled_fields.2024-07-02.csv`, 'utf8' ),
    {
        header:true,
        dynamicTyping:true,
        transform: ( value ) => { return value == 'NA' ? undefined : value; }
    } )
    ;

// We don' have a fields table for 2023, but we can obtain it.
let plantings2023Source = `${__dirname}/../../raw_data/third_version_tables/data_2023/Planting2023 - bullet field names.csv`;
let plantings2023 = papa.parse(
    fs.readFileSync(plantings2023Source, 'utf8'),
    {
        header: true,
        dynamicTyping: true,
        transform: (value) => { return value == 'NA' ? undefined : value; }
    })
;

let fields2023 = [];
plantings2023
    .data
    .forEach( row => {
        if (!fields2023.find( d => d.field_id == row.field_id )) {
            let output = {
                field_id: row.field_id,
                field_name: row.field_name,
                farm_id: row.farm_id,
                farm_name: row.farm_name,
                active: true
            };
            fields2023.push(output);
        }
    });
// It seems there are no new fields, so we don't need to merge this or create further field entities.
fields2023.filter( d => !fieldsSource.data.find( f => d.field_id == d.field_id ) );


let allFarmIDs = Array.from( new Set( fieldsSource.data.map( d => d.farm_id ) ) );

let farmsData = allFarmIDs
    .map( farm_id => {
        let entries = fieldsSource.data.filter( d => d.farm_id == farm_id );
        let entry = entries[0];
        let errorName = entries.find( d => d.farm_name != entry.farm_name );
        let errorId = fieldsSource.data.filter( d => d.farm_name == entry.farm_name ).find( d => d.farm_id != farm_id );
        let output = {
            farm_id: farm_id,
            farm_name: entry.farm_name,
            amount_of_fields: entries.length,
            associated_field_ids: entries.map( d => d.field_id ),
            associated_field_name: entries.map( d => d.field_name ),
            errors: {
                has_errors: false
            }
        };
        if ( errorName ) {
            output.errors.has_errors = true;
            output.errors.multiple_names = {
                description: "Multiple farm names are matched to this id",
                details: errorName
            };
        };
        if ( errorId ) {
            output.errors.has_errors = true;
            output.errors.multiple_ids = {
                description: "Multiple farm ids are matched to this farm name",
                details: errorId
            };
        };
        output.farmos_url = farmDomainDict[output.farm_name];
        output.farmDomain = farmDomainDict[output.farm_name];
        return output;
    } )
;

fs.writeFileSync( `${__dirname}/../operation_registers/finalFarmsOrganizationData.json`, JSON.stringify( farmsData ) );

let farmNamesOntology = farmsData.map( d => { return { natural: new String( d.farm_name ), slug: d.farm_name, replaces: [] }; } );
let farmIdsOntology = farmsData.map( d => { return { natural: d.farm_id, slug: d.farm_id, replaces: [] }; } );

fs.writeFileSync( `${__dirname}/../operation_registers/farm_names_ontology.csv`, papa.unparse(farmNamesOntology, {header: true}) );
fs.writeFileSync( `${__dirname}/../operation_registers/farm_ids_ontology.csv`, papa.unparse(farmIdsOntology) );
