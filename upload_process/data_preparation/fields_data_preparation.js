const papa = require('papaparse');
const fs = require('fs');
const {
    selectedFarms,
    actualToDemoFarmDict,
    farmDomainDict,
    datePattern
} = require('../submitters/artifacts.js');
const { geojsonToWKT } = require( "@terraformer/wkt" );


// Get the geometries for all fields, which will be paired when parsing the main field data data source.
let geometrySource = JSON.parse(fs.readFileSync(`${__dirname}/../../raw_data/geographic_data/SHBS_Fields_11-25.geojson`)).
    features
    .map( d => {
        return( { field_id: d.properties.field_id, geometry: geojsonToWKT( d.geometry ) } );
    } )
;

// Main data source.

// let fieldsSourcePreNov2024 = papa.parse(
//     fs.readFileSync( "../../raw_data/second_version_tables/shbs_enrolled_fields.2024-07-02.csv", 'utf8' ),
//     {
//         header:true,
//         dynamicTyping:true,
//         transform: ( value ) => { return value == 'NA' ? undefined : value; }
//     } )
// ;

let fieldsSource = JSON.parse(fs.readFileSync(`${__dirname}/../../raw_data/geographic_data/SHBS_Fields_11-25.geojson`))
    .features
    .map( d => {
    let output = {
        field_id: d.properties.field_id,
        farm_id: d.properties.farm_id,
        farm_name: d.properties.farm_name,
        field_name: d.properties.field_name,
        active: true
    };
    return output;
} );

let fieldsData = fieldsSource
    .map( entry => {
        let fieldGeometryObj = geometrySource.find( source => source.field_id == entry.field_id );
        entry.geometry = fieldGeometryObj?.geometry;
        entry.status = entry.active ? 'active' : 'archived';
        return entry;
    } )
;

let fieldsMissingAGeometry = fieldsData.filter( d => !d.geometry ).map( d => d.field_id );
fs.writeFileSync(`${__dirname}/../operation_registers/fields_missing_geometry.json`, JSON.stringify(fieldsMissingAGeometry));


let farmNames = Array.from( new Set( fieldsSource.map( d => d.farm_name ) ) );
let fieldsSummaryTable = farmNames.map( farm => {
    let entries = fieldsSource.filter( d => d.farm_name == farm );
    let field_id_values = new Set( entries.map( d => d.field_id ) );
    let field_name_values = new Set( entries.map( d => d.field_name ) );
    return( {
        farm_name: farm,
        field_id_values: field_id_values,
        field_name_values: field_name_values,
        number_of_entries: entries.length
    });
} );

fs.writeFileSync( `${__dirname}/../operation_registers/finalFieldsData.json`, JSON.stringify(fieldsData) );

let fieldsOntology = fieldsData.map( d => { return { natural: new String( d.field_name ), slug: d.field_name, farm_name: d.farm_name, field_name: d.field_name, field_id: d.field_id, farm_id: d.farm_id, replaces: [] }; } );
fs.writeFileSync( `${__dirname}/../operation_registers/fields_ontology.json`, JSON.stringify(fieldsOntology) );
