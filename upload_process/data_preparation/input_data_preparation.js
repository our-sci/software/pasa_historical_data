// This script should read data in the appropirate CSV and prepare it to be submitted by the submitters.
const papa = require('papaparse');
const fs = require('fs');
const {
    farmDomainDict
} = require('../submitters/artifacts.js');
const input_encoding = require('../encodings/organic_inputs_data_encoding');
const { parsePasaDate, addIndexToRepeatedNames } = require("./preparation_utilities.js");

let inputs_encoding = require("../encodings/organic_inputs_data_encoding");
let farm_data = JSON.parse( fs.readFileSync(`${__dirname}/../operation_registers/finalFarmsOrganizationData.json`) );

const farmStagesTable = JSON.parse( fs.readFileSync("../submitters/farmsStageAssignment.json") );

let inputsSource = papa.parse(
    fs.readFileSync( "../../raw_data/third_version_tables/orginput_records_2024-09-03.csv" , 'utf8' ),
    {
        header:true,
        dynamicTyping:true,
        transform: ( value ) => { return ['NA', ' '].includes( value )  ? undefined : value; }
    } )
;
let inputsSource2023 = papa.parse(
    fs.readFileSync("../../raw_data/third_version_tables/data_2023/Org Inputs 2023 - bullet field names.csv", 'utf8'),
    {
        header: true,
        dynamicTyping: true,
        transform: (value) => { return ['NA', ' '].includes(value) ? undefined : value; }
    })
;

// needed to estimate planting / amendment association using dates
let plantingsSourceData = JSON.parse( fs.readFileSync( "../operation_registers/finalPlantingData.json" ) )
    .filter( d => d.planting_date )
    .map( row => {
        row.planting_date = new Date(row.planting_date);
        row.term_date = new Date(row.term_date);
        return row;
    } )
;

// needed to obtain field names, using when naming logs (we now do that when preparing data)
let fieldsFinalData = JSON.parse( fs.readFileSync( `${__dirname}/../operation_registers/finalFieldsData.json` ) );

let fullDataset = [ ... inputsSource.data, ... inputsSource2023.data ];

let allInputsRepeated = fullDataset
    .filter( d => d.field_id && d.input_type != "none" )
    .map( ( row, index ) => {
        // TODO assuming applied_date
        let dateSource = row.applied_date ? row.applied_date : `01-01-${ row.survey_year }`;
        row.applied_date = parsePasaDate({string: dateSource});
        // Associate with a planting using the time range between seeding and harvest.
        // pair farm name with farmos url
        row.farm_id = row.field_id.split('_')[0];
        let farmEntry = farm_data.find( d => d.farm_id == row.farm_id );
        row.farmDomain = farmStagesTable.find( farm => farm.farm_id == row.farm_id ).farmDomain;
        row.farm_name = farmEntry.farm_name;
        let associatedPlanting = plantingsSourceData.filter( plantingRow => {
            // The criteria for planting association currently is: amendment happened BEFORE seeding, seeding is the first one after the amendment (applied later, when sorting).
            return plantingRow.term_date >= row.applied_date &&
                plantingRow.field_id == row.field_id &&
                plantingRow.farm_name == row.farm_name
            ;
        } );
        if ( associatedPlanting.length == 1 ) {
            row.planting = associatedPlanting[0];
            row.planting_name= associatedPlanting[0].plantingName;
            row.paired = true;
        } else if (associatedPlanting.length == 0) {
            row.paired = false;
        } else if ( associatedPlanting.length > 1 ) {
            row.planting = associatedPlanting
                .sort( (a,b) => b.planting_date.getTime() - a.planting_date.getTime() )
            [0]
            ;
            row.planting_name= row.planting.plantingName;
            row.paired = true;
        }
        ;
        let fieldEntry = fieldsFinalData.find( d => d.field_id == row.field_id );
        if (fieldEntry) {
            row.farm_name = fieldEntry.farm_name;
            row.field_name = fieldEntry.field_name;
        };

        // if there's no "area percentage" but the unit in "area" is "percent", populate area percent
        if (!row.area_percent && ["percent", "percentage", "%"].includes( row.area_units )) {
            row.area_percent = row.area;
        };
        // If there's an area percentage, we will delete the other area quantities
        if (row.area_percent) {
            delete row.area;
            delete row.area_units;
        };

        // create log name
        row.input_log_name = input_encoding.inputNameGenerator({activity: row.input_type, farmerName: row.farmer_input, field: row.field_name, date: row.applied_date});

        // structure notes as a unique JSON document.
        if (row.farmer_notes?.length > 0 || row.research_notes?.length > 0 || row.farmer_input) {
            let notes = {
                value: "Observations for this planting:\n",
                format: "default"
            };
            if ( row.farmer_input ) {
                notes.value = notes.value.concat( `<blockquote> <bold>Farmer input name:</bold> ${ row.farmer_input } </blockquote>` );
            };
            if ( row.farmer_notes?.length ) {
                notes.value = notes.value.concat( `<blockquote> <bold>Farmer notes:</bold> ${ row.farmer_notes } </blockquote>` );
            };
            if ( row.research_notes?.length ) {
                notes.value = notes.value.concat( `<blockquote> <bold>Research notes:</bold> ${ row.research_notes } </blockquote>` );
            };
            row.notes = notes;
        };
        delete row.research_notes;
        delete row.farmer_notes;
        return row;
    })
;

let inputs = addIndexToRepeatedNames({ sourceArray: allInputsRepeated, pkeyAttribute: "organic_input_record_pkey", logNameAttribute: "input_log_name" });

fs.writeFileSync( `${__dirname}/../operation_registers/finalInputData.json`, JSON.stringify(inputs) );
fs.writeFileSync( `${__dirname}/../operation_registers/finalInputData.csv`, papa.unparse(inputs) );


// Source data usage.
let sourceFields = new Set( inputsSource.meta.fields );
let usedFields = new Set( inputs_encoding.populators.flatMap( d => d.source ) );
let dataFieldComments = [
    {
        field:'complete',
        comment: 'Does not seem relevant, could map to the log.',
        explained: false
    },
    {
        field:'field_id',
        comment: 'Used indirectly for data matching',
        explained: true
    },
    {
        field:'field_name',
        comment: 'Used indirectly for data matching',
        explained: true
    },
    {
        field:'survey_year',
        comment: 'Used indirectly for data matching',
        explained: true
    },
    {
        field:'farm_name',
        comment: 'Used indirectly for data matching',
        explained: true
    },
    {
        field:'farmer_input',
        comment: 'Seems to be the originbal non processed entry. I would also add it to the notes attribute document.',
        explained: false
    },
    {
        field:'farmer_notes',
        comment: 'Need to figure out the format for notes upload on FarmOS',
        explained: true
    },
    {
        field:'research_notes',
        comment: 'Need to figure out the format for notes upload on FarmOS',
        explained: true
    },
    {
        field:'bed_width',
        comment: 'Need to figure out where to store it',
        explained: true
    },
    {
        field:"analysis",
        comment: "only indicates if npk values will be present",
        explained: true
    },
    {
        field:"area_units",
        comment: "The area is only stored as acres, but here several units exist.",
        known_values: new Set( inputsSource.data.map( d => d.area_units ) ),
        explained: false
    },
    {
        field:"area",
        comment: "The area is only stored as acres, but here several units exist.",
        known_values: new Set( inputsSource.data.map( d => d.area_units ) ),
        explained: false
    },
    {
        field: "field_area_acres",
        comment: "Storing the total field area might be redundant.",
        explained: true
    },
    {
        field: "bed_width_ft",
        comment: "NEed to figure where to store it. Seems to belong into the planting.",
        explained: false
    },
    {
        field: "quantity",
        comment: "seems redundant having quantity_lbs",
        expalined: true
    },
    {
        field: "quantity_units",
        comment: "seems redundant having quantity_lbs",
        expalined: true
    },
];
let unusedFields = Array.from( sourceFields )
    .filter( field => !usedFields.has(field) )
    .filter( field => !dataFieldComments.filter( d => d.explained ).find( comment => comment.field == field ) )
;
let unexplainedFields = Array.from( sourceFields )
    .filter( field => !usedFields.has(field) )
    .filter( field => !dataFieldComments.find( comment => comment.field == field ) )
;
