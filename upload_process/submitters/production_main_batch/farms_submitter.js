// This script should read data in the appropirate CSV and send it into a FarmOS instance in the right pre established format.
// In this case, we will submit entities representing whole farms, that will be roots of the whole data structure representing the operation.
// Our intention with this encoding is to have a root entity that encompasses all the farm data, because we are considering grouping many farms into a single FarmOS instance in the future, and this would be a good way to separate them and have control of what belongs to each individual physical farm.
const papa = require('papaparse');
const fs = require('fs');
const { randomUUID } = require('crypto');
const {
    printResults,
    GeneralFootprint,
    ExternalRelationshipDescription,
    Matcher,
    EntityFootprint
} = require('farm_os_entity_uploader');
const {
    synthesisTable,
    uploadAndRegister
} = require('../utilities.js');
const {
    selectedFarms,
    marylandBatchFarms,
    mainBatchTable
} = require("./main_batch_artifacts.js");

let farms_encoding = require("../../encodings/farms_data_encoding.js");

const currentStage = "production_main_batch";
const datasetName = "farms";
const farmStagesTable = JSON.parse( fs.readFileSync("../farmsStageAssignment.json") );

const operationRegistersFolder = `${__dirname}/../../operation_registers`;

require('dotenv').config({ path:`${__dirname}/../../../.env` });
const aggregatorKey = process.env.FARMOS_KEY;

let farmsData = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalFarmsOrganizationData.json` ) )
    .filter( row => selectedFarms.includes( row.farm_id ) )
    .map( row => {
        row.farmDomain = mainBatchTable.find( entry => entry['Account: Research ID'] == row.farm_id )['farmOS ID'];
        return row;
    } )
;

let uploader = new GeneralFootprint( {
    entityFootprints: farms_encoding.footprints,
    populators: farms_encoding.populators,
    aggregatorKey: aggregatorKey
} );

let encodedData = JSON.parse( fs.readFileSync(`${operationRegistersFolder}/${currentStage}/mainBatchFarmUploadEncodedData.json`) );

// fixing wrong entries in the encoded data
let fixes = JSON.parse( fs.readFileSync(`${__dirname}/../../operation_registers/production_main_batch/farm_errors_analysis.json`) );

// find the wrongly assigned entries, and assign them to a proper payload.
fixes.to_remove
    .forEach( fix => {
        let correctData = fixes.to_add.find( d => d.name == fix.name );
        let entries = encodedData.payloads[fix.current_wrong_url].filter( d => d.attributes.name == fix.name );
        // console.log(`${entries.length} for ${fix.name} found in ${fix.current_wrong_url}, sent to ${correctData.url}`);
        encodedData.payloads[correctData.url] = structuredClone( entries );
        encodedData.payloads[correctData.url].forEach( e => e.submitter_attributes.farmDomain = correctData.url );
        // remove entities from wrong farm
        encodedData.payloads[fix.current_wrong_url] = encodedData.payloads[fix.current_wrong_url].filter( d => d.attributes.name != fix.name );
    } );

// check it looks sane
fixes.to_add.map( d => { return { payload: encodedData.payloads[d.url], farm: d.name }; } );
// all farms have exactly 2 entries
Object.entries( encodedData.payloads ).map( d => d[1].length ).filter( d => d != 2 );

// the farms weren't mentioned in the stages table correctly
// we fix the main table first, because we need it for the pairing of farm_id attributes.
farmsData
    .filter( row => fixes.to_add.map(d => d.name).includes( row.farm_name ) )
    .forEach( row => {
        let correction = fixes.to_add.find( d => d.name == row.farm_name );
        correction.farm_id = row.farm_id;
        row.farmDomain = correction.url;
    } )
;
farmStagesTable
    .filter( d => fixes.to_add.map(d => d.farm_id).includes( d.farm_id ) )
    .forEach( row => {
        let correction = fixes.to_add.find( d => d.farm_id == row.farm_id );
        row.farmDomain = correction.url;
    } )
;

let preexistingUpload = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${currentStage}/${datasetName}_upload_initial.json` ) );

// let upload = await uploadAndRegister({
//     uploader: uploader,
//     currentStage: currentStage,
//     datasetName: `${ datasetName }_initial`,
//     operationRegisterFolder: operationRegistersFolder,
//     dataset: farmsData,
//     preexistingEntities: [],
//     farmsStageAssignment: farmStagesTable,
//     dryRun: true,
//     encodedData: encodedData
// });

let remainingFarms = Array.from( new Set( preexistingUpload.uploadsNeedingReview.map( d => d.farmos_url ) ) );

let remainingEncodedData = {
    payloads: {}
};

let wrongURL = [
    {
        current: 'raicesfarms.farmos.net',
        actual: "raicesfarm.farmos.net"
    },
    {
        current: 'cow-a-henfarm.farmos.net',
        actual: 'cow-a-hen.farmos.net'
    },
    {
        current: 'thistlecreekfarms.farmos.ne',
        actual: 'thistlecreekfarms.farmos.net'
    }
];

remainingFarms.forEach( farm => {
    let isWrongURL = wrongURL.find( d => d.current == farm );
    let payload = encodedData.payloads[farm];
    if ( isWrongURL ) {
        farm = isWrongURL.actual;
    }
    remainingEncodedData.payloads[farm] = payload;
} );

// let uploadRemainingFarms = await uploadAndRegister({
//     uploader: uploader,
//     currentStage: currentStage,
//     datasetName: `${ datasetName }_remaining_farms`,
//     operationRegisterFolder: operationRegistersFolder,
//     dataset: farmsData,
//     preexistingEntities: [],
//     farmsStageAssignment: farmStagesTable,
//     dryRun: true,
//     encodedData: remainingEncodedData
// });

let marylandFarmIDs = farmStagesTable
    .filter( d => d.stage == "production_maryland_batch" )
    .map( d => d.farm_id )
;

let marylandFarmsData = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalFarmsOrganizationData.json` ) )
    .filter( row => marylandFarmIDs.includes( row.farm_id ) )
    .map( row => {
        row.farmDomain = farmStagesTable.find( entry => entry.farm_id == row.farm_id ).farmDomain;
        return row;
    } )
;

// let uploadMarylandFarms = await uploadAndRegister({
//     uploader: uploader,
//     currentStage: "production_maryland_batch",
//     datasetName: `${ datasetName }_maryland_farms`,
//     operationRegisterFolder: operationRegistersFolder,
//     dataset: marylandFarmsData,
//     preexistingEntities: [],
//     farmsStageAssignment: farmStagesTable,
//     dryRun: true,
// });


// let resend = [
//     'cornerstonefarm.farmos.net',
//     'libertygardens.farmos.net',
//     'appleridgefarm.farmos.net'
// ];

// let resendData = farmsData.filter( d => resend.includes( d.farmDomain ) );


// let resendFarms = await uploadAndRegister({
//     uploader: uploader,
//     currentStage: currentStage,
//     datasetName: `${ datasetName }_resend`,
//     operationRegisterFolder: operationRegistersFolder,
//     dataset: resendData,
//     preexistingEntities: [],
//     farmsStageAssignment: farmStagesTable,
//     dryRun: true,
// });


let nov2024NewFarms = JSON.parse(fs.readFileSync(`${__dirname}/../../../raw_data/geographic_data/nov2024FieldIDs.json`));
let farmsDataNov2024 = farmStagesTable
    .filter(row => nov2024NewFarms.includes(row.farm_id))
;

let uploadNov2024 = await uploadAndRegister({
    uploader: uploader,
    currentStage: currentStage,
    datasetName: 'production_main_batch_nov2024_farms',
    operationRegisterFolder: operationRegistersFolder,
    dataset: farmsDataNov2024,
    preexistingEntities: [],
    farmsStageAssignment: farmStagesTable,
    dryRun: true,
});
