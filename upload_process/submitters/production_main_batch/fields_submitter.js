// This script should read data in the appropirate CSV and send it into a FarmOS instance in the right pre established format.
// In this case, we will link fields with previously existing land assets representing whole farms, in order to get a meaningful hierarchy organizing the data, as well as a good way to unify all fields into a unique instance in he future if needed, and still have a global entity representing individual farms.
const papa = require('papaparse');
const fs = require('fs');
const { randomUUID } = require('crypto');
const {
    printResults,
    GeneralFootprint,
    ExternalRelationshipDescription,
    Matcher,
    EntityFootprint
} = require('farm_os_entity_uploader');
const {
    uploadAndRegister,
    synthesisTable
} = require('../utilities.js');
const {
    datePattern
} = require('../artifacts.js');
const {
    selectedFarms,
    mainBatchTable
} = require("./main_batch_artifacts.js");

// Our data design.
let fields_encoding = require("../../encodings/fields_data_encoding.js");

const farmStagesTable = JSON.parse( fs.readFileSync("../farmsStageAssignment.json") );
require('dotenv').config({ path:'../../../.env' });
const aggregatorKey = process.env.FARMOS_KEY;

const currentStage = "production_main_batch";
const datasetName = "fields";
const operationRegistersFolder = `${__dirname}/../../operation_registers`;

// We need to reference entities from previous upload processes (the assets representing whole farms).
let preexistingFarms = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${currentStage}/farms_upload_initial.json` ) )
   .summaries
   .flatResults
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
;

let fieldsData = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalFieldsData.json` ) )
    .filter( row => selectedFarms.includes( row.farm_id ) )
    .map( row => {
        row.farmDomain = mainBatchTable.find( entry => entry['Account: Research ID'] == row.farm_id )['farmOS ID'];
        return row;
    } )
    .map( row => {
        let fields = Object.keys(row);
        fields.forEach( field => {
            if ( datePattern.test( row[field] ) ) {
                row[field] = new Date(row[field]);
            }
        } );
        return row;
    } )
;


// fixing wrong entries in the encoded data
let fixes = JSON.parse( fs.readFileSync(`${__dirname}/../../operation_registers/production_main_batch/farm_errors_analysis.json`) );
fieldsData
    .filter( row => fixes.to_add.map(d => d.name).includes( row.farm_name ) )
    .forEach( row => {
        let correction = fixes.to_add.find( d => d.name == row.farm_name );
        correction.farm_id = row.farm_id;
        row.farmDomain = correction.url;
    } )
;
farmStagesTable
    .filter( d => fixes.to_add.map(d => d.farm_id).includes( d.farm_id ) )
    .forEach( row => {
        let correction = fixes.to_add.find( d => d.farm_id == row.farm_id );
        row.farmDomain = correction.url;
    } )
;

let uploader = new GeneralFootprint( {
    entityFootprints: fields_encoding.footprints,
    populators: fields_encoding.populators,
    aggregatorKey: aggregatorKey
} );

let exampleEntry = fieldsData.find( d => d.farm_id == 'PA00013' );

// let upload = await uploadAndRegister({
//     uploader: uploader,
//     currentStage: currentStage,
//     datasetName: datasetName,
//     operationRegisterFolder: operationRegistersFolder,
//     dataset: fieldsData,
//     preexistingEntities: preexistingFarms,
//     farmsStageAssignment: farmStagesTable,
//     dryRun: true,
// });

let preexistingUpload = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${currentStage}/${datasetName}_upload_initial.json` ) );
preexistingUpload.uploadsNeedingReview = preexistingUpload.summaries.flatResults.filter( d => ![201, 409].includes(d.status) );

let remainingFarms = Array.from( new Set( preexistingUpload.uploadsNeedingReview.map( d => d.farmos_url ) ) );

let remainingEncodedData = {
    payloads: {}
};

let wrongURL = [
    {
        current: 'raicesfarms.farmos.net',
        actual: "raicesfarm.farmos.net"
    },
    {
        current: 'cow-a-henfarm.farmos.net',
        actual: 'cow-a-hen.farmos.net'
    },
    {
        current: 'thistlecreekfarms.farmos.ne',
        actual: 'thistlecreekfarms.farmos.net'
    }
];

remainingFarms.forEach( farm => {
    let isWrongURL = wrongURL.find( d => d.current == farm );
    let payload = preexistingUpload.encodedData.payloads[farm];
    if ( isWrongURL ) {
        farm = isWrongURL.actual;
    }
    remainingEncodedData.payloads[farm] = payload;
} );

// let uploadRemaining = await uploadAndRegister({
//     uploader: uploader,
//     currentStage: currentStage,
//     datasetName: datasetName + "_remaining",
//     operationRegisterFolder: operationRegistersFolder,
//     dataset: fieldsData,
//     preexistingEntities: preexistingFarms,
//     farmsStageAssignment: farmStagesTable,
//     dryRun: true,
//     encodedData: remainingEncodedData
// });

let uploadRemaining = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${currentStage}/${datasetName}_remaining_upload.json` ) );

let fullManifest = [
    ... preexistingUpload.summaries.flatResults.filter( d => !remainingFarms.includes( d.farmos_url ) ),
    ... uploadRemaining.summaries.flatResults
];

fs.writeFileSync( `${operationRegistersFolder}/${currentStage}/${datasetName}_upload.json`, JSON.stringify(fullManifest) );

let notYetSent = [
    'pasa-bucknelluniversityfarm.farmos.net',
    'rodaleinstitute.farmos.net',
    'smallvalleymilling.farmos.net',
    // 'bendingbridgefarm.farmos.net',
    'bradleydairyfarm.farmos.net',
    'buckhillfarm.farmos.net',
    'pasa-carversvillefarmfoundation.farmos.net',
    'charlesloverichfarm.farmos.net',
    'davilerayfarm.farmos.net',
    'davidstoltzfusjrfarm.farmos.net',
    'frankcarrfarm.farmos.net',
    'haresvalleygrowers.farmos.net',
    'hiddenvalleyfarm.farmos.net',
    'hurstfarmworks.farmos.net',
    'kerithbrookfarm.farmos.net',
    'kirbynissleyfarm.farmos.net',
    'longacrefarm.farmos.net',
    'oldtinrooffarmllc.farmos.net',
    'raicesfarm.farmos.net',
    'saconymarshfarms.farmos.net',
    'windyknollfarm.farmos.net'
];

let notSentEntities = notYetSent.flatMap( d => preexistingUpload.encodedData.payloads[d] );

let missingGeometry = notSentEntities
    .filter(  d => d )
    .filter( d => !d.attributes.intrinsic_geometry )
    .map( d => d.id )
;

missingGeometry.push(preexistingUpload.encodedData.payloads["raicesfarms.farmos.net"][0].id);

let noGeometryEncodedData = { payloads: {} };

notYetSent.forEach( url => {
    let payload = preexistingUpload.encodedData.payloads[url];
    if (url == "raicesfarm.farmos.net") {
        payload = preexistingUpload.encodedData.payloads["raicesfarms.farmos.net"];
    };
    if (!payload) {
        payload = remainingEncodedData.payloads[url];
    };
    payload = payload
        .filter( d => missingGeometry.includes( d.id ) )
        .map( d => {
        d.submitter_attributes.empty = false;
        d.submitter_attributes.void = false;
        return d;
    } )
    ;
    noGeometryEncodedData.payloads[url] = payload;
} );


// let uploadNoGeo = await uploadAndRegister({
//     uploader: uploader,
//     currentStage: currentStage,
//     datasetName: datasetName + "_no_geo",
//     operationRegisterFolder: operationRegistersFolder,
//     dataset: fieldsData,
//     preexistingEntities: preexistingFarms,
//     farmsStageAssignment: farmStagesTable,
//     dryRun: true,
//     encodedData: noGeometryEncodedData
// });

let preexistingMarylandFarms = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/production_main_batch/maryland_farms_upload.json` ) )
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
;

// let marylandFarmIDs = farmStagesTable
//     .filter( d => d.stage == "production_maryland_batch" )
//     .map( d => d.farm_id )
// ;
// let marylandFieldsData = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalFieldsData.json` ) )
//     .filter( row => marylandFarmIDs.includes( row.farm_id ) )
//     .map( row => {
//         row.farmDomain = farmStagesTable.find( d => d.farm_id == row.farm_id ).farmDomain;
//         return row;
//     } )
//     .map( row => {
//         let fields = Object.keys(row);
//         fields.forEach( field => {
//             if ( datePattern.test( row[field] ) ) {
//                 row[field] = new Date(row[field]);
//             }
//         } );
//         return row;
//     } )
// ;

// let marylandUpload = await uploadAndRegister({
//     uploader: uploader,
//     currentStage: "production_maryland_batch",
//     datasetName: `${ datasetName }_maryland`,
//     operationRegisterFolder: operationRegistersFolder,
//     dataset: marylandFieldsData,
//     preexistingEntities: [ ... preexistingMarylandFarms, ... preexistingFarms ],
//     farmsStageAssignment: farmStagesTable,
//     dryRun: true,
// });


// let resend = [
//     'cornerstonefarm.farmos.net',
//     'libertygardens.farmos.net',
//     'appleridgefarm.farmos.net'
// ];

// let resendFarmsData = fieldsData.filter( d => resend.includes( d.farmDomain ) );

// let resendUpload = await uploadAndRegister({
//     uploader: uploader,
//     currentStage: currentStage,
//     datasetName: `${ datasetName }_resend`,
//     operationRegisterFolder: operationRegistersFolder,
//     dataset: resendFarmsData,
//     preexistingEntities: [ ... preexistingMarylandFarms, ... preexistingFarms ],
//     farmsStageAssignment: farmStagesTable,
//     dryRun: true,
// });


let nov2024NewFarms = JSON.parse(fs.readFileSync(`${__dirname}/../../../raw_data/geographic_data/nov2024FieldIDs.json`));
// one field was for a preexisting farm
let extraField = JSON.parse(fs.readFileSync(`${__dirname}/../../../raw_data/geographic_data/newFieldPreexistingFarm2025.json`))[0];

let fieldsNov2024 = JSON.parse(fs.readFileSync(`${operationRegistersFolder}/finalFieldsData.json`))
    .filter(row => nov2024NewFarms.includes(row.farm_id) || row.field_id == extraField.field_id)
    .map(row => {
        row.farmDomain = farmStagesTable.find( entry => entry.farm_id == row.farm_id ).farmDomain;
        return row;
    })
    .map(row => {
        let fields = Object.keys(row);
        fields.forEach(field => {
            if (datePattern.test(row[field])) {
                row[field] = new Date(row[field]);
            }
        });
        return row;
    })
    ;

;


let preexistingNov2024 = JSON.parse(fs.readFileSync(`${operationRegistersFolder}/${currentStage}/production_main_batch_nov2024_farms_upload.json` ) )
   .summaries
   .flatResults
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
;

let uploadNov2024 = await uploadAndRegister({
    uploader: uploader,
    currentStage: currentStage,
    datasetName: 'production_main_batch_nov2024_fields',
    operationRegisterFolder: operationRegistersFolder,
    dataset: fieldsNov2024,
    preexistingEntities: [ ... preexistingMarylandFarms, ... preexistingFarms, ... preexistingNov2024 ],
    farmsStageAssignment: farmStagesTable,
    dryRun: false,
});
