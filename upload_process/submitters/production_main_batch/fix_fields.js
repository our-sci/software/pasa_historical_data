const fs = require("fs");
const {
    selectedFarms,
    mainBatchTable
} = require("./main_batch_artifacts.js");

const operationRegistersFolder = `${__dirname}/../../operation_registers`;
const currentStage = "production_main_batch";

let upload = JSON.parse( fs.readFileSync(`${__dirname}/../../operation_registers/production_main_batch/old_farms_upload.json`) )
    .summaries
    .flatResults
;

let fixes = JSON.parse( fs.readFileSync(`${__dirname}/../../operation_registers/production_main_batch/farm_errors_analysis.json`) );

let namesToRemove = fixes.to_remove.map( d => d.name );

let operationsToUndo = upload.filter( d => namesToRemove.includes( d.entity.data.attributes.name ) );

let controlUndoOperations = operationsToUndo.map( d => { return { name: d.entity.data.attributes.name, farmDomain: d.farmos_url } } );


fs.writeFileSync( `${operationRegistersFolder}/${currentStage}/entitiesToRemove.json`, JSON.stringify(operationsToUndo) );


let farmsData = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalFarmsOrganizationData.json` ) )
    .filter( row => selectedFarms.includes( row.farm_id ) )
    .map( row => {
        row.farmDomain = mainBatchTable.find( entry => entry['Account: Research ID'] == row.farm_id )['farmOS ID'];
        return row;
    } )
;

let namesToAdd = fixes.to_add.map( d => d.name );
let missingRows = farmsData
    .filter( d => namesToAdd.includes(d.farm_name) )
    .map( d => {
        let entry = fixes.to_add.find( e => e.name == d.farm_name );
        d.farmDomain = entry.url;
        return d;
    } )
;


fs.writeFileSync( `${operationRegistersFolder}/${currentStage}/entitiesToEncode.json`, JSON.stringify(missingRows) );
