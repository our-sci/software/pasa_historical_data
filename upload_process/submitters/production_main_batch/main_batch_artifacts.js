const papa = require('papaparse');
const fs = require('fs');

let firstBatchFarms = [
    'PA00256', 'PA00194',
    'MD00108', 'PA00195',
    'MD00109', 'PA00065',
    'PA00001', 'PA00257',
    'PA00260', 'PA00013',
    'PA00197', 'PA00198',
    'PA00199', 'PA00210',
    'PA00201', 'PA00258'
];

// document shared by Serena Xu
// https://docs.google.com/spreadsheets/d/1gC1YBDtgDLhs-UwFOuo-MIvfBHiW2JxVmRZrNiFLQBA/edit?gid=0#gid=0
// Name: 'FarmID+farmOS URL'
let mainBatchTable = papa.parse( fs.readFileSync( `${__dirname}/../../../raw_data/production_farms_corrected.csv`, "utf8" ), {header: true} )
    .data
    .filter(d => !firstBatchFarms.includes(d['Account: Research ID']) )
;

let selectedFarms = mainBatchTable.map( row => row["Account: Research ID"] );

exports.selectedFarms = selectedFarms;
exports.mainBatchTable = mainBatchTable;
