# Groups of farms

  We have 4 groups of farms, which when sent in a strict order will allow a tight control of the submission process, feedback and adequate error detection.
  Farms are assigned to stages in the `farmsStageAssignment.json` file.
  
## Stages

### Test farms

  Test farms can't be accessed by any users.

1. Test farm. This farm is meant to try and test all new developments until all conceivable errors are tested, prevented and fixed. It will mostly concern developers. There's no naturally associated data for it, so depending on the experiment a dataset needs to be generated.
2. Demo farms. These farms are copies of actual farms, selected by PASA. Each one will be populated until they are ready for review. They should reflect the results we expect to obtain when submitting to production farms.

### Production farms

1. Production first batch. This batch encompasses farms for which we can expect the farmers to never use them, for example because they avoid the usage of any electric appliance. We will submit first here, and review for errors again, while ideally there should be none.
2. Main production batch.  All other production farms, send when all review steps are finished successfully.

# Submissions Order

  Due to relationships, there's a natural dependency between processes. The order is quite shallow: Most logs require a location and a plant that shold be preexisting.
  As of july 2024, there are three levels.
  
1. Farm creation. The submtter is called `farm_submitter` and it creates an `organization--farm` to represent the farm, plus an `asset--land` log of the type `property` that will encompass the land assets represdenting fields.
2. Field Creation ( first `farm_submitter.js`, then `fields_submitter.js`). This encompasses the `asset--land` entities.
3. Plant creation (`plantings_submitter.js`). This will create the `asset--plant` entitites that represent plantings, as we as the `taxonomy_term-season`.
4. All other logs (`organic_inputs_submitter.js`, `tillage_submitter.js`). These typically only depend on farms and fields.


