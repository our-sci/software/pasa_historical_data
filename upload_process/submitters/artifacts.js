// Some constants that control the upload process, repeated through upload processes.
let selectedFarms = [
    // // (Row Crop, Vegetable)
    "Cedar Meadow Farm",
    // // (Row Crop, wheat in NDBS?)
    "Red Cat Farm, LLC" ,
    // // (Row Crop, wheat in NDBS?)
    "Provident Farms",
    // // (Pastured Livestock)
    "Willow Run Farm - Boyer",
    // //  (Pastured Livestock) Will need to add perennial pasture to plantings.
    "Spring Creek Farms",
    //  // (Pastured Livestock) Will need to add perennial pasture to plantings.
    "Greene Kitchen Farm"
];



let actualToDemoFarmDict = {
    "cedarmeadowfarm.farmos.net":'cedarmeadowsdemo.farmos.net',
    "redcatfarm.farmos.net":'redcatfarmdemo.farmos.net',
    'providentfarms.farmos.net':'providentfarmsdemo.farmos.net',
    "willowrunfarmstead.farmos.net":'willowrundemo.farmos.net',
    "springcreekfarms.farmos.net":'springcreekfarmdemo.farmos.net',
    "greenekitchenfarm.farmos.net":'greenekitchendemo.farmos.net'
};

let farmDomainDict = {
    "Cedar Meadow Farm":'cedarmeadowsdemo.farmos.net',
    "Red Cat Farm, LLC":'redcatfarmdemo.farmos.net',
    "Provident Farms":'providentfarmsdemo.farmos.net',
    "Willow Run Farmstead":'willowrundemo.farmos.net',
    "Spring Creek Farms":'springcreekfarmdemo.farmos.net',
    "Greene Kitchen Farm":'greenekitchendemo.farmos.net'
};


let datePattern = /[0-9]{4}\-[0-9]{2}\-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}/;

exports.selectedFarms = selectedFarms;
exports.actualToDemoFarmDict = actualToDemoFarmDict;
exports.farmDomainDict = farmDomainDict;
exports.datePattern = datePattern;
