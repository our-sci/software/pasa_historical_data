// This script should read data in the appropirate CSV and send it into a FarmOS instance in the right pre established format.
// In this case, we will submit entities representing whole farms, that will be roots of the whole data structure representing the operation.
// Our intention with this encoding is to have a root entity that encompasses all the farm data, because we are considering grouping many farms into a single FarmOS instance in the future, and this would be a good way to separate them and have control of what belongs to each individual physical farm.
const papa = require('papaparse');
const fs = require('fs');
const { randomUUID } = require('crypto');
const {
    printResults,
    GeneralFootprint,
    ExternalRelationshipDescription,
    Matcher,
    EntityFootprint
} = require('farm_os_entity_uploader');
const {
    datePattern
} = require('../artifacts.js');
const {
    formatOperationDeliverables,
    synthesisTable,
    uploadAndRegister
} = require('../utilities.js');
const {
    selectedFarms,
    firstBatchTable
} = require("./first_batch_artifacts.js");

let farms_encoding = require("../../encodings/farms_data_encoding.js");

const currentStage = "production_first_batch";
const datasetName = "farms";

const operationRegistersFolder = `${__dirname}/../../operation_registers`;

require('dotenv').config({ path:`${__dirname}/../../../.env` });
const aggregatorKey = process.env.FARMOS_KEY;

const farmStagesTable = JSON.parse( fs.readFileSync("../farmsStageAssignment.json") );

let farmsData = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalFarmsOrganizationData.json` ) )
    .filter( row => selectedFarms.includes( row.farm_id ) )
    .map( row => {
        row.farmDomain = farmStagesTable.find( entry => entry.farm_id == row.farm_id ).farmDomain;
        return row;
    } )
;

let uploader = new GeneralFootprint( {
    entityFootprints: farms_encoding.footprints,
    populators: farms_encoding.populators,
    aggregatorKey: aggregatorKey
} );


let uploadProcess = uploadAndRegister({
    uploader: uploader,
    currentStage: currentStage,
    datasetName: datasetName,
    operationRegisterFolder: operationRegistersFolder,
    dataset: farmsData,
    preexistingEntities: [],
    farmsStageAssignment: farmStagesTable,
    dryRun: true,
});

let exampleEntry = farmsData.find(d => d.farm_id == "PA00013");
let table = synthesisTable({ dataRow: exampleEntry, uploader: uploader, preexistingEntities: [], encoding: farms_encoding, datasetName: "farms", currentStage: currentStage, operationRegistersFolder: operationRegistersFolder });


// let uploadProcessBendingBridge = uploadAndRegister({
//     uploader: uploader,
//     currentStage: currentStage,
//     datasetName: `${ datasetName }_bending_bridge`,
//     operationRegisterFolder: operationRegistersFolder,
//     dataset: farmsData.filter( d => d.farm_id == "PA00001" ),
//     preexistingEntities: [],
//     farmsStageAssignment: farmStagesTable,
//     dryRun: true,
// });
