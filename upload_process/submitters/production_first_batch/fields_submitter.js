// This script should read data in the appropirate CSV and send it into a FarmOS instance in the right pre established format.
// In this case, we will link fields with previously existing land assets representing whole farms, in order to get a meaningful hierarchy organizing the data, as well as a good way to unify all fields into a unique instance in he future if needed, and still have a global entity representing individual farms.
const papa = require('papaparse');
const fs = require('fs');
const { randomUUID } = require('crypto');
const {
    printResults,
    GeneralFootprint,
    ExternalRelationshipDescription,
    Matcher,
    EntityFootprint
} = require('farm_os_entity_uploader');
const {
    formatOperationDeliverables,
    synthesisTable,
    uploadAndRegister
} = require('../utilities.js');
const {
    datePattern
} = require('../artifacts.js');
const {
    selectedFarms,
    firstBatchTable
} = require("./first_batch_artifacts.js");

// Our data design.
let fields_encoding = require("../../encodings/fields_data_encoding.js");

require('dotenv').config({ path:'../../../.env' });
const aggregatorKey = process.env.FARMOS_KEY;

const farmStagesTable = JSON.parse( fs.readFileSync("../farmsStageAssignment.json") );

const currentStage = "production_first_batch";
const datasetName = "fields";
const operationRegistersFolder = `${__dirname}/../../operation_registers`;

// We need to reference entities from previous upload processes (the assets representing whole farms).
let preexistingEntities = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${currentStage}/farms_upload.json` ) )
    .filter( d => [201, 409].includes( d.status ) )
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
;

let fieldsData = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalFieldsData.json` ) )
    .filter( row => selectedFarms.includes( row.farm_id ) )
    .map( row => {
        row.farmDomain = farmStagesTable.find( entry => entry.farm_id == row.farm_id ).farmDomain;
        return row;
    } )
    .map( row => {
        let fields = Object.keys(row);
        fields.forEach( field => {
            if ( datePattern.test( row[field] ) ) {
                row[field] = new Date(row[field]);
            }
        } );
        return row;
    } )
;

let uploader = new GeneralFootprint( {
    entityFootprints: fields_encoding.footprints,
    populators: fields_encoding.populators,
    aggregatorKey: aggregatorKey
} );

// let encodedData = uploader.encode({
//     dataset: fieldsData,
//     externalEntitiesArray: preexistingEntities,
// });

// let uploadOperations = await uploader.upload({
//     encodedData: encodedData,
//     dryRun: true
// })
// ;

let exampleEntry = fieldsData.find( d => d.farm_id == 'PA00013' );

// let summaries = formatOperationDeliverables({encodedData: encodedData, uploadOperations: uploadOperations, currentStage: currentStage, datasetName: "fields", operationRegistersFolder:operationRegistersFolder});

// summaries.flatResults.filter( d => d.status != 201 ).length;

let table = synthesisTable({ dataRow: exampleEntry, uploader: uploader, preexistingEntities: preexistingEntities, encoding: fields_encoding, datasetName: "fields", currentStage: currentStage, operationRegistersFolder: operationRegistersFolder });


let preexistingEntitiesBendingBridge = JSON.parse(fs.readFileSync(`${operationRegistersFolder}/${currentStage}/farms_bending_bridge_upload.json` ) )
    .summaries
    .flatResults
        .filter( d => [201, 409].includes( d.status ) )
        .map( d => {
            let output = d.entity.data;
            output.farmDomain = d.farmos_url;
            return output;
        } )
;

let uploadBendingBridge = await uploadAndRegister({
    uploader: uploader,
    currentStage: currentStage,
    datasetName: `${ datasetName }_bending_bridge`,
    operationRegisterFolder: operationRegistersFolder,
    dataset: fieldsData.filter( d => d.farm_id == "PA00001" ),
    preexistingEntities: preexistingEntitiesBendingBridge,
    farmsStageAssignment: farmStagesTable,
    dryRun: true,
});
