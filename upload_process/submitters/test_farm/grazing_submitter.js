// This script should read data in the appropirate CSV and send it into a FarmOS instance in the right pre established format.
const papa = require('papaparse');
const fs = require('fs');
const { randomUUID } = require('crypto');
const {
    printResults,
    ExternalRelationshipDescription,
    Matcher,
    EntityFootprint,
    GeneralFootprint
} = require('farm_os_entity_uploader');
const {
    selectedFarms,
    actualToDemoFarmDict,
    farmDomainDict,
    datePattern
} = require('../artifacts.js');
const {
    uploadAndRegister,
    synthesisTable
} = require('../utilities.js');

// data design
const grazing_encoding = require('../../encodings/grazing_data_encoding');

const exampleFarm  = "ourscitest.farmos.net";

const currentStage = "test_farm";
const datasetName = "grazing";

require('dotenv').config({ path:`${__dirname}/../../../.env` });

const aggregatorKey = process.env.FARMOS_KEY;

const operationRegistersFolder = `${__dirname}/../../operation_registers`;
const farmStagesTable = JSON.parse( fs.readFileSync("../farmsStageAssignment.json") )
      .filter( d => !d.production )
    ;

// We need to reference entities from previous upload processes.
let preexistingFields = JSON.parse(fs.readFileSync(`${operationRegistersFolder}/${currentStage}/fields_upload.json`))
    .summaries
    .flatResults
    .filter(d => d.status !== "404")
    .map(d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    })
    ;

let preexistingPlantings = JSON.parse(fs.readFileSync(`${operationRegistersFolder}/${currentStage}/plantings_upload.json`))
    .summaries
    .flatResults
    .filter(d => d.status !== "404")
    .map(d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    })
    ;
let preexistingEntities = [
    ...preexistingFields,
    ...preexistingPlantings
];

let grazing = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalGrazingData.json` ) )
    .filter( d => selectedFarms.includes( d.farm_name ) )
    .map( row => {
        row.farmDomain = exampleFarm;
        return row;
    } )
    .map( row => {
        let fields = Object.keys(row);
        fields.forEach( field => {
            if ( datePattern.test( row[field] ) ) {
                row[field] = new Date(row[field]);
            }
        } );
        return row;
    } )
;


let uploader = new GeneralFootprint( {
    entityFootprints: grazing_encoding.footprints,
    populators: grazing_encoding.populators,
    aggregatorKey: aggregatorKey
} );


let upload = await uploadAndRegister({
    uploader: uploader,
    currentStage: currentStage,
    datasetName: datasetName,
    operationRegisterFolder: operationRegistersFolder,
    dataset: grazing,
    preexistingEntities: preexistingEntities,
    farmsStageAssignment: farmStagesTable,
    dryRun: true
});

let table = synthesisTable({
    dataRow: grazing[12],
    uploader: uploader,
    preexistingEntities: preexistingEntities,
    encoding: grazing_encoding,
    datasetName: datasetName,
    currentStage: currentStage,
    operationRegistersFolder: operationRegistersFolder
});

