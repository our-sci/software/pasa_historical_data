// This script should read data in the appropirate CSV and send it into a FarmOS instance in the right pre established format.
// In this case, we will submit entities representing whole farms, that will be roots of the whole data structure representing the operation.
// Our intention with this encoding is to have a root entity that encompasses all the farm data, because we are considering grouping many farms into a single FarmOS instance in the future, and this would be a good way to separate them and have control of what belongs to each individual physical farm.
const papa = require('papaparse');
const fs = require('fs');
const { randomUUID } = require('crypto');
const {
    printResults,
    GeneralFootprint,
    ExternalRelationshipDescription,
    Matcher,
    EntityFootprint
} = require('farm_os_entity_uploader');
const {
    synthesisTable,
    uploadAndRegister
} = require('../utilities.js');
const {
    selectedFarms,
    actualToDemoFarmDict,
    farmDomainDict,
    datePattern
} = require('../artifacts.js');

let farms_encoding = require(`${__dirname}/../../encodings/farms_data_encoding.js`);

const currentStage = "test_farm";
const datasetName = "farms";

const operationRegistersFolder = `${__dirname}/../../operation_registers`;
const farmStagesTable = JSON.parse( fs.readFileSync("../farmsStageAssignment.json") );


require('dotenv').config({ path:`${__dirname}/../../../.env` });
const aggregatorKey = process.env.FARMOS_KEY;
const exampleFarm = 'ourscitest.farmos.net';

let farmsData = JSON.parse( fs.readFileSync( `${__dirname}/../../operation_registers/finalFarmsOrganizationData.json` ) )
    .map( row => {
        let fields = Object.keys(row);
        fields.forEach( field => {
            if ( datePattern.test( row[field] ) ) {
                row[field] = new Date(row[field]);
            }
        } );
        return row;
    } )
    .filter( row => selectedFarms.includes( row.farm_name ) )
    .map( ent => {
        // marking test farm
        ent.farmDomain = exampleFarm;
        return ent;
    } );


let uploader = new GeneralFootprint( {
    entityFootprints: farms_encoding.footprints,
    populators: farms_encoding.populators,
    aggregatorKey: aggregatorKey
} );

let encodedData = uploader.encode({
    dataset:farmsData,
    externalEntitiesArray:[],
});

let upload = uploadAndRegister({
    uploader: uploader,
    currentStage: currentStage,
    datasetName: datasetName,
    operationRegisterFolder: operationRegistersFolder,
    dataset: farmsData,
    preexistingEntities: [],
    farmsStageAssignment: farmStagesTable,
    dryRun: true
});


let exampleEntry = farmsData.find( d => d.farm_id == "PA00094" );
let table = synthesisTable({ dataRow: exampleEntry, uploader: uploader, preexistingEntities: [], encoding: farms_encoding, datasetName: "farms", currentStage: currentStage, operationRegistersFolder: operationRegistersFolder });
