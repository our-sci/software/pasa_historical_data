// This script should read data in the appropirate CSV and send it into a FarmOS instance in the right pre established format.
const papa = require('papaparse');
const fs = require('fs');
const { randomUUID } = require('crypto');
const {
    printResults,
    ExternalRelationshipDescription,
    Matcher,
    EntityFootprint,
    GeneralFootprint
} = require('farm_os_entity_uploader');
const {
    selectedFarms,
    actualToDemoFarmDict,
    farmDomainDict,
    datePattern
} = require('../artifacts.js');
const {
    uploadAndRegister,
    synthesisTable
} = require('../utilities.js');

// data design
const tillage_encoding = require('../../encodings/tillage_data_encoding');

const exampleFarm  = "ourscitest.farmos.net";

const currentStage = "test_farm";
const datasetName = "tillage";

require('dotenv').config({ path:`${__dirname}/../../../.env` });

const aggregatorKey = process.env.FARMOS_KEY;

const operationRegistersFolder = `${__dirname}/../../operation_registers`;
const farmStagesTable = JSON.parse( fs.readFileSync("../farmsStageAssignment.json") )
      .filter( d => !d.production )
;



// We need to reference entities from previous upload processes.
let preexistingFields = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${currentStage}/fields_upload.json` ) )
    .summaries
    .flatResults
    .filter( d => d.status !== "404" )
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
;

let preexistingPlantings = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${currentStage}/plantings_upload.json` ) )
    .summaries
    .flatResults
    .filter( d => d.status !== "404" )
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
;
let preexistingEntities = [
    ... preexistingFields,
    ... preexistingPlantings
];

// TODO to send to testing instead of actual target farm
// tillage.forEach( ent => ent.farmDomain = exampleFarm );
// preexistingEntities.forEach( ent => ent.farmDomain = exampleFarm );

let tillage = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalTillageData.json` ) )
    .filter( d => selectedFarms.includes( d.farm_name ) )
    .map( row => {
        row.farmDomain = exampleFarm;
        return row;
    } )
    .map( row => {
        let fields = Object.keys(row);
        fields.forEach( field => {
            if ( datePattern.test( row[field] ) ) {
                row[field] = new Date(row[field]);
            }
        } );
        return row;
    } )
;


let uploader = new GeneralFootprint( {
    entityFootprints: tillage_encoding.footprints,
    populators: tillage_encoding.populators,
    aggregatorKey: aggregatorKey
} );


let upload = await uploadAndRegister({
    uploader: uploader,
    currentStage: currentStage,
    datasetName: datasetName,
    operationRegisterFolder: operationRegistersFolder,
    dataset: tillage,
    preexistingEntities: preexistingEntities,
    farmsStageAssignment: farmStagesTable,
    dryRun: true
});

let table = synthesisTable({
    dataRow: tillage[12],
    uploader: uploader,
    preexistingEntities: preexistingEntities,
    encoding: tillage_encoding,
    datasetName: datasetName,
    currentStage: currentStage,
    operationRegistersFolder: operationRegistersFolder
});


// let intendedEntitiesRepeated = upload.encodedData
//     .payloads[exampleFarm]
//     .filter( d => !d.submitter_attributes.empty )
//     .filter( d => !d.submitter_attributes.void )
//     .map( d => d.id )
// ;

// let intendedEntities = Array.from(new Set(intendedEntitiesRepeated));

// let attemptedEntities = upload.summaries.flatResults.map( d => d.entity.data.id );

// let notAttemptedEntitiesIDs = intendedEntities.filter( d => !attemptedEntities.includes(d) );

// let notAttemptedEntities = upload.encodedData.payloads[exampleFarm].filter( d => notAttemptedEntitiesIDs.includes( d.id ) );


// let missingCat = upload.uploadsNeedingReview[0].entity.data.relationships.category.data[0].id;
// upload.summaries.flatResults.find( d => d.entity.data.id == missingCat );
// let missingAssets = upload.uploadsNeedingReview[0].entity.data.relationships.asset.data.map( d => d.id );
// upload.summaries.flatResults.find( d => missingAssets.includes( d.entity.data.id ) );
// preexistingEntities.find( d => missingAssets.includes( d.id ));
// let missingQuantity = upload.uploadsNeedingReview[0].entity.data.relationships.quantity.data[0].id;
// upload.summaries.flatResults.find( d => d.entity.data.id == missingQuantity );

// let culprit = 'b2592c2b-03c0-4bcd-8786-87af1dcb2f12';
// upload.encodedData.payloads[exampleFarm].find( d => d.id == culprit );
