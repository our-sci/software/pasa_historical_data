// This script should read data in the appropirate CSV and send it into a FarmOS instance in the right pre established format.
const papa = require('papaparse');
const fs = require('fs');
const { randomUUID } = require('crypto');
const {
    printResults,
    ExternalRelationshipDescription,
    Matcher,
    EntityFootprint,
    GeneralFootprint
} = require('farm_os_entity_uploader');
const {
    selectedFarms,
    actualToDemoFarmDict,
    farmDomainDict,
    datePattern
} = require('../artifacts.js');
const {
    uploadAndRegister,
    synthesisTable
} = require('../utilities.js');

const inputs_encoding = require('../../encodings/organic_inputs_data_encoding');

const exampleFarm  = "ourscitest.farmos.net";

const currentStage = "test_farm";
const datasetName = "inputs";

require('dotenv').config({ path:'../../../.env' });
const aggregatorKey = process.env.FARMOS_KEY;
const operationRegistersFolder = `${__dirname}/../../operation_registers`;
const farmStagesTable = JSON.parse( fs.readFileSync("../farmsStageAssignment.json") )
      .filter( d => !d.production )
;

let inputs = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalInputData.json` ) )
    .filter( d => selectedFarms.includes( d.farm_name ) )
    .map( row => {
        row.farmDomain = exampleFarm;
        return row;
    } )
    .map( row => {
        let fields = Object.keys(row);
        fields.forEach( field => {
            if ( datePattern.test( row[field] ) ) {
                row[field] = new Date(row[field]);
            }
        } );
        return row;
    } )
;


// We need to reference entities from previous upload processes.
let preexistingFields = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${currentStage}/fields_upload.json` ) )
    .summaries
    .flatResults
    .filter( d => d.status !== "404" )
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
    .map( d => {
        d.farmDomain = exampleFarm;
        return d;
    } )
;
let preexistingPlantings = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${currentStage}/plantings_upload.json` ) )
    .summaries
    .flatResults
    .filter( d => d.status !== "404" )
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
    .map( d => {
        d.farmDomain = exampleFarm;
        return d;
    } )
;
let preexistingEntities = [ ... preexistingPlantings, ... preexistingFields ];


// TODO to send to testing instead of actual target farm
inputs.forEach( ent => ent.farmDomain = "ourscitest.farmos.net" );
preexistingEntities.forEach( ent => ent.farmDomain = "ourscitest.farmos.net" );


// upload process

// let cleaningOperation = cleanTestInstance(aggregatorKey);

let uploader = new GeneralFootprint( {
    entityFootprints: inputs_encoding.footprints,
    populators: inputs_encoding.populators,
    aggregatorKey: aggregatorKey,
    externalEntitiesArray: preexistingEntities,
} );


let upload = await uploadAndRegister({
    uploader: uploader,
    currentStage: currentStage,
    datasetName: datasetName,
    operationRegisterFolder: operationRegistersFolder,
    dataset: inputs,
    preexistingEntities: preexistingEntities,
    farmsStageAssignment: farmStagesTable,
    dryRun: true
});

let table = synthesisTable({
    dataRow: inputs[12],
    uploader: uploader,
    preexistingEntities: preexistingEntities,
    encoding: inputs_encoding,
    datasetName: datasetName,
    currentStage: currentStage,
    operationRegistersFolder: operationRegistersFolder
});
