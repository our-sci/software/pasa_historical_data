// This script should read data in the appropirate CSV and send it into a FarmOS instance in the right pre established format.
const papa = require('papaparse');
const fs = require('fs');
const { randomUUID } = require('crypto');
const {
    printResults,
    GeneralFootprint,
    ExternalRelationshipDescription,
    Matcher,
    EntityFootprint
} = require('farm_os_entity_uploader');
const {
    selectedFarms,
    actualToDemoFarmDict,
    farmDomainDict,
    datePattern
} = require('../artifacts.js');
const {
    uploadAndRegister,
    synthesisTable
} = require('../utilities.js');

// data design
const plantings_encoding = require('../../encodings/plantings_data_encoding');

const currentStage = "test_farm";
const datasetName = "plantings";

require('dotenv').config({ path:`${__dirname}/../../../.env` });

const aggregatorKey = process.env.FARMOS_KEY;
const exampleFarm  = "ourscitest.farmos.net";

const operationRegistersFolder = `${__dirname}/../../operation_registers`;
const farmStagesTable = JSON.parse( fs.readFileSync("../farmsStageAssignment.json") );

let preexistingFields = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${currentStage}/fields_upload.json` ) )
    .summaries
    .flatResults
    .filter( d => d.status !== "404" )
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
    .map( d => {
        d.farmDomain = exampleFarm;
        return d;
    } )
;

let preexistingCategories = JSON.parse(fs.readFileSync( `${operationRegistersFolder}/${currentStage}/categories_upload.json` ))
    .summaries
    .flatResults
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
;

let allPlantings = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalPlantingData.json` ) )
    .map( row => {
        let fields = Object.keys(row);
        fields.forEach( field => {
            if ( datePattern.test( row[field] ) ) {
                row[field] = new Date(row[field]);
            }
        } );
        return row;
    } )
    .filter( row => selectedFarms.includes( row.farm_name ) )
// adding the active attribute
    .map( ent => {
        ent.status = ent.survey_year == "2022" ? "active" : "archived";
        return ent;
    } )
    .map( ent => {
        // marking test farm
        ent.farmDomain = exampleFarm;
        ent.bed_width_e2e = 3,
        ent.bed_width_c2c = 2,
        // adding bed_width_ft for testing
        ent.seed_price = 23;
        return ent;
} );


let uploader = new GeneralFootprint( {
    entityFootprints: plantings_encoding.footprints,
    populators: plantings_encoding.populators,
    aggregatorKey: aggregatorKey
} );

let upload = await uploadAndRegister({
    uploader: uploader,
    currentStage: currentStage,
    datasetName: datasetName,
    operationRegisterFolder: operationRegistersFolder,
    dataset: allPlantings,
    preexistingEntities: [ ... preexistingFields, ... preexistingCategories ],
    farmsStageAssignment: farmStagesTable,
    dryRun: false
});

let table = synthesisTable({
    dataRow: allPlantings[0],
    uploader: uploader,
    preexistingEntities: [ ... preexistingFields, ... preexistingCategories ],
    encoding: plantings_encoding,
    datasetName: "plantings",
    currentStage: currentStage,
    operationRegistersFolder: operationRegistersFolder
});


let intendedEntitiesRepeated = upload.encodedData
    .payloads[exampleFarm]
    .filter( d => !d.submitter_attributes.empty )
    .filter( d => !d.submitter_attributes.void )
    .map( d => d.id )
;

let intendedEntities = Array.from(new Set(intendedEntitiesRepeated));

let attemptedEntities = upload.summaries.flatResults.map( d => d.entity.data.id );

let notAttemptedEntitiesIDs = intendedEntities.filter( d => !attemptedEntities.includes(d) );

let notAttemptedEntities = upload.encodedData.payloads[exampleFarm].filter( d => notAttemptedEntitiesIDs.includes( d.id ) );
