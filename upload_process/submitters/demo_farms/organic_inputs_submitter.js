// This script should read data in the appropirate CSV and send it into a FarmOS instance in the right pre established format.
const papa = require('papaparse');
const fs = require('fs');
const { randomUUID } = require('crypto');
const {
    printResults,
    ExternalRelationshipDescription,
    Matcher,
    EntityFootprint,
    GeneralFootprint
} = require('farm_os_entity_uploader');
const {
    actualToDemoFarmDict,
    farmDomainDict,
    datePattern
} = require('../artifacts.js');
const {
    uploadAndRegister,
    synthesisTable
} = require('../utilities.js');

const inputs_encoding = require('../../encodings/organic_inputs_data_encoding');

const currentStage = "demo_farms";
const datasetName = "inputs";

require('dotenv').config({ path: '../../../.env' });
const aggregatorKey = process.env.FARMOS_KEY;
const operationRegistersFolder = `${__dirname}/../../operation_registers`;
const farmStagesTable = JSON.parse(fs.readFileSync("../farmsStageAssignment.json"))
    .filter(d => !d.production)
    ;
let currentStageFarms = farmStagesTable
    .filter(d => !d.production)
    .filter(d => d.stage == currentStage)
    ;
let selectedFarmIDs = currentStageFarms.map( d => d.farm_id );

let inputs = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalInputData.json` ) )
    .filter( row => selectedFarmIDs.includes( row.farm_id ) )
    .map( row => {
        let fields = Object.keys(row);
        fields.forEach( field => {
            if ( datePattern.test( row[field] ) ) {
                row[field] = new Date(row[field]);
            }
        } );
        row.farmDomain = currentStageFarms.find(farm => farm.farm_id == row.farm_id).farmDomain;
        return row;
    })
    ;


// We need to reference entities from previous upload processes.
let preexistingFields = JSON.parse(fs.readFileSync(`${operationRegistersFolder}/${currentStage}/fields_upload.json`))
    .summaries
    .flatResults
    .filter(d => d.status !== "404")
    .map(d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
;
let preexistingPlantings = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${currentStage}/plantings_upload.json` ) )
    .summaries
    .flatResults
    .filter( d => d.status !== "404" )
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
;
let preexistingEntities = [ ... preexistingPlantings, ... preexistingFields ];


// upload process

// let cleaningOperation = cleanTestInstance(aggregatorKey);

let uploader = new GeneralFootprint( {
    entityFootprints: inputs_encoding.footprints,
    populators: inputs_encoding.populators,
    aggregatorKey: aggregatorKey,
    externalEntitiesArray: preexistingEntities,
} );

fs.writeFileSync(`${operationRegistersFolder}/demo_farms/source_inputs_data.json`, JSON.stringify(inputs));

let upload = await uploadAndRegister({
    uploader: uploader,
    currentStage: currentStage,
    datasetName: datasetName,
    operationRegisterFolder: operationRegistersFolder,
    dataset: inputs,
    preexistingEntities: preexistingEntities,
    farmsStageAssignment: farmStagesTable,
    dryRun: true
});

let table = synthesisTable({
    dataRow: inputs[12],
    uploader: uploader,
    preexistingEntities: preexistingEntities,
    encoding: inputs_encoding,
    datasetName: datasetName,
    currentStage: currentStage,
    operationRegistersFolder: operationRegistersFolder
});


// prepare an example for our PASA SHBS schema development
let exampleSource = inputs[12];
let examplePreexistingEntities = preexistingEntities.filter(d => d.farmDomain == exampleSource.farmDomain );
exampleSource.farmDomain = "example.farmos.net";
exampleSource.farm_name = "example farm";
examplePreexistingEntities.forEach(d => d.farmDomain = "example.farmos.net");
let example = uploader
    .encode({ dataset: [exampleSource], externalEntitiesArray: examplePreexistingEntities })
    .payloads['example.farmos.net']
    ;
let relatedEntityIDs = example
    .flatMap( d => d.relationships ? Object.entries( d.relationships ) : [] )
    .flatMap( d => d[1].data ).map( d => d.id )
;
let relatedEntities = preexistingEntities.filter( d => relatedEntityIDs.includes(d.id) );
relatedEntities.find( d => d.type == "asset--land" ).attributes.intrinsic_geometry = 'POINT (-76 40)';
fs.writeFileSync(`${operationRegistersFolder}/inputExampleForSchema.json`, JSON.stringify([... example, ... relatedEntities]));
