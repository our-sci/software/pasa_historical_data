const {
    cleanTestInstance,
    cleanDemoInstance
} = require("/home/oc/OurSci/mirror_farmos/src/nightly_build")
;

const {
    deleteUUIDsInUploadLog
} = require("farm_os_entity_uploader")
    ;
let coffeeshopAggregatorURL = "https://surveystack.farmos.group/api/v2";

require('dotenv').config({ path: `${__dirname}/../../../.env` });
const aggregatorKey = process.env.FARMOS_KEY;
const farmStagesTable = JSON.parse(fs.readFileSync("../farmsStageAssignment.json"));


const demoFarms = farmStagesTable
    .filter(d => !d.production)
    .filter(d => /demo/.test(d.farmDomain))
    .map(d => d.farmDomain)
    ;

let registersToClean = [
    // "tillage",
    // "inputs",
    // "grazing",
    // "plantings",
    // "fields",
    // "farms",
    // "categories"
];
let cleaningResults = {};
for (let i = 0; i < registersToClean.length; i++) {
    let currentRegister = registersToClean[i];
    let register = JSON.parse( fs.readFileSync( `/home/oc/OurSci/pasa_historical_data/upload_process/operation_registers/demo_farms/${currentRegister}_upload.json` ) )
            .summaries
            .flatResults
        .filter( d => demoFarms.includes( d.farmos_url ) )
        ;
    let deleteOp = await deleteUUIDsInUploadLog({
        uploadLog: register.map( d => { d.marked_for_removal = true; return d; } ),
        farmOSKey: aggregatorKey,
        aggregatorURL: coffeeshopAggregatorURL
    } );
    let resultStatuses = Array.from( new Set( deleteOp.operations.flatMap( op => op.deleteOperations?.map( d => d.status ) ) ) );
    let statusAmounts = resultStatuses.map( code => {
    let output = {
        status: code,
        amount: deleteOp.operations.flatMap(op => op.deleteOperations?.filter( d => d.status == code )).length
    };
        return output;
    } );
    // console.log("fed entities");
    // console.log(register.length);
    console.log("status amounts");
    console.log(statusAmounts);
    cleaningResults[currentRegister] = {
        amounts: statusAmounts,
        details: resultStatuses,
        register: currentRegister
    };
};

// let bruteCleaningResults = [];
// for (let i = 0; i < demoFarms.length; i++ ) {
//     let currentFarmDomain = demoFarms[i];
//     let currentOperation = await cleanDemoInstance( { farmOSKey: aggregatorKey, instance: currentFarmDomain } );
//     bruteCleaningResults.push(currentOperation);
// };
