// This script should read data in the appropirate CSV and send it into a FarmOS instance in the right pre established format.
// In this case, we will link fields with previously existing land assets representing whole farms, in order to get a meaningful hierarchy organizing the data, as well as a good way to unify all fields into a unique instance in he future if needed, and still have a global entity representing individual farms.
const papa = require('papaparse');
const fs = require('fs');
const {
    printResults,
    GeneralFootprint,
    ExternalRelationshipDescription,
    Matcher,
    EntityFootprint
} = require('farm_os_entity_uploader');
const {
    synthesisTable,
    uploadAndRegister
} = require('../utilities.js');
const {
    actualToDemoFarmDict,
    farmDomainDict,
    datePattern
} = require('../artifacts.js');

// Our data design.
let fields_encoding = require("../../encodings/fields_data_encoding.js");

require('dotenv').config({ path:`${__dirname}/../../../.env` });
const aggregatorKey = process.env.FARMOS_KEY;


const currentStage = "demo_farms";
const datasetName = "fields";
const operationRegistersFolder = `${__dirname}/../../operation_registers`;
const farmStagesTable = JSON.parse( fs.readFileSync("../farmsStageAssignment.json") );
let currentStageFarms = farmStagesTable
    .filter( d => !d.production )
    .filter( d => d.stage == currentStage )
;
let selectedFarmDomains = currentStageFarms.map( d => d.farmDomain );

// We need to reference entities from previous upload processes (the assets representing whole farms).
let preexistingFarms = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${currentStage}/farms_upload.json` ) )
    .summaries
    .flatResults
    .filter( d => [201, 409].includes( d.status ) )
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
;

let fieldsData = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalFieldsData.json` ) )
    .map( d => {
        d.farmDomain = farmDomainDict[d.farm_name];
        return d;
    } )
    .map( row => {
        let fields = Object.keys(row);
        fields.forEach( field => {
            if ( datePattern.test( row[field] ) ) {
                row[field] = new Date(row[field]);
            }
        } );
        return row;
    } )
    // grab selected farms only
    .filter( row => selectedFarmDomains.includes( row.farmDomain ) )
;

let uploader = new GeneralFootprint( {
    entityFootprints: fields_encoding.footprints,
    populators: fields_encoding.populators,
    aggregatorKey: aggregatorKey
} );

let upload = await uploadAndRegister({
    uploader: uploader,
    currentStage: currentStage,
    datasetName: datasetName,
    operationRegisterFolder: operationRegistersFolder,
    dataset: fieldsData,
    preexistingEntities: preexistingFarms,
    farmsStageAssignment: farmStagesTable,
    dryRun: true
});

let exampleEntry = fieldsData.find( d => d.farm_id == 'PA00094' );
let table = synthesisTable({ dataRow: exampleEntry, uploader: uploader, preexistingEntities: preexistingFarms, encoding: fields_encoding, datasetName: "fields", currentStage: currentStage, operationRegistersFolder: operationRegistersFolder });
