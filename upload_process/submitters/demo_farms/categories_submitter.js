// This step will submit some hierarchical taxonomy terms that will allow us to order all other taxonomy terms as needed.
const papa = require('papaparse');
const fs = require('fs');
const { randomUUID } = require('crypto');
const {
    printResults,
    GeneralFootprint,
    ExternalRelationshipDescription,
    Matcher,
    EntityFootprint
} = require('farm_os_entity_uploader');
const {
    selectedFarms,
    actualToDemoFarmDict,
    farmDomainDict,
    datePattern
} = require('../artifacts.js');
const {
    uploadAndRegister,
    synthesisTable
} = require('../utilities.js');

// data design
const categories_encoding = require('../../encodings/categories_data_encoding');

const currentStage = "demo_farms";
const datasetName = "categories";

require('dotenv').config({ path:`${__dirname}/../../../.env` });

const aggregatorKey = process.env.FARMOS_KEY;

const operationRegistersFolder = `${__dirname}/../../operation_registers`;
const farmStagesTable = JSON.parse( fs.readFileSync("../farmsStageAssignment.json") );

let dataSkeleton = [
    {
        name: "pasa_crop_type",
        description: "Groups crops by their agricultural practices",
        example_members: [ "cash_crop", "row_crop", "diversified_vegetables" ],
        type: "taxonomy_term--plant_type",
    },
    {
        name: "pasa_species",
        description: "Species as categorized by pasa",
        example_members: [ "wheat", "hay" ],
        type: "taxonomy_term--plant_type",
    },
    {
        name: "farmer_species",
        description: "Species as named by the farmer",
        example_members: [ "wheat", "hay" ],
        type: "taxonomy_term--plant_type",
    }
];

let data = dataSkeleton.flatMap( entity => {
    let allFarmEntities = farmStagesTable
        .filter( d => d.stage == currentStage )
        .map( farm => {
            let farmEntity = structuredClone(entity);
            farmEntity.farmDomain = farm.farmDomain;
            return farmEntity;
        } );
    return allFarmEntities;
} );


let uploader = new GeneralFootprint( {
    entityFootprints: categories_encoding.footprints,
    populators: categories_encoding.populators,
    aggregatorKey: aggregatorKey
} );

let upload = await uploadAndRegister({
    uploader: uploader,
    currentStage: currentStage,
    datasetName: datasetName,
    operationRegisterFolder: operationRegistersFolder,
    dataset: data,
    preexistingEntities: [],
    farmsStageAssignment: farmStagesTable,
    dryRun: true
});
