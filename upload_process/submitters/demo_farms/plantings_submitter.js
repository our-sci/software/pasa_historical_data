// This script should read data in the appropirate CSV and send it into a FarmOS instance in the right pre established format.
const papa = require('papaparse');
const fs = require('fs');
const { randomUUID } = require('crypto');
const {
    printResults,
    GeneralFootprint,
    ExternalRelationshipDescription,
    Matcher,
    EntityFootprint
} = require('farm_os_entity_uploader');
const {
    uploadAndRegister,
    synthesisTable
} = require('../utilities.js');
const {
    actualToDemoFarmDict,
    farmDomainDict,
    datePattern
} = require('../artifacts.js');

// data design
const plantings_encoding = require('../../encodings/plantings_data_encoding');


require('dotenv').config({ path:`${__dirname}/../../../.env` });
const aggregatorKey = process.env.FARMOS_KEY;

const currentStage = "demo_farms";
const operationRegistersFolder = `${__dirname}/../../operation_registers`;
const farmStagesTable = JSON.parse( fs.readFileSync("../farmsStageAssignment.json") );
let currentStageFarms = farmStagesTable
    .filter(d => !d.production)
    .filter(d => d.stage == currentStage)
    ;
let selectedFarmIDs = currentStageFarms.map( d => d.farm_id );
const datasetName = "plantings";

let preexistingFields = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${currentStage}/fields_upload.json` ) )
    .summaries
    .flatResults
    .filter( d => [201, 409].includes( d.status ) )
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
;

let preexistingCategories = JSON.parse(fs.readFileSync( `${operationRegistersFolder}/${currentStage}/categories_upload.json` ))
    .summaries
    .flatResults
    .map( d => {
        let output = d.entity.data;
        output.farmDomain = d.farmos_url;
        return output;
    } )
;

let allPlantings = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/finalPlantingData.json` ) )
    .filter( row => selectedFarmIDs.includes( row.farm_id ) )
    .filter( row => row.crop_name.some( name =>  name != "none" ) )
    .map( row => {
        let fields = Object.keys(row);
        fields.forEach( field => {
            if ( datePattern.test( row[field] ) ) {
                row[field] = new Date(row[field]);
            }
        } );
        // assign url
        row.farmDomain = currentStageFarms.find( farm => farm.farm_id == row.farm_id ).farmDomain;
        // WARNING don't do this in non demo farms.
        if (!row.farmDomain.includes("demo")) {
            throw new Error("You are adding random data meant to test demo data into a publicly visible farm.");
        } else {
            // no entries in the demo farm have bed width, so we will randomly add it to some
            row.bed_width_ft = Math.random > 0.5 ? Math.random().toFixed(1).split(".")[1] : undefined;
        }
        return row;
    } )
    .map( ent => {
        ent.status = ent.survey_year == "2023" ? "active" : "archived";
        return ent;
    } )
;

fs.writeFileSync(`${operationRegistersFolder}/demo_farms/source_planting_data.json`, JSON.stringify(allPlantings));

let preexistingEntities = [ ... preexistingFields, ... preexistingCategories ];

let uploader = new GeneralFootprint( {
    entityFootprints: plantings_encoding.footprints,
    populators: plantings_encoding.populators,
    aggregatorKey: aggregatorKey
} );


let upload = await uploadAndRegister({
    uploader: uploader,
    currentStage: currentStage,
    datasetName: datasetName,
    operationRegisterFolder: operationRegistersFolder,
    dataset: allPlantings,
    preexistingEntities: preexistingEntities,
    farmsStageAssignment: farmStagesTable,
    dryRun: true
});

// let upload = JSON.parse( fs.readFileSync( `${operationRegistersFolder}/${ currentStage }/${datasetName}_upload.json` ) );



// We have different encoding behaviors according to the presence of information confirming a harvest happened, versus any other kind of termination.
let tableHarvest = synthesisTable({ dataRow: allPlantings.find( d => d.harvest_date ),
                                    uploader: uploader,
                                    preexistingEntities: preexistingEntities,
                                    encoding: plantings_encoding,
                                    datasetName: "plantings_harvest",
                                    currentStage: currentStage,
                                    operationRegistersFolder: operationRegistersFolder
                                  });

let tableTermination = synthesisTable({ dataRow: allPlantings.find( d => d.term_date ),
                             uploader: uploader,
                             preexistingEntities: preexistingEntities,
                             encoding: plantings_encoding,
                             datasetName: "plantings_termination",
                             currentStage: currentStage,
                             operationRegistersFolder: operationRegistersFolder
});

// prepare an example for our PASA SHBS schema development
let exampleSource = allPlantings[12];
let examplePreexistingEntities = preexistingEntities.filter(d => d.farmDomain == exampleSource.farmDomain );
exampleSource.farmDomain = "example.farmos.net";
exampleSource.farm_name = "example farm";
examplePreexistingEntities.forEach(d => d.farmDomain = "example.farmos.net");
let example = uploader
    .encode({ dataset: [exampleSource], externalEntitiesArray: examplePreexistingEntities })
    .payloads['example.farmos.net']
    ;
let relatedEntityIDs = example
    .flatMap(d => d.relationships ? Object.entries(d.relationships) : [])
    .flatMap(d => d[1].data).map(d => d.id)
    ;
let relatedEntities = preexistingEntities.filter(d => relatedEntityIDs.includes(d.id));
relatedEntities.find(d => d.type == "asset--land").attributes.intrinsic_geometry = { value: 'POINT (-76 40)' };
fs.writeFileSync(`${operationRegistersFolder}/plantingExampleForSchema.json`, JSON.stringify([...example, ...relatedEntities]));
