const papa = require('papaparse');
const fs = require('fs');

function synthesisTable({dataRow, uploader, preexistingEntities, encoding, datasetName, currentStage, operationRegistersFolder}) {
    let exampleEntry = dataRow;
    let encodedExample = uploader.encode({
        dataset: [exampleEntry],
        externalEntitiesArray: preexistingEntities,
    })
        .payloads[exampleEntry.farmDomain]
    ;
    let synthesisTable = encoding
        .populators
        .map( d => {
            let exampleOutput = encodedExample.find( entity => entity.reference == d.entity );
            let outputValue = exampleOutput.attributes[d.attribute];
            let exampleValue = Array.isArray( d.source ) ? d.source.map( attr => exampleEntry[attr] ) : exampleEntry[d.source];
            return {
                table_column: Array.isArray( d.source ) ? d.source.join(", ") : d.source,
                host_entity: d.entity,
                attribute_in_host_entity: d.attribute,
                value_in_example: exampleValue,
                value_in_output: typeof( outputValue ) == "object" ? JSON.stringify(outputValue) : outputValue
            };
        } )
    ;

    fs.writeFileSync( `${operationRegistersFolder}/${currentStage}/${datasetName}ExampleTable.csv`, papa.unparse( synthesisTable ) );

    return synthesisTable;
};

function formatOperationDeliverables({encodedData, uploadOperations, datasetName, currentStage, operationRegistersFolder}) {
    let uploads = {
        payloads: {},
        operations: {}
    };

    Object.assign(uploads.payloads, encodedData.payloads);
    uploadOperations.operations.forEach( operation => {
        uploads.operations[operation.farm] = {};
        Object.assign(uploads.operations[operation.farm], operation);
    } );

    let results = Object.entries( uploads.operations ).flatMap( d => d[1].results );
    let allResults = results.flatMap( farm => farm.flatResults );
    if (allResults.length > 0) {
        fs.writeFileSync( `${operationRegistersFolder}/${currentStage}/${datasetName}_upload.json`, JSON.stringify(allResults), console.error );
    };
    return {
        results: results,
        flatResults: allResults,
    };
};

async function uploadAndRegister({
    uploader,
    currentStage,
    datasetName,
    operationRegisterFolder,
    dataset,
    preexistingEntities = [],
    farmsStageAssignment,
    dryRun = true,
    encodedData
}) {
    let registerFilename = `${operationRegisterFolder}/${currentStage}/${datasetName}_upload.json`;
    console.log(`Checking if ${registerFilename} exists: ${fs.existsSync(registerFilename)}.`);
    let output = {
        registerFilename: registerFilename
    };
    let currentFarms = farmsStageAssignment
        .filter( d => d.stage == currentStage )
        .map( d => d.farmDomain )
    ;
    currentFarms = Array.from(new Set(currentFarms));
    let notAssignedFarms = dataset.filter( d => !currentFarms.includes(d.farmDomain) ).map( d => d.farmDomain );
    if (notAssignedFarms.length > 0) {
        console.log(new Set( notAssignedFarms ));
        throw new Error(`Some entries in the dataset belong to farms not associated with this stage.`);
    };
    if (!fs.existsSync(registerFilename)) {
        if (encodedData) {
            output.encodedData = encodedData;
        } else {
            output.encodedData = uploader.encode({
                dataset: dataset,
                externalEntitiesArray: preexistingEntities
            });
        };

        output.uploadOperations = await uploader.upload({
            encodedData: output.encodedData,
            dryRun: dryRun
        })
        ;
        output.summaries = formatOperationDeliverables({
            encodedData: output.encodedData,
            uploadOperations: output.uploadOperations,
            currentStage: currentStage,
            datasetName: datasetName,
            operationRegistersFolder: operationRegisterFolder
        })
        ;
        output.uploadsNeedingReview = output.summaries.flatResults.filter( d => ![201, 409].includes( d.status ) );
        output.potential_failures_amount = output.uploadsNeedingReview.length;

        if (!dryRun) {
            fs.writeFileSync( registerFilename, JSON.stringify(output) );
        };
    } else {
        throw new Error(`A register for this operation already exists in ${registerFilename}. If you are sure it is no longer relevant, you can erase it and try again.`);
    };
    return output;
};

exports.formatOperationDeliverables = formatOperationDeliverables;
exports.synthesisTable = synthesisTable;
exports.uploadAndRegister = uploadAndRegister;
