# Upload Process

## Summary

* Data is pre processed in the `data_preparation` scripts. There's roughly one script for each CSV table, though the addition of new batches sometimes requires adding small patches.
* In parallel, a data design should exist to transform the data from a flat CSV into a convention, which is expressed as the `data_encoding`. The uploader library has abundant documentation about this process.
* In order to control which parts of the process are ready, the uploader will refuse to upload entities if a register already exists for the same batch and data type. This prevents losing preexisging registers or repeating operations. To add data, you need to define new stages/data types.
* Farms need to have a stage assigned, which is done in the `generateByStageFarmsTable.js`. You can't arbitrarily point into farms. This is another security precaution.
