const {
    EntityFootprint,
    UniqueEntityFootprint,
    Populator,
    Matcher,
    GeneralFootprint,
    QuantityValuePopulator,
    ExternalRelationshipDescription
} = require('farm_os_entity_uploader');

function grazingNameGenerator({field, date, event}) {
    let nameString = `Grazing ${event}:`;
    nameString = nameString.concat(" ", field);
    if (date) {
        let formattedDate = date ? `${date.getUTCMonth() + 1}/${date.getUTCDate()}/${date.getFullYear()}` : undefined;
        nameString = nameString.concat(" ", formattedDate);
    };
    return nameString;

};

function grazingStartNameGenerator({field, date}) {
        return grazingNameGenerator({field:field, date: date, event: "start"});
};
function grazingEndNameGenerator({field, date}) {
        return grazingNameGenerator({field:field, date: date, event: "end"});
};
exports.grazingStartNameGenerator = grazingStartNameGenerator;
exports.grazingEndNameGenerator = grazingEndNameGenerator;

let footprintParameters = [
    {
        id_reference: "grazing_initiation_log",
        type: "log--activity",
        relationships: [
            {
                relation: "category",
                type: "taxonomy_term--log_category",
                target_id_reference: "grazing_log_category"
            },
            {
                relation: "location",
                type: "asset--land",
                matchersArray: [new Matcher({ attribute: "pasa_field_id", value_in_source: "field_id" })]
            },
            {
                relation: "asset",
                type: "asset--animal",
                target_id_reference: "herd_asset" 
            },
            {
                relation: 'quantity',
                type: 'quantity--standard',
                target_id_reference: `quantity_area`
            },
            {
                relation: "quantity",
                type: "quantity--standard",
                target_id_reference: "quantity_animal_number"
            },
            {
                relation: "quantity",
                type: "quantity--standard",
                target_id_reference: "quantity_subsections_involved"
            },
            {
                relation: "quantity",
                type: "quantity--standard",
                target_id_reference: "quantity_animal_weight"
            },
        ]
    },
    {
        id_reference: "grazing_ending_log",
        type: "log--activity",
        relationships: [
            {
                relation: "category",
                type: "taxonomy_term--log_category",
                target_id_reference: "grazing_log_category"
            },
            {
                relation: "asset",
                type: "asset--animal",
                target_id_reference: "herd_asset" 
            },
            {
                relation: 'quantity',
                type: 'quantity--standard',
                target_id_reference: `quantity_area`
            },
            {
                relation: "quantity",
                type: "quantity--standard",
                target_id_reference: "quantity_animal_number"
            },
            {
                relation: "quantity",
                type: "quantity--standard",
                target_id_reference: "quantity_subsections_involved"
            },
            {
                relation: "quantity",
                type: "quantity--standard",
                target_id_reference: "quantity_animal_weight"
            },
        ]
    },
    {
        id_reference: "herd_asset",
        type: "asset--animal",
        unique: true,
        relationships: [
            {
                relation: "animal_type",
                type: "taxonomy_term--animal_type",
                target_id_reference: "animal_type_taxonomy"
            },
        ],
        matchers: [
            new Matcher({ attribute: "name", value_in_source: "name" })
        ],
    },
    {
        id_reference: "animal_type_taxonomy",
        type: "taxonomy_term--animal_type",
        unique: true,
        matchers: [
            new Matcher({ attribute: "name", value_in_source: "name" })
        ],
    },
    {
        id_reference: "quantity_animal_number",
        type: "quantity--standard",
    },
    {
        id_reference: "grazing_log_category",
        type: "taxonomy_term--log_category",
        unique: true,
        matchers: [
            new Matcher({ attribute: "name", value_in_source: "name" })
        ],
    },
    {
        id_reference: "quantity_subsections_involved",
        type: "quantity--standard",
    },
    {
        id_reference: `quantity_area`,
        type: "quantity--standard",
        relationships: [
            {
                relation: 'units',
                type: 'taxonomy_term--unit',
                target_id_reference:`unit_taxonomy_area`,
                matchersArray: [new Matcher({ attribute: "name", constant: "%" })]
            },
        ]
    },
    {
        id_reference: `quantity_animal_weight`,
        type: "quantity--standard",
        relationships: [
            {
                relation: 'units',
                type: 'taxonomy_term--unit',
                target_id_reference:`unit_taxonomy_animal_weight` 
            },
        ]
    },
    {
        id_reference:`unit_taxonomy_animal_weight`,
        type:"taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({ attribute: "name", value_in_source: "name" })
        ],
    },
];


let footprints = footprintParameters.map( d => {
        let output;
        if (d.unique) {
            output = new UniqueEntityFootprint(d);
        } else {
            output = new EntityFootprint(d);
        }
        return output;
    } );
;
exports.footprints = footprints;

let populators =  [
    new Populator({
            source: "animal_type",
            entity: "animal_type_taxonomy",
            attribute: "name"
        }),
    new QuantityValuePopulator({
        source: "animal_number",
        entity: "quantity_animal_number",
    }),
    new Populator({
        defaultValue: "count",
        entity: "quantity_animal_number",
        attribute: "measure"
    }),
    new Populator({
        defaultValue: "herd size",
        entity: "quantity_animal_number",
        attribute: "label"
    }),
    new QuantityValuePopulator({
        source: "subsections",
        entity: "quantity_subsections_involved",
    }),
    new Populator({
        defaultValue: "count",
        entity: "quantity_subsections_involved",
        attribute: "measure"
    }),
    new Populator({
        defaultValue: "involved subsections",
        entity: "quantity_subsections_involved",
        attribute: "label"
    }),
    new QuantityValuePopulator({
        source: "avg_weight_lbs",
        entity: "quantity_animal_weight",
    }),
    new Populator({
        defaultValue: "weight",
        entity: "quantity_animal_weight",
        attribute: "measure"
    }),
    new Populator({
        defaultValue: "average animal weight",
        entity: "quantity_animal_weight",
        attribute: "label"
    }),
    new Populator({
        defaultValue: "lbs",
        entity: "unit_taxonomy_animal_weight",
        attribute: "name"
    }),
    new QuantityValuePopulator({
        source:"area_percent",
        entity:"quantity_area",
    }),
    new Populator({
        defaultValue:"area",
        entity:"quantity_area",
        attribute:"measure"
    }),
    new Populator({
        defaultValue:"area percentage",
        entity:"quantity_area",
        attribute:"label"
    }),
    new Populator({
        entity:"grazing_initiation_log",
        attribute:"is_movement",
        defaultValue: true
    }),
    new Populator({
        source:"grazing_start_log_name",
        entity:"grazing_initiation_log",
        attribute:"name"
    }),
    new Populator({
        source:[ "graze_start_date" ],
        entity:"grazing_initiation_log",
        attribute:"timestamp",
        transformation: ({graze_start_date}) => { return graze_start_date ? ( graze_start_date.getTime() / 1000 ) + 18000 : undefined; }
    }),
    new Populator( {
        entity: 'grazing_initiation_log',
        source: "notes",
        attribute: 'notes',
        allowMissing: true
    } ),
    new Populator({
        source:"grazing_ending_log_name",
        entity:"grazing_ending_log",
        attribute:"name"
    }),
    new Populator({
        source:[ "graze_end_date" ],
        entity:"grazing_ending_log",
        attribute:"timestamp",
        transformation: ({graze_end_date}) => { return graze_end_date ? ( graze_end_date.getTime() / 1000 ) + 18000 : undefined; }
    }),
    new Populator({
        entity:"grazing_ending_log",
        attribute:"is_movement",
        defaultValue: true
    }),
    new Populator({
        source:"animal_asset_name",
        entity:"herd_asset",
        attribute:"name"
    }),
    new Populator({
        defaultValue: "grazing",
        entity: "grazing_log_category",
        attribute: "name"
    }),
];

exports.populators = populators;
