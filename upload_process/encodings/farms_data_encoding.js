const {
    EntityFootprint,
    UniqueEntityFootprint,
    Populator,
    Matcher,
    EntitiesSchema,
    QuantityValuePopulator,
    ExternalRelationshipDescription
} = require('farm_os_entity_uploader');

let footprintsSource = [
    {
        id_reference:"farm_organization",
        type: "organization--farm",
        unique: true,
        matchers: [
            new Matcher({attribute:"pasa_farm_id", value_in_source:"pasa_farm_id"})
        ]
    },
    {
        id_reference:"property",
        type: "asset--land",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ],
        relationships: [
            {
                relation: "farm",
                type: "organization--farm",
                target_id_reference: 'farm_organization',
                allowMissing: false
            },
        ]
    },
];

exports.footprints = footprintsSource.map( d => {
    let output;
    if (d.unique) {
        output = new UniqueEntityFootprint(d);
    } else {
        output = new EntityFootprint(d);
    }
    return output;
} );

let populators = [
    new Populator( {
        source: 'farm_name',
        entity: 'farm_organization',
        attribute: 'name'
    } ),
    new Populator( {
        defaultValue: 'active',
        entity: 'farm_organization',
        attribute: 'status'
    } ),
    new Populator({
        source: "farm_id",
        entity: "farm_organization",
        attribute: "pasa_farm_id"
    }),
    new Populator( {
        source: 'farm_name',
        entity: 'property',
        attribute: 'name'
    } ),
    new Populator( {
        defaultValue: 'active',
        entity: 'property',
        attribute: 'status'
    } ),
    new Populator( {
        defaultValue: 'property',
        entity: 'property',
        attribute: 'land_type'
    } ),
];
exports.populators = populators;
