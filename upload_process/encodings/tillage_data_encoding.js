const {
    EntityFootprint,
    UniqueEntityFootprint,
    Populator,
    Matcher,
    GeneralFootprint,
    QuantityValuePopulator,
    ExternalRelationshipDescription
} = require('farm_os_entity_uploader');

function disturbNameGenerator({activity, field, date}) {
    let nameString = "Disturb: ";
    if (activity) {
        nameString = nameString.concat(activity, "");
    };
    if (field) {
      nameString = nameString.concat(" ", field);
    }
    if (date) {
        let formattedDate = date ? `${date.getUTCMonth() + 1}/${date.getUTCDate()}/${date.getFullYear()}` : undefined;
        nameString = nameString.concat(" ", formattedDate);
    };
    return nameString;
    
};
exports.disturbNameGenerator = disturbNameGenerator;

let footprints = [
    {
        id_reference: 'tillage_log',
        type: "log--activity",
        unique: true,
        matchers: [
            new Matcher({ attribute: "name", value_in_source: "name" })
        ],
        relationships: [
            {
                relation: "category",
                type: "taxonomy_term--log_category",
                target_id_reference: "tillage_log_category"
            },
            {
                relation: "location",
                type: "asset--land",
                matchersArray: [ new Matcher({attribute:"pasa_field_id", value_in_source:"field_id"}) ]
            },
            {
                relation: "asset",
                type: "asset--plant",
                matchersArray: [ new Matcher({attribute:"name", value_in_source:"planting_name"}) ]
            },
            {
                relation: 'equipment',
                type: 'asset--equipment',
                target_id_reference:"equipment_asset" 
            },
            {
                relation: 'quantity',
                type: 'quantity--standard',
                target_id_reference:`quantity_area` 
            },
            {
                relation: 'quantity',
                type: 'quantity--standard',
                target_id_reference:`quantity_speed` 
            },
            {
                relation: 'quantity',
                type: 'quantity--standard',
                target_id_reference:`quantity_depth` 
            },
        ]
    },
    {
        id_reference:`tillage_log_category`,
        type:"taxonomy_term--log_category",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ],
    },
    {
        id_reference: "equipment_asset",
        type: "asset--equipment",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ],
    },
    {
        id_reference: `quantity_area`,
        type: "quantity--standard",
        relationships: [
            {
                relation: 'units',
                type: 'taxonomy_term--unit',
                target_id_reference:`unit_taxonomy_area` 
            },
        ]
    },
    {
        id_reference:`unit_taxonomy_area`,
        type:"taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ],
    },
    {
        id_reference:`unit_taxonomy_percentage`,
        type:"taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ],
    },
    {
        id_reference:`unit_depth`,
        type:"taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ],
    },
    {
        id_reference:`unit_speed`,
        type:"taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ],
    },
    {
        id_reference: "quantity_depth",
        type: "quantity--standard",
        relationships: [
            {
                relation: "unit",
                target_id_reference: "unit_depth",
                type: "taxonomy_term--unit",
            }
        ]
    },
    {
        id_reference: "quantity_speed",
        type: "quantity--standard",
        relationships: [
            {
                relation: "unit",
                target_id_reference: "unit_speed",
                type: "taxonomy_term--unit",
            }
        ]
    }
]
    .map( d => {
        let output;
        if (d.unique) {
            output = new UniqueEntityFootprint(d);
        } else {
            output = new EntityFootprint(d);
        }
        return output;
    } );
;
exports.footprints = footprints;

let populators = [
    new Populator({
        source: ["disturb_date"],
        entity: "tillage_log",
        attribute: "timestamp",
        transformation: ({ disturb_date }) => { return disturb_date ? (disturb_date.getTime() / 1000) + 18000 : undefined; }
    }),
    new Populator({
        entity:"tillage_log",
        attribute:"name",
        source: "disturb_log_name"
    }),
    new Populator({
        entity:"tillage_log",
        attribute:"status",
        defaultValue: "done"
    }),
    new Populator({
        entity:"tillage_log",
        attribute:"notes",
        source: "notes",
        allowMissing: true
    }),
    new Populator({
        entity: "tillage_log_category",
        attribute:"name",
        defaultValue:"tillage"
    }),
    new QuantityValuePopulator({
        entity:"quantity_area",
        source:"area_percent"
    }),
    new QuantityValuePopulator({
        entity:"quantity_speed",
        source:"tillage_speed_mph"
    }),
    new QuantityValuePopulator({
        entity:"quantity_depth",
        source:"tillage_depth_in"
    }),
    new Populator({
        entity: "equipment_asset",
        attribute:"name",
        source:"implement_type"
    }),
    new Populator({
        entity: "equipment_asset",
        attribute:"model",
        source:"farmer_implement",
        allowMissing: true
    }),
    new Populator({
        entity:"unit_taxonomy_area",
        attribute:"name",
        source: "area_units"
    }),
    new Populator({
        defaultValue:"percentage",
        entity:"unit_taxonomy_percentage",
        attribute:"name"
    }),
    new Populator({
        defaultValue:"mph",
        entity:"unit_speed",
        attribute:"name"
    }),
    new Populator({
        defaultValue:"in",
        entity:"unit_depth",
        attribute:"name"
    }),
    new Populator({
        defaultValue:"Area affected",
        entity:"quantity_area",
        attribute:"label"
    }),
    new Populator({
        defaultValue:"area",
        entity:"quantity_area",
        attribute:"measure"
    }),
    new Populator({
        defaultValue:"Tillage speed",
        entity:"quantity_speed",
        attribute:"label"
    }),
    new Populator({
        defaultValue:"speed",
        entity:"quantity_speed",
        attribute:"measure"
    }),
    new Populator({
        defaultValue:"Tillage depth",
        entity:"quantity_depth",
        attribute:"label"
    }),
    new Populator({
        defaultValue:"length",
        entity:"quantity_depth",
        attribute:"measure"
    }),
];
exports.populators = populators;
