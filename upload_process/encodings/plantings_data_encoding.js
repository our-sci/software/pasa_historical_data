const {
    EntityFootprint,
    UniqueEntityFootprintArrayMultiplied,
    UniqueEntityFootprint,
    Populator,
    Matcher,
    EntitiesSchema,
    QuantityValuePopulator,
    PopulatorWithArraySource,
    ExternalRelationshipDescription
} = require('farm_os_entity_uploader');

/* Description of this encoding
* 
*/

function plantingNameGenerator({ crop_name, planting_date, field_name }) {
    let nameString = "";
    if (crop_name) {
        nameString = nameString.concat(crop_name, "");
    } else {
        nameString = nameString.concat("unknown crop-");
    };
    if (planting_date) {
        let formattedDate = planting_date ? `${planting_date.getUTCMonth() + 1}/${planting_date.getUTCDate()}/${planting_date.getFullYear()}` : undefined;
        nameString = nameString.concat(" ", formattedDate);
    };
    if (field_name) {
        nameString = nameString.concat(", ", field_name);
    };
    return nameString;
};
function seedingNameGenerator({ crop_name, field_name, season }) {
    let nameString = "Seeding of";
    if (crop_name) {
        nameString = nameString.concat(" ", crop_name, "");
    } else {
        nameString = nameString.concat(" unknown crop-");
    };
    nameString.concat(" in");
    if (field_name) {
        nameString = nameString.concat(" ", field_name);
    };
    nameString = nameString.concat(", ", season);
    return nameString;
};

function terminationNameGenerator({crop_name, field_name, season}) {
    let nameString = "Termination of";
    if (crop_name) {
        nameString = nameString.concat(" ",crop_name, " ");
    } else {
        nameString = nameString.concat(" unknown crop-");
    };
    nameString.concat(" in");
    if (field_name) {
        nameString = nameString.concat(" ", field_name);
    };
    nameString = nameString.concat(", ", season);
    return nameString;
};
function harvestNameGenerator({crop_name, field_name, season}) {
    let nameString = "Harvest of";
    if (crop_name) {
        nameString = nameString.concat(" ", crop_name, " ");
    } else {
        nameString = nameString.concat(" unknown crop-");
    };
    nameString.concat(" in");
    if (field_name) {
        nameString = nameString.concat(" ", field_name);
    };
    nameString = nameString.concat(", ", season);
    return nameString;
};
exports.plantingNameGenerator = plantingNameGenerator;
exports.seedingNameGenerator = seedingNameGenerator;
exports.harvestNameGenerator = harvestNameGenerator;
exports.terminationNameGenerator = terminationNameGenerator;


let populators = [
    {
        entity: 'plant_asset',
        attribute: 'status',
        source:"status"
    },
    {
        entity: 'plant_asset',
        source: "notes",
        attribute: 'notes',
        allowMissing: true
    },
    {
        source: "plantingName",
        entity: 'plant_asset',
        attribute:'name'
    },
    {
        source: [ 'planting_date' ],
        entity: 'seeding_log',
        attribute: 'timestamp',
        transformation: ({planting_date}) => { return planting_date ? ( planting_date.getTime() / 1000 ) + 18000 : undefined; }
    },
    {
        defaultValue:'done',
        entity: 'seeding_log',
        attribute:'status'
    },
    {
        source: 'seeding_log_name',
        entity: 'seeding_log',
        attribute:'name'
    },
    {
        defaultValue: 'rate',
        entity: 'seeding_log_rate_quantity',
        attribute: 'measure'
    },
    {
        defaultValue: 'seeding_rate',
        entity: 'seeding_log_rate_quantity',
        attribute: 'label'
    },
    {
        source: 'seeding_rate_units',
        entity: 'seeding_log_rate_unit',
        attribute: 'name'
    },
    {
        defaultValue: 'weight',
        entity: 'seeding_log_rate_quantity',
        attribute: 'measure'
    },
    {
        defaultValue: 'seed price',
        entity: 'seeding_log_price_quantity',
        attribute: 'label'
    },
    {
        source: 'seed_price_units',
        entity: 'seeding_log_price_unit',
        attribute: 'name'
    },
    {
        defaultValue: 'bed width',
        entity: 'seeding_log_bed_width_quantity',
        attribute: 'label'
    },
    {
        defaultValue: 'length',
        entity: 'seeding_log_bed_width_quantity',
        attribute: 'measure'
    },
    {
        defaultValue: 'bed width e2e',
        entity: 'seeding_log_bed_width_quantity_e2e',
        attribute: 'label'
    },
    {
        defaultValue: 'length',
        entity: 'seeding_log_bed_width_quantity_e2e',
        attribute: 'measure'
    },
    {
        defaultValue: 'bed width c2c',
        entity: 'seeding_log_bed_width_quantity_c2c',
        attribute: 'label'
    },
    {
        defaultValue: 'length',
        entity: 'seeding_log_bed_width_quantity_c2c',
        attribute: 'measure'
    },
    {
        defaultValue: 'ft',
        entity: 'seeding_log_bed_width_unit',
        attribute: 'name'
    },
    {
        defaultValue: 'area percent',
        entity: 'area_percent_quantity',
        attribute: 'label'
    },
    {
        defaultValue: 'ratio',
        entity: 'area_percent_quantity',
        attribute: 'measure'
    },
    {
        defaultValue: '%',
        entity: 'area_percent_unit',
        attribute: 'name'
    },
    {
        source: [ 'term_date' ],
        entity: 'termination_log',
        attribute: 'timestamp',
        transformation: ({term_date}) => { return term_date ? ( term_date.getTime() / 1000 ) + 18000 : undefined; },
        allowMissing: false
    },
    {
        defaultValue:'done',
        entity: 'termination_log',
        attribute:'status'
    },
    {
        defaultValue:true,
        entity: 'termination_log',
        attribute:'is_termination'
    },
    {
        source:"termination_log_name",
        entity: 'termination_log',
        attribute:'name'
    },
    {
        source:"harvest_log_name",
        entity: 'harvest_log',
        attribute:'name'
    },
    {
        source: [ 'harvest_date' ],
        entity: 'harvest_log',
        attribute: 'timestamp',
        transformation: ({harvest_date}) => { return harvest_date ? ( harvest_date.getTime() / 1000 ) + 18000 : undefined; },
        allowMissing: false
    },
    {
        defaultValue:'done',
        entity: 'harvest_log',
        attribute:'status'
    },
    {
        source: 'yield_units',
        entity: 'harvest_log_yield_unit',
        attribute: 'name',
        allowMissing: false
    },
    {
        defaultValue: 'yield',
        entity: 'harvest_log_yield_quantity',
        attribute: 'label',
        allowMissing: false
    },
    {
        source: "season",
        entity: "season_taxonomy_term",
        attribute: "name"
    },
];

let cropArrayPopulator = new PopulatorWithArraySource({
        source: 'crop_name',
        entity: 'taxonomy_term_species',
        attribute: 'name',
});

let cropTypeArrayPopulator = new PopulatorWithArraySource({
        source: 'crop_type',
        entity: 'taxonomy_term_crop_type',
        attribute: 'name',
});

let quantityValuePopulators = [
    {
        source: 'seeding_rate',
        entity: 'seeding_log_rate_quantity',
        attribute: 'value',
        allowMissing: false
    },
    {
        source: 'seed_price',
        entity: 'seeding_log_price_quantity',
        attribute: 'value',
        allowMissing: false
    },
    {
        source: 'yield',
        entity: 'harvest_log_yield_quantity',
        attribute: 'value',
        allowMissing: false
    },
    {
        source: 'bed_width_ft',
        entity:"seeding_log_bed_width_quantity",
        attribute: 'value',
        allowMissing: false
    },
    {
        source: 'bed_width_e2e',
        entity:"seeding_log_bed_width_quantity_e2e",
        attribute: 'value',
        allowMissing: false
    },
    {
        source: 'bed_width_c2c',
        entity:"seeding_log_bed_width_quantity_c2c",
        attribute: 'value',
        allowMissing: false
    },
    {
        source: 'area_percent',
        entity:"area_percent_quantity",
        attribute: 'value',
        allowMissing: false
    }
];
exports.populators = [ ... populators.map( d => new Populator(d) ), ... quantityValuePopulators.map( d => new QuantityValuePopulator(d) ) ];
exports.populators_no_field = [ ... populators.filter( d => d.entity != "land_asset" ).map( d => new Populator(d) ), ... quantityValuePopulators.filter( d => d.entity != "land_asset" ).map( d => new QuantityValuePopulator(d) ) ];

let footprintsSource = [
    {
        id_reference:"plant_asset",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"}),
        ],
        relationships: [
            {
                relation: "location",
                type: "asset--land",
                matchersArray: [
                    new Matcher( { attribute: "pasa_field_id", value_in_source: "field_id" } ),
                ]
            },
            {
                relation: "plant_type",
                type: 'taxonomy_term--plant_type',
                target_id_reference: 'taxonomy_term_species',
                allowMissing: false
            },
            {
                relation: "plant_type",
                type: 'taxonomy_term--plant_type',
                target_id_reference: 'taxonomy_term_crop_type',
                allowMissing: false
            },
            {
                relation: "season",
                type: "taxonomy_term--season",
                target_id_reference:"season_taxonomy_term",
                allowMissing: false
            }
        ],
        type: "asset--plant"
    },
    {
        id_reference:"termination_log",
        type: "log--activity",
        relationships: [
            {
                relation: "location",
                type: "asset--land",
                matchersArray: [
                    new Matcher( { attribute: "pasa_field_id", value_in_source: "field_id" } ),
                ]
            },
            {
                relation: "asset",
                type: "asset--plant",
                target_id_reference: "plant_asset"
            }
        ],
    },
    {
        id_reference:"harvest_log",
        relationships: [
            {
                relation: "quantity",
                type: "quantity--standard",
                target_id_reference: "harvest_log_yield_quantity"
            },
            {
                relation: "location",
                type: "asset--land",
                target_id_reference: "land_asset",
                matchersArray: [
                    new Matcher( { attribute: "pasa_field_id", value_in_source: "field_id" } ),
                ]
            },
            {
                relation: "asset",
                type: "asset--plant",
                target_id_reference: "plant_asset"
            }
        ],
        type: "log--harvest"
    },
    {
        id_reference:"harvest_log_yield_quantity",
        relationships: [
            {
                relation: "units",
                type: "taxonomy_term--unit",
                target_id_reference: "harvest_log_yield_unit"
            }
        ],
        type: "quantity--standard"
    },
    {
        id_reference:"harvest_log_yield_unit",
        type: "taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ]
    },
    {
        id_reference:"seeding_log",
        relationships: [
            {
                relation: "quantity",
                type: "quantity--standard",
                target_id_reference: "seeding_log_rate_quantity"
            },
            {
                relation: "quantity",
                type: "quantity--standard",
                target_id_reference: "seeding_log_price_quantity"
            },
            {
                relation: "quantity",
                type: "quantity--standard",
                target_id_reference: "seeding_log_bed_width_quantity"
            },
            {
                relation: "quantity",
                type: "quantity--standard",
                target_id_reference: "seeding_log_bed_width_quantity_e2e"
            },
            {
                relation: "quantity",
                type: "quantity--standard",
                target_id_reference: "seeding_log_bed_width_quantity_c2c"
            },
            {
                relation: "quantity",
                type: "quantity--standard",
                target_id_reference: "area_percent_quantity"
            },
            {
                relation: "location",
                type: "asset--land",
                target_id_reference: "land_asset",
                matchersArray: [
                    new Matcher( { attribute: "pasa_field_id", value_in_source: "field_id" } ),
                ]
            },
            {
                relation: "asset",
                type: "asset--plant",
                target_id_reference: "plant_asset"
            },
        ],
        type: "log--seeding"
    },
    {
        id_reference:"seeding_log_rate_quantity",
        relationships: [
            {
                relation: "units",
                type: "taxonomy_term--unit",
                target_id_reference: "seeding_log_rate_unit"
            }
        ],
        type: "quantity--standard"
    },
    {
        id_reference:"seeding_log_rate_unit",
        type: "taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ]
    },
    {
        id_reference:"seeding_log_rate_unit",
        type: "taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ]
    },
    {
        id_reference:"seeding_log_bed_width_quantity",
        relationships: [
            {
                relation: "units",
                type: "taxonomy_term--unit",
                target_id_reference: "seeding_log_bed_width_unit"
            }
        ],
        type: "quantity--standard"
    },
    {
        id_reference:"seeding_log_bed_width_quantity_e2e",
        relationships: [
            {
                relation: "units",
                type: "taxonomy_term--unit",
                target_id_reference: "seeding_log_bed_width_unit"
            }
        ],
        type: "quantity--standard"
    },
    {
        id_reference:"seeding_log_bed_width_quantity_c2c",
        relationships: [
            {
                relation: "units",
                type: "taxonomy_term--unit",
                target_id_reference: "seeding_log_bed_width_unit"
            }
        ],
        type: "quantity--standard"
    },
    {
        id_reference:"seeding_log_bed_width_unit",
        type: "taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ]
    },
    {
        id_reference:"area_percent_quantity",
        relationships: [
            {
                relation: "units",
                type: "taxonomy_term--unit",
                target_id_reference: "area_percent_unit"
            }
        ],
        type: "quantity--standard"
    },
    {
        id_reference:"area_percent_unit",
        type: "taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ]
    },
    {
        id_reference:"seeding_log_price_quantity",
        relationships: [
            {
                relation: "units",
                type: "taxonomy_term--unit",
                target_id_reference: "seeding_log_price_unit"
            }
        ],
        type: "quantity--standard"
    },
    {
        id_reference:"seeding_log_price_unit",
        type: "taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ]
},
    {
        id_reference: "season_taxonomy_term",
        type: "taxonomy_term--season",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ]
    }
];

let speciesArrayFootprint = new UniqueEntityFootprintArrayMultiplied( {
    id_reference:"taxonomy_term_species",
    type: "taxonomy_term--plant_type",
    unique: true,
    arrayBasedPopulator: cropArrayPopulator,
    matchers: [
        new Matcher({attribute:"name", value_in_source:"name"})
    ],
    // use the category as parent
    relationships: [
    {
        relation: "parent",
        type: "taxonomy_term--plant_type",
        matchersArray: [
            new Matcher( { attribute: "name", constant: "pasa_species" } ),
    ]
    }
    ]
});

let cropTypeArrayFootprint = new UniqueEntityFootprintArrayMultiplied( {
    id_reference:"taxonomy_term_crop_type",
    type: "taxonomy_term--plant_type",
    unique: true,
    arrayBasedPopulator: cropTypeArrayPopulator,
    matchers: [
        new Matcher({attribute:"name", value_in_source:"name"})
    ],
    // use the category as parent
    relationships: [
    {
        relation: "parent",
        type: "taxonomy_term--plant_type",
        matchersArray: [
            new Matcher( { attribute: "name", constant: "pasa_crop_type" } ),
    ]
    }
    ]
});

let regularFootprints = footprintsSource.map( d => {
    let output;
    if (d.unique) {
            output = new UniqueEntityFootprint(d);
    } else {
        output = new EntityFootprint(d);
    }
    return output;
} );

exports.footprints = [
    ... regularFootprints,
    speciesArrayFootprint,
    cropTypeArrayFootprint
];
