const {
    EntityFootprint,
    UniqueEntityFootprint,
    Populator,
    Matcher,
    GeneralFootprint,
    QuantityValuePopulator,
    ExternalRelationshipDescription
} = require('farm_os_entity_uploader');

function inputNameGenerator({activity, farmerName, field, date}) {
    let nameString = "Input: ";
    if (activity) {
        nameString = nameString.concat(activity, "");
    };
    if (farmerName) {
        nameString = nameString.concat(" (", farmerName, ")", "");
    };
    if (field) {
        
        nameString = nameString.concat(" ", field);
    };
    if (date) {
        let formattedDate = date ? `${date.getUTCMonth() + 1}/${date.getUTCDate()}/${date.getFullYear()}` : undefined;
        nameString = nameString.concat(" ", formattedDate);
    };
    return nameString;
    
};
exports.inputNameGenerator = inputNameGenerator;


let footprints = [
    {
        id_reference: 'input_log',
        type: "log--input",
        relationships: [
            {
                relation: "location",
                type: "asset--land",
                matchersArray: [ new Matcher({attribute:"pasa_field_id", value_in_source:"field_id"}) ]
            },
            {
                relation: "asset",
                type: "asset--plant",
                matchersArray: [ new Matcher({attribute:"name", value_in_source:"planting_name"}) ]
            },
            {
                relation: 'quantity',
                type: 'quantity--standard',
                target_id_reference:`quantity_mass` 
            },
            {
                relation: 'quantity',
                type: 'quantity--standard',
                target_id_reference:`quantity_area` 
            },
            {
                relation: 'quantity',
                type: 'quantity--standard',
                target_id_reference:`moisture_percentage` 
            },
            {
                relation: 'quantity',
                type: 'quantity--standard',
                target_id_reference:`phosphorus_wet_percentage` 
            },
            {
                relation: 'quantity',
                type: 'quantity--standard',
                target_id_reference:`nitrogen_wet_percentage` 
            },
            {
                relation: 'quantity',
                type: 'quantity--standard',
                target_id_reference:`potassium_wet_percentage` 
            }, {
                relation: 'category',
                type: 'taxonomy_term--log_category',
                target_id_reference: `specific_input_type_category`
            }, {
                relation: 'category',
                type: 'taxonomy_term--log_category',
                target_id_reference: `input_type_category`
            }
        ]
    },{
        id_reference: `quantity_mass`,
        type: "quantity--standard",
        relationships: [
            {
                relation: 'units',
                type: 'taxonomy_term--unit',
                target_id_reference:`unit_taxonomy_mass` 
            },
        ]
    }, {
        id_reference: `quantity_area`,
        type: "quantity--standard",
        relationships: [
            {
                relation: 'units',
                type: 'taxonomy_term--unit',
                target_id_reference:`unit_taxonomy_area` 
            },
        ]
    },{
        id_reference: `moisture_percentage`,
        type: "quantity--standard",
        relationships: [
            {
                relation: 'units',
                type: 'taxonomy_term--unit',
                target_id_reference:`unit_taxonomy_percentage` 
            },
        ]
    },{
        id_reference: `phosphorus_wet_percentage`,
        type: "quantity--standard",
        relationships: [
            {
                relation: 'units',
                type: 'taxonomy_term--unit',
                target_id_reference:`unit_taxonomy_percentage` 
            },
        ]
    },{
        id_reference: `nitrogen_wet_percentage`,
        type: "quantity--standard",
        relationships: [
            {
                relation: 'units',
                type: 'taxonomy_term--unit',
                target_id_reference:`unit_taxonomy_percentage` 
            },
        ]
    },{
        id_reference: `potassium_wet_percentage`,
        type: "quantity--standard",
        relationships: [
            {
                relation: 'units',
                type: 'taxonomy_term--unit',
                target_id_reference:`unit_taxonomy_percentage` 
            },
        ]
    },
    {
        id_reference:`unit_taxonomy_mass`,
        type:"taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ],
    },
    {
        id_reference:`unit_taxonomy_area`,
        type:"taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ],
    },
    {
        id_reference:`unit_taxonomy_percentage`,
        type:"taxonomy_term--unit",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ],
    },
    {
        id_reference: `input_type_category`,
        type: "taxonomy_term--log_category",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ],
    },
    {
        id_reference: `specific_input_type_category`,
        type: "taxonomy_term--log_category",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ],
    }
]
    .map( d => {
        let output;
        if (d.unique) {
            output = new UniqueEntityFootprint(d);
        } else {
            output = new EntityFootprint(d);
        }
        return output;
    } );
;
exports.footprints = footprints;

let populators = [
    new Populator({
        source:"input_log_name",
        entity:"input_log",
        attribute:"name"
    }),
    new Populator({
        source:"specific_input_type",
        entity:"input_log",
        attribute:"source",
        allowMissing: true
    }),
    new Populator({
        source:[ "applied_date" ],
        entity:"input_log",
        attribute:"timestamp",
        transformation: ({applied_date}) => { return applied_date ? ( applied_date.getTime() / 1000 ) + 18000 : undefined; }
    }),
    new Populator({
        source:"notes",
        entity:"input_log",
        attribute:"notes",
        allowMissing: true
    }),
    new QuantityValuePopulator({
        entity:"quantity_mass",
        source:"quantity_lbs",
        attribute: "value"
    }),
    new QuantityValuePopulator({
        entity:"moisture_percentage",
        source:"pc_moisture",
        attribute: "value"
    }),
    new QuantityValuePopulator({
        entity:"phosphorus_wet_percentage",
        source:"pc_p_wet",
        attribute: "value"
    }),
    new QuantityValuePopulator({
        entity:"nitrogen_wet_percentage",
        source:"pc_n_wet",
        attribute: "value"
    }),
    new QuantityValuePopulator({
        entity:"potassium_wet_percentage",
        source:"pc_k_wet",
        attribute: "value"
    }),
    new Populator({
        defaultValue:"Mass",
        entity:"quantity_mass",
        attribute:"label"
    }),
    new Populator({
        defaultValue:"weight",
        entity:"quantity_mass",
        attribute:"measure"
    }),
    new QuantityValuePopulator({
        entity:"quantity_area",
        source:"area_percent"
    }),
    new Populator({
        entity:"unit_taxonomy_area",
        attribute:"name",
        source: "area_units"
    }),
    new Populator({
        defaultValue:"percentage",
        entity:"unit_taxonomy_percentage",
        attribute:"name"
    }),
    new Populator({
        defaultValue:"Area affected",
        entity:"quantity_area",
        attribute:"label"
    }),
    new Populator({
        defaultValue:"area",
        entity:"quantity_area",
        attribute:"measure"
    }),
    new Populator({
        defaultValue:"lbs",
        entity:"unit_taxonomy_mass",
        attribute:"name"
    }),
    new Populator({
        source:"input_type",
        entity: "input_type_category",
        attribute: "name"
    }),
    new Populator({
        source:"specific_input_type",
        entity: "specific_input_type_category",
        attribute: "name"
    })
];
exports.populators = populators;
