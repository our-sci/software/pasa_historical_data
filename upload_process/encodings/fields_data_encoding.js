const {
    EntityFootprint,
    UniqueEntityFootprint,
    Populator,
    Matcher,
    EntitiesSchema,
    QuantityValuePopulator,
    ExternalRelationshipDescription
} = require('farm_os_entity_uploader');

/*
 * Description of this encoding: Each farm is represented by an organization--farm entity, which will be related by all of the land--assets that will represent fields.
 * Each field has a pasa field id value, a geometry describing its location.
*/
let footprintsSource = [
    {
        id_reference:"land_asset",
        type: "asset--land",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ],
        relationships: [
            {
                relation: "farm",
                type: "organization--farm",
                matchersArray: [ new Matcher({attribute:"pasa_farm_id", value_in_source:"farm_id"}) ]
            },
            {
                relation: "parent",
                type: "asset--land",
                matchersArray: [ new Matcher({attribute:"name", value_in_source:"farm_name"}) ]
            },
        ]
    },
];

exports.footprints = footprintsSource.map( d => {
    let output;
    if (d.unique) {
        output = new UniqueEntityFootprint(d);
    } else {
        output = new EntityFootprint(d);
    }
    return output;
} );


let populators = [
    new Populator( {
        source: 'field_name',
        entity: 'land_asset',
        attribute: 'name'
    } ),
    new Populator( {
        entity: 'land_asset',
        attribute: 'status',
        source: 'status'
    } ),
    new Populator( {
        defaultValue: 'field',
        entity: 'land_asset',
        attribute: 'land_type',
    } ),
    new Populator({
        source: "field_id",
        entity: "land_asset",
        attribute: "pasa_field_id"
    }),
    new Populator({
            source: "geometry",
            entity: "land_asset",
            attribute: "intrinsic_geometry"
    }),
    new Populator({
            defaultValue: true,
            entity: "land_asset",
            attribute: "is_fixed"
    }),
    new Populator({
            defaultValue: true,
            entity: "land_asset",
            attribute: "is_location"
    }),
    new Populator( {
        transformation: ({farm_id, field_id, farm_name, field_name}) => {
            return( [
                {
                    id:farm_id,
                    type: "eid",
                    location:"farm_id"
                },
                {
                    id:field_id,
                    type: "eid",
                    location:"field_id",
                },
                {
                    id:farm_name,
                    type: "eid",
                    location:"farm_name",
                },
                {
                    id:field_name,
                    location:"field_name",
                    type: "eid",
                },
            ]);
        },
        source: [ 'farm_id', 'field_id', 'farm_name', 'field_name' ],
        entity: 'land_asset',
        attribute:'id_tag'
    } )
];
exports.populators = populators;
