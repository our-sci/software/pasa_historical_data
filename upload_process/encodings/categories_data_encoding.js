const {
    EntityFootprint,
    UniqueEntityFootprint,
    Populator,
    Matcher,
    EntitiesSchema,
    QuantityValuePopulator,
    ExternalRelationshipDescription
} = require('farm_os_entity_uploader');

let footprintsSource = [
    {
        id_reference:"category",
        type: "taxonomy_term--plant_type",
        unique: true,
        matchers: [
            new Matcher({attribute:"name", value_in_source:"name"})
        ]
    },
];

exports.footprints = footprintsSource.map( d => {
    let output;
    if (d.unique) {
        output = new UniqueEntityFootprint(d);
    } else {
        output = new EntityFootprint(d);
    }
    return output;
} );

let populators = [
    new Populator( {
        source: 'name',
        entity: 'category',
        attribute: 'name'
    } ),
];
exports.populators = populators;
