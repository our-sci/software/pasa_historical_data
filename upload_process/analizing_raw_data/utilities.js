const papa = require("papaparse");

/**
 * Feed it with a table detailing corresponding files.  It will compare their variable sets.
 * @param {} tableEntry
 * @returns {} 
 */
exports.compareVariables = function compareVariables({ tableEntry, firstGroupFolder, secondGroupFolder }) {
    let firstGroupTable = papa.parse(fs.readFileSync(`${firstGroupFolder}/${tableEntry.first}`, "utf8"), { header: true });
    let secondGroupTable = papa.parse(fs.readFileSync(`${secondGroupFolder}/${tableEntry.second}`, "utf8"), { header: true });

    let onlyInFirst = firstGroupTable.meta.fields.filter(d => !secondGroupTable.meta.fields.includes(d));
    let onlyInSecond = secondGroupTable.meta.fields.filter(d => !firstGroupTable.meta.fields.includes(d));

    let output = {
        first_group_only_variables: onlyInFirst,
        second_group_only_variables: onlyInSecond,
        first_group_full_table: firstGroupTable.data,
        second_group_full_table: secondGroupTable.data,
        all_first_group_variables: firstGroupTable.meta.fields,
        all_second_group_variables: secondGroupTable.meta.fields,
    };
    return output;
};
