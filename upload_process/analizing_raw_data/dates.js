

let datesComparisonTable = plantings.data
    .map( d => {
        let output = {
            term_string: d.term_date,
            planting_string: d.planting_date,
            term_date: d.term_date ? new Date( d.term_date ) : undefined,
            planting_date: d.planting_date ? new Date( d.planting_date ) : undefined,
        };
        output.term_rebuilt = output.term_date?.toISOString().split("T")[0];
        output.planting_rebuilt = output.planting_date?.toISOString().split("T")[0];
        output.term_discrepancy = output.term_rebuilt != output.term_string;
        output.planting_discrepancy = output.planting_rebuilt != output.planting_string;
        return output;
} );
