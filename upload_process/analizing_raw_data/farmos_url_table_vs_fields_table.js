// We want to compare fields and farms implied by both tables.
// An error in the compilation of the table happened, which caused data from some farms to be sent to wrong urls. This was detected as some farms having more then one organization.

const fs = require("fs");
const papa = require("papaparse");


const urlsTable = papa.parse(fs.readFileSync( `${__dirname}/../../raw_data/production_farms.csv`, "utf8" ), {header:true}).data;
const fieldsTable = papa.parse(fs.readFileSync( `${__dirname}/../../raw_data/second_version_tables/shbs_enrolled_fields.2024-07-02.csv`, "utf8" ), {header:true}).data;


// The common key is the farm name. It is called ''Account: Account Name'' in the URLs list and farm_name in the fields one.

let allFarmNames = Array.from(
    new Set(
        [
            ... urlsTable.map( d => d['Account: Account Name'] ),
            ... fieldsTable.map( d => d.farm_name ),
        ]
    )
)
    .sort()
;


let review = allFarmNames.map( currentFarmName => {
    let fieldEntries = fieldsTable.filter( d => d.farm_name == currentFarmName );
    let urlEntry = urlsTable.filter( d => d['Account: Account Name'] == currentFarmName );
    let output = {
        farm_name: currentFarmName,
        amount_of_fields: fieldEntries.length,
        url: urlEntry[0] ? urlEntry[0]['farmOS ID'] : undefined,
        exists_in_fields: fieldEntries.length > 0,
        exists_in_urls: urlEntry.length > 0,
        amount_of_urls: urlEntry.length,
        too_many_urls:urlEntry.length > 1,
        no_fields: fieldEntries.length == 0
    };
    return output;
} );

urlsTable.filter( d => d['farmOS ID'] == 'twoganderfarm.farmos.net' );

let allUrls = Array.from( new Set( urlsTable.map( d => d['farmOS ID'] ) ) );

let urlsReview = allUrls.map( url => {
    let entries = urlsTable.filter( d => d['farmOS ID'] == url );
    let farmNames = Array.from( new Set( entries.map( d => d['Account: Account Name'] ) ) );
    return {
        url: url,
        farm_names: farmNames,
        amount_of_names: farmNames.length
    };
} );

let farmsCarryingExtraAssignments  = urlsReview.filter( d => d.amount_of_names > 1 );

let correctlyAssigned = ['Two Gander Farm (Downington, Pa)','Old Tin Roof Farm, LLC','Flying Plow Farm','Churchview Farm LLC (Pittsburgh)'];

let missingURLPairs = [
    {
        name: 'Apple Ridge Farm',
        actual_url: 'appleridgefarm.farmos.net'
    },
    {
        name: 'Frankenfield Farm',
        actual_url: 'frankenfieldfarm.farmos.net'
    },
    {
        name: 'Olszanowski Farm',
        actual_url: 'olszanowskifarm.farmos.net'
    },
    {
        name: 'Ulrich Farms',
        actual_url: 'ulrichfarms.farmos.net'
    },
    {
        name: 'Village Acres Farm',
        actual_url: 'villageacresfarm.farmos.net'
    }
];
    

let farmsMissingURL = urlsReview.filter( d => d.amount_of_names != 1 )
    .flatMap( entry => entry.farm_names.map( name => { return { name: name, original_url: entry.url }; } ) )
    .filter( row => !correctlyAssigned.includes( row.name ) )
    .map( farm => {
        let correction = missingURLPairs.find( p => p.name == farm.name );
        farm.url = correction.actual_url;
        return farm;
    } )
;


let wrongFarmURLPairs = farmsCarryingExtraAssignments
    .flatMap( entry => {
        entry.farm_names = entry.farm_names.filter( name => !correctlyAssigned.includes(name) );
        let output = entry.farm_names.map( name => { return { name: name, current_wrong_url: entry.url } } );
        return output;
    } )
;

let deliverables = {
    to_remove: wrongFarmURLPairs,
    to_add: farmsMissingURL
};

fs.writeFileSync( `${__dirname}/../operation_registers/production_main_batch/farm_errors_analysis.json`, JSON.stringify(deliverables) );
