const fs = require("fs");
const papa = require("papaparse");
const {
    selectedFarms,
} = require('../submitters/artifacts.js');

const plantings_encoding = require('../encodings/plantings_data_encoding');

let dataSourceFields = plantings_encoding
    .populators
    .flatMap( d => d.source )
    .filter( d => d )
;

let plantingFirst = papa.parse( fs.readFileSync( `${__dirname}/../../raw_data/second_version_tables/shbs_planting_records.2024-07-02.csv`, "utf8" ), {header:true} );
let plantingSecond = papa.parse( fs.readFileSync( `${__dirname}/../../raw_data/first_version_tables/SHBS.2016-2022.Planting.csv`, "utf8" ), {header:true} );

let fieldsInFirstVersion = dataSourceFields.filter( d => plantingFirst.meta.fields.includes(d) );
let fieldsInSecondVersion = dataSourceFields.filter( d => plantingSecond.meta.fields.includes(d) );

let missingFields = fieldsInFirstVersion.filter( d => !fieldsInSecondVersion.includes(d) );
let newFields = plantingSecond.meta.fields.filter( d => !plantingFirst.meta.fields.includes(d) ); 
