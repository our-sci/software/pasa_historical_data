const fs = require("fs");

let data = JSON.parse( fs.readFileSync(`${__dirname}/../operation_registers/finalPlantingData.json`) );

let finalDatePattern = /-12-31T/;
let initialDatePattern = /-01-01/;

let initialPlantings = data.filter( plant => initialDatePattern.test( plant.planting_date ) );
let finalPlantings = data.filter( plant => finalDatePattern.test( plant.term_date ) );


// We have way more plantings ending on December 31 than starting on January first.

let pairings = initialPlantings.map( initial => {
    let finalCandidates = data
        .filter( d => d.farm_id == initial.farm_id && d.field_id == initial.field_id && d.survey_year == initial.survey_year + 1 && d.crop_name == initial.crop_name );
    let output = {
        initial: initial,
        final_candidates: finalCandidates,
        final_candidates_amount: finalCandidates.length
    };
    return output;
} );
