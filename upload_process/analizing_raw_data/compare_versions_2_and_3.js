// In this file, we analyze the tables containing all data, particularyl the currently latest 3rd version vs the preceding one, searching for new fields, missing fields, and columns that are new, missing or have changed names.
const papa = require("papaparse");
const fs = require("fs");
const {compareVariables} = require("./utilities.js");

const secondGenTables = `${__dirname}/../../raw_data/second_version_tables`;
const thirdGenTables = `${__dirname}/../../raw_data/third_version_tables`;

let fileNames = {
    disturbance: {
        first: "shbs_disturbance_records.2024-07-02.csv",
        second: "disturbance_records_2024-09-03.csv",
    },
    fields: {
        first: "shbs_enrolled_fields.2024-07-02.csv",
        second: undefined
    },
    grazing: {
        first: "shbs_grazing_records.2024-07-02.csv",
        second: "grazing_records_2024-09-03.csv"
    },
    input: {
        first: "shbs_organic_input_records.2024-07-02.csv",
        second: "orginput_records_2024-09-03.csv"
    },
    planting: {
        first: "shbs_planting_records.2024-07-02.csv",
        second: "planting_records_2024-09-03.csv"
    },
    collapsed_planting: {
        first: "shbs_planting_records.2024-07-02.csv",
        second: "planting_records_collapsed_2024-09-03.csv"
    }
};


// let dist = compareVariables({ tableEntry: fileNames["disturbance"], firstGroupFolder: secondGenTables, secondGroupFolder: thirdGenTables });

let variablesComparison = Object.keys(fileNames)
    .filter(name => fileNames[name].first && fileNames[name].second)
    .map(name => {
        let comparisonOutput = {
            file: name,
            result: compareVariables({ tableEntry: fileNames[name] , firstGroupFolder: secondGenTables, secondGroupFolder: thirdGenTables })
        };
        return comparisonOutput;
    });

let secondGenFields = variablesComparison
    .find(d => d.file == "input")
    .result
    .first_group_full_table
    .map(d => {
        d.generation = "second";
        return d;
    })
    ;
let thirdGenFields = variablesComparison
    .find(d => d.file == "input")
    .result
    .second_group_full_table
    .map(d => {
        d.generation = "third";
        return d;
    })
    ;

let fieldsTable = [];

[... secondGenFields, ... thirdGenFields]
    .forEach( field => {
        field.farm_id = field.field_id?.split("_")[0];
        let output = {};
        let preexistingEntry = fieldsTable.find(entry => entry.field_id == field.field_id );
        if (preexistingEntry) {
            output = preexistingEntry;
        } else {
            output = {
                second: false,
                third: false,
                complete: false,
                farm_id: field.farm_id,
                field_id: field.field_id
            };
            fieldsTable.push(output);
        }
        if (field.generation == "second") {
            output.second = true;
        } else if (field.generation == "third") {
            output.third = true;
        };
        if (output.third && output.second) {
            output.complete = true;
        };
    } )
;

let plantingsCollapsed = papa.parse(fs.readFileSync(`${thirdGenTables}/${fileNames.collapsed_planting.third}`, "utf8"), { header: true });
let plantingsExpanded = variablesComparison.find( d => d.file == "collapsed_planting" ).first_group_full_table;


let collapsedSpecies = new Set( plantingsCollapsed.data.flatMap( d => d.crop_name?.split(", ") ) );
