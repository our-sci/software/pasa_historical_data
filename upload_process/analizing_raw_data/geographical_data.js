// This file is only a manual review, not intended to be part of a data pipeline.
// We received a second batch of geographical data for fields, and need to review which fields already existed and which ones are new.
const fs = require("fs");
const { geojsonToWKT } = require( "@terraformer/wkt" );
const papa = require("papaparse");

let polygons = JSON.parse(fs.readFileSync( `${__dirname}/../../raw_data/geographic_data/SHBS_Field_Polygons_2024-07-23.geojson` ));
let points = JSON.parse(fs.readFileSync( `${__dirname}/../../raw_data/geographic_data/SHBS_Field_Polygons_2024-07-23.geojson` ));

let fields2025 = JSON.parse(fs.readFileSync(`${__dirname}/../../raw_data/geographic_data/SHBS_Fields_11-25.geojson`));


// Transforming one polygon.
let examleTransformedObject = geojsonToWKT( polygons.features[0].geometry );


// Analyzing already sent and not yet sent fields as of 27 November 2024. A first batch was sent (SHBS_Field_Polygons_2024-07-23.geojson) and a new one is available (SHBS_Fields_11-25.geojson).
let firstSubmissionFields = polygons.features.map(d => d.properties.field_id);
let secondSubmissionFields = fields2025.features.map(d => d.properties.field_id);

// fields only in first submission
let ommitedFields = firstSubmissionFields.filter(field_id => !secondSubmissionFields.includes(field_id));


// fields only in second submission
let newFields = secondSubmissionFields.filter( field_id => !firstSubmissionFields.includes( field_id ) );

let addedFieldsDetails  = fields2025.features.filter(d => newFields.includes( d.properties.field_id ));

let newFieldsSummary = addedFieldsDetails.map(d => { return { field_id: d.properties.field_id, field_name: d.properties.field_name, farm_name: d.properties.farm_name, farm_id: d.properties.farm_id }; });

fs.writeFileSync(`${__dirname}/../../raw_data/geographic_data/summary_of_new_2025_fields.csv`, papa.unparse(newFieldsSummary, { header: true }));
