// In this file, we analyze the third version of the data, delivered in September, 2024, to check which adaptations are needed in order for the scripts to keep working.
const papa = require("papaparse");
const fs = require("fs");
const {compareVariables} = require("./utilities.js");

const data2023Tables = `${__dirname}/../../raw_data/third_version_tables/data_2023`;
const thirdGenTables = `${__dirname}/../../raw_data/third_version_tables`;


let fileNames = {
    disturbance: {
        first: "disturbance_records_2024-09-03.csv",
        second: "Disturbance 2023 - bullet field names.csv",
    },
    // fields: {
    //     first: "shbs_enrolled_fields.2024-07-02.csv",
    //     second: undefined
    // },
    grazing: {
        first: "grazing_records_2024-09-03.csv",
        second:  "Animals 2023 - bullet field names.csv",
    },
    input: {
        first: "orginput_records_2024-09-03.csv",
        second: "Org Inputs 2023 - bullet field names.csv"
    },
    planting: {
        first: "planting_records_2024-09-03.csv",
        second: "Planting2023 - bullet field names.csv"
    },
    // soil_tests: {
    //     second: "Research Database - CASH Tests 2023.csv"
    // }
};


let variablesComparison = Object.keys(fileNames)
    .filter(name => fileNames[name].first && fileNames[name].second)
    .map(name => {
        let comparisonOutput = {
            file: name,
            result: compareVariables({ tableEntry: fileNames[name] , firstGroupFolder: thirdGenTables, secondGroupFolder: data2023Tables })
        };
        return comparisonOutput;
    });
