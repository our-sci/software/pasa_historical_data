const papa = require("papaparse");

let dataFolder = `${__dirname}/../../raw_data/third_version_tables/data_2023`;

let files = [
    // "Research Database - CASH Tests 2023.csv",
    "Research Database - Org Inputs 2023.csv",
    "Research Database - Planting2023.csv",
    "Research Database - Disturbance 2023.csv",
    "Research Database - Animals 2023.csv",
];

// function bulletify(variableName) {
//     let output = variableName
//         .toLowerCase()
//         .replace(/(\()|(\))/g, "")
//         .replace(/\s/g, "_")
//         .replace(/%/g, "percent")
//         .replace(/till/g, "tillage")
//         ;
//     return output;
// }


let replacementLists = [
    {
        table: "Research Database - Planting2023.csv",
        planting_record_pkey: "Index",
        complete: "Complete Record?",
        survey_year: "Year",
        field_id: "Field ID",
        farm_name: "Farm Name",
        field_name: "Field Name",
        planting_date: "Plant Date",
        term_date: "Term Date",
        crop_name: "Standard Crop",
        farmer_crop_name: "Farmer Crop",
        mixture_id: "Mixture ID",
        crop_type: "Crop Type",
        area: "Area",
        area_units: "Area Units",
        area_percent: "Area %",
        bed_width_e2e: "E2E bed width (ft)",
        bed_width_c2c: "C2C bed width (ft)",
        aisle_width: "Aisle width (ft)",
        yield: "Yield",
        yield_units: "Yield Units",
        farmer_notes: "Farmer Notes",
        research_notes: "Internal Notes",
        ss_response_id: "SS Response ID",
        ss_record_id: "SS Record ID"
    }, {        
        table: "Research Database - Disturbance 2023.csv",
        disturbance_record_pkey: "Index",
        complete: "Complete Record?",
        survey_year: "Year",
        field_id: "Field ID",
        farm_name: "Farm Name",
        field_name: "Field Name",
        disturb_date: "Date",
        implement_type: "Standard Implement",
        farmer_implement: "Farmer Implement",
        tillage_depth_in: "Till Depth (in)",
        tillage_depth: "Till Depth",
        tillage_speed_mph: "Till Speed (mph)",
        tillage_speed: "Till Speed",
        area: "Area",
        area_units: "Area Units",
        area_percent: "Area %",
        bed_width_e2e: "E2E bed width (ft)",
        bed_width_c2c: "C2C bed width (ft)",
        aisle_width: "Aisle width (ft)",
        farmer_notes: "Farmer Notes",
        research_notes: "Internal Notes",
        ss_response_id: "SS Response ID",
        ss_record_id: "SS Record ID"
    }, {
        table: "Research Database - Org Inputs 2023.csv",
        organic_input_record_pkey: "Index",
        complete: "Complete Record?",
        survey_year: "Year",
        field_id: "Field ID",
        farm_name: "Farm Name",
        field_name: "Field Name",
        applied_date: "Date",
        input_type: "Standard Input",
        specific_input_type: "Specific Input",
        farmer_input: "Farmer Input",
        area: "Area",
        area_units: "Area Units",
        area_percent: "Area %",
        bed_width_e2e: "E2E bed width (ft)",
        bed_width_c2c: "C2C bed width (ft)",
        aisle_width: "Aisle width (ft)",
        quantity: "Quantity",
        quantity_units: "Quantity Units",
        quantity_lbs: "Quantity (lbs)",
        analysis: "Analysis?",
        pc_moisture: "Moisture %",
        pc_n_wet: "N %",
        pc_p_wet: "P %",
        pc_k_wet: "K %",
        farmer_notes: "Farmer Notes",
        research_notes: "Internal Notes",
        ss_response_id: "SS Response ID",
        ss_record_id: "SS Record ID"
    }, {
        table: "Research Database - Animals 2023.csv",
        grazing_record_pkey: "Index",
        complete: "Complete Record?",
        survey_year: "Year",
        field_id: "Field ID",
        farm_name: "Farm Name",
        field_name: "Field Name",
        graze_start_date: "Start Date",
        graze_end_date: "End Date",
        animal_type: "Standard Animal",
        farmer_animal: "Farmer Animal",
        animal_number: "Animal Number",
        avg_weight_lbs: "Avg Weight (lbs)",
        area: "Area",
        area_units: "Area Units",
        area_percent: "Area %",
        subsections: "Subsections",
        farmer_notes: "Farmer Notes",
        research_notes: "Internal Notes",
        ss_response_id: "SS Response ID",
        ss_record_id: "SS Record ID"
    }
];


// let organic = papa.parse(fs.readFileSync(`${dataFolder}/Research Database - Org Inputs 2023.csv`, "utf8"), { header: true });
// let planting = papa.parse(fs.readFileSync(`${dataFolder}/Research Database - Planting2023.csv`, "utf8"), { header: true });


files.forEach(file => {
    let outputName = file
        .replace("Research Database - ", "")
        .replace(".csv", " - bullet field names.csv")
    ;
    let replacementTable = replacementLists.find( d => d.table == file ); 
    let replacementObjects = Object.keys(replacementTable).map( k => { return { slug: k, natural: replacementTable[k] }; } );
    let outputData = papa.parse(fs.readFileSync(`${dataFolder}/${file}`, "utf8"), { header: true })
    .data
    .map( row => {
        let outputRow = {};
        replacementObjects.forEach( field => {
            outputRow[field.slug] = row[field.natural];
        } );
        return outputRow;
    } )
    ;
    fs.writeFileSync( `${dataFolder}/${ outputName }`, papa.unparse(outputData, {header: true}) );
} );
