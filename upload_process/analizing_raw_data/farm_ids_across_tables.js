const fs = require("fs");
const papa = require("papaparse");
const {
    selectedFarms,
} = require('../submitters/artifacts.js');

const plantings_encoding = require('../encodings/plantings_data_encoding');

let dataSourceFields = plantings_encoding
    .populators
    .flatMap( d => d.source )
    .filter( d => d )
;

let fields = papa.parse(
    fs.readFileSync( "../../raw_data/second_version_tables/shbs_enrolled_fields.2024-07-02.csv", 'utf8' ),
    {
        header:true,
        dynamicTyping:true,
        transform: ( value ) => { return value == 'NA' ? undefined : value; }
    } )
    .data
;
let plantingSecond = papa.parse( fs.readFileSync( `${__dirname}/../../raw_data/second_version_tables/shbs_planting_records.2024-07-02.csv`, "utf8" ), {header:true} );
let plantingFirst = papa.parse( fs.readFileSync( `${__dirname}/../../raw_data/first_version_tables/SHBS.2016-2022.Planting.csv`, "utf8" ), {header:true} );


let redCatFieldsFieldList = fields.filter( d => d.farm_name == selectedFarms[0] )
    .map( d => { return { farm_name: d.farm_name, field_id: d.field_id, field_name: d.field_name }; } )
;

let providentFieldsFieldist = fields.filter( d => d.farm_name == selectedFarms[1] )
    .map( d => { return { farm_name: d.farm_name, field_id: d.field_id, field_name: d.field_name }; } )
;

let redCatFieldsData = [];
let redCatFieldsDataRepeated = plantingSecond.data.filter( d => d.farm_name == selectedFarms[0] )
    .map( d => { return { field_id: d.field_id, field_name: d.field_name, farm_name: d.farm_name }; } )
    .forEach( e => {
        if (!redCatFieldsData.find( a => a.field_id == e.field_id )) {
            redCatFieldsData.push(e);
        }
    } )
;
let providentFieldsData = [];
let providentFieldsDataRepeated = plantingSecond.data.filter( d => d.farm_name == selectedFarms[1] )
    .map( d => { return { field_id: d.field_id, field_name: d.field_name, farm_name: d.farm_name }; } )
    .map( d => { return { field_id: d.field_id, field_name: d.field_name, farm_name: d.farm_name }; } )
    .forEach( e => {
        if (!providentFieldsData.find( a => a.field_id == e.field_id )) {
            providentFieldsData.push(e);
        }
    } )
;


let redCatFieldsDataOld = [];
let redCatFieldsDataRepeatedOld = plantingFirst.data.filter( d => d.farm_name == selectedFarms[0] )
    .map( d => { return { field_id: d.field_id, field_name: d.field_name, farm_name: d.farm_name }; } )
    .forEach( e => {
        if (!redCatFieldsDataOld.find( a => a.field_id == e.field_id )) {
            redCatFieldsDataOld.push(e);
        }
    } )
;
let providentFieldsDataOld = [];
let providentFieldsDataRepeatedOld = plantingFirst.data.filter( d => d.farm_name == selectedFarms[1] )
    .map( d => { return { field_id: d.field_id, field_name: d.field_name, farm_name: d.farm_name }; } )
    .map( d => { return { field_id: d.field_id, field_name: d.field_name, farm_name: d.farm_name }; } )
    .forEach( e => {
        if (!providentFieldsDataOld.find( a => a.field_id == e.field_id )) {
            providentFieldsDataOld.push(e);
        }
    } )
;

let informedRedCatFields = redCatFieldsData.map( d => d.field_id );
let redCatPlantings = plantingSecond.data.filter( d => informedRedCatFields.includes( d.field_id ) );



let allFieldsInList = Array.from( new Set( fields.map( d => d.field_id ) ) ).sort();
let allFieldsData = Array.from( new Set( plantingSecond.data.map( d => d.field_id ) ) ).sort();


let noPlantingFields = allFieldsInList.filter( id => !allFieldsData.includes(id) );
allFieldsData.filter( id => !allFieldsInList.includes(id) );

fs.writeFileSync( "no_planting_fields.csv", papa.unparse(fields.filter( d => noPlantingFields.includes( d.field_id ) )) );
