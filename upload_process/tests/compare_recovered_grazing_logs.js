let recovered = require("../operation_registers/demo_farms/recovered_grazing_table.json");
let source = require("../operation_registers/demo_farms/source_grazing_data.json");
let papa = require("papaparse");
let operationRegister = require("../operation_registers/demo_farms/grazing_upload.json");
let { revertTimezoneInDate, createComparisonArray, checkPairEquality } = require("./comparison_tools.js");

// add an array to tag detected source rows
recovered.forEach( recoveredRow => {
    recoveredRow.sourceRows = [];
} );
recovered.forEach( recoveredRow => {
        let season = recoveredRow.planting_date?.match(/20[0-9]{2}/);
        if (season) {
            recoveredRow.season = season[0];
        };
    // removing the timezone modifications
    recoveredRow.graze_start_date = recoveredRow.graze_start_date ? revertTimezoneInDate(recoveredRow.graze_start_date) : undefined;

    // extract farmer animal from text
    recoveredRow.farmer_animal = recoveredRow.notes ? recoveredRow.notes.match(/(?<=Farmer animal name:\<\/bold\> ).*(?=\<\/blockquote\>)/) : undefined;
    recoveredRow.farmer_animal = recoveredRow.farmer_animal ? recoveredRow.farmer_animal[0].replace(/\s$/, "") : undefined;
    // extract farmer notes from text
    recoveredRow.farmer_notes = recoveredRow.notes ? recoveredRow.notes.match(/(?<=Farmer notes:\<\/bold\> )[^\<]*(?=\s)/) : undefined;
    recoveredRow.farmer_notes = recoveredRow.farmer_notes ? recoveredRow.farmer_notes[0].replace(/((\s))$/g, "") : undefined;
    // extract research notes from text
    recoveredRow.research_notes = recoveredRow.notes ? recoveredRow.notes.match(/(?<=Research notes:\<\/bold\> )[^\<]*(?=\s)/) : undefined;
    recoveredRow.research_notes = recoveredRow.research_notes ? recoveredRow.research_notes[0].replace(/\s$/, "") : undefined;
} );

let pairUniqueGrazingLogs = source.map(sourceGrazingLog => {
    let recoveredGrazingLogs = recovered.filter(recoveredGrazingLog => {
        return recoveredGrazingLog.grazing_start_log_name == sourceGrazingLog.grazing_start_log_name && recoveredGrazingLog.field_id == sourceGrazingLog.field_id;
    });
    recoveredGrazingLogs.forEach(recoveredRow => recoveredRow.sourceRows.push(sourceGrazingLog.grazing_record_pkey));
    return {
        sourceGrazingLog: sourceGrazingLog,
        pairedGrazingLogs: recoveredGrazingLogs,
        field_id: sourceGrazingLog.field_id,
        grazing_start_log_name: sourceGrazingLog.grazing_start_log_name,
        amount_paired: recoveredGrazingLogs.length
    };
});

let tooManyPairs = pairUniqueGrazingLogs.filter(d => d.amount_paired > 1);

tooManyPairs.length;

let missingPairs = pairUniqueGrazingLogs.filter(d => d.amount_paired == 0);

missingPairs.length;

// let example = missingPairs[0].sourceGrazingLog;
// let candidatesArray = recovered.filter(d => d.grazingLogName == example.grazingLogName);
// let candidate = candidatesArray[0];

//// Recovered with more than 1 source candidate.

let ambiguousEntriesArray = recovered.filter(d => d.sourceRows.length > 1);

if (ambiguousEntriesArray.length > 0) {
    let exampleAmbiguous = ambiguousEntriesArray[2];
    let ambiguousSources = source.filter(d => exampleAmbiguous.sourceRows.includes(d.grazing_record_pkey));
};



let grazingLogsSource = `${__dirname}/../../raw_data/third_version_tables/grazing_records_2024-09-03.csv`;

let grazingLogs = papa.parse(
    fs.readFileSync(grazingLogsSource, 'utf8'),
    {
        header: true,
        dynamicTyping: true,
        transform: (value) => { return value == 'NA' ? undefined : value; }
    })
    .data
    ;

let omitedFields = [
    'grazing_record_pkey',
    "created_at",
    "updated_at",
    "survey_year",
    "notes",
    "complete",
    "farm_id",
    // currently a constant, we only use "area percent"
    "area",
    "area_units",
    "paired",
    "planting",
    "planting_name",
    "plantings_amount",
    "season",
    "graze_end_date",
    "grazing_ending_log_name",
    "graze_days"
];

let omitedFieldsSynonims = [
    "grazingLog_record_pkey",
    "created_at",
    "updated_at",
    "survey_year",
    "term_date",
    "farmer_crop_name",
    "research_notes",
    "area",
    "area_units",
    "complete",
];

// checking if source entries are similar to the paired recovered entries, field by field
let comparePairedEntries = pairUniqueGrazingLogs
    .filter(pair => pair.pairedGrazingLogs.length == 1 )
    .map(pair => createComparisonArray({ recoveredEntry: pair.pairedGrazingLogs[0], sourceEntry: pair.sourceGrazingLog, pkeyAttributeName: "grazing_record_pkey", irrelevantFields: omitedFields }) )
;

let firstCheck = comparePairedEntries
    .filter( d => !d.equal )[0]
    ;

let unequalAmount = comparePairedEntries.filter(d => !d.equal).length;
let equalAmount = comparePairedEntries.filter(d => d.equal).length;

console.log(`Equal entries (success): ${equalAmount}, Unequal entries (failures): ${unequalAmount}`);
console.log(`Entries missing a pair: ${missingPairs.length}, entries with more than one pair: ${tooManyPairs.length}`);

if (firstCheck) {
    firstCheck
        .differences
        .filter(d => !omitedFields.includes(d.attribute))
        .filter(d => ![...omitedFields].includes(d.attribute))
        ;
};

let fieldsWithDifferencesByEntry = comparePairedEntries
    .filter(d => !d.equal)
    .map(d => {

        let output = {};
        output.key = d.key;
        output.relevantDifferences = d
            .differences
            .filter(d => !omitedFields.includes(d.attribute))
            .filter(d => ![...omitedFields].includes(d.attribute))
            ;
        output.differentFields = output.relevantDifferences.map(d => d.attribute);
        return output;
    })
    ;

new Set(fieldsWithDifferencesByEntry.flatMap(d => d.differentFields));

    let fieldsToCheck = new Set(fieldsWithDifferencesByEntry.flatMap(d => d.differentFields));
    let freq = ( Array.from( fieldsToCheck ) )
        .map( field => {
            let entries = fieldsWithDifferencesByEntry.filter( d => d.differentFields.includes(field) );
            let output = {
                field: field,
                entries: entries,
                entriesAmount: entries.length,
            };
            return output;
        } );

    let scope = freq[0].entries[0];
    let sourceScope = source.find( d => d.planting_record_pkey == scope.key );
    let recoveredScope = recovered.filter( d => d.grazing_start_log_name == sourceScope.grazing_start_log_name );
    let operationScope = operationRegister.encodedData.payloads[sourceScope.farmDomain].find( d => d.attributes.name == sourceScope.grazing_start_log_name );



///// Checking reasons for ambiguous entries

if (ambiguousEntriesArray.length > 0) {
    let allComparisons = ambiguousEntriesArray.map(d => checkPairEquality({ pkeysArray: d.sourceRows, pkeyAttributeName: "grazing_record_pkey", sourceData: grazingLogs,  irrelevantFields: omitedFields } ) );

    if (allComparisons.filter(d => !d[0].equal).length > 0) {
        allComparisons.filter( d => !d[0].equal )[0][0].differences.filter( dif => !omitedFields.includes( dif.attribute ) );
    }

    let withYear = allComparisons.map(comp => {
        let year = comp[0].attributes.find(d => d.attribute == "survey_year");
        let output = comp[0];
        output.yearFirst = year.sourceValue;
        output.yearSecond = year.recoveredValue;
        return output;
    } );

    let synonimComparisonTable = allComparisons.map( d => {
        let row = {};
        row.source_record_pkey = d[0].key;
        row.compared_with = d[0].compared_with;
        row.different_fields = d[0].differences.map( d => d.attribute ).join(", ");
        d[0].attributes.forEach( attr =>  {
            row[attr.attribute] = attr.equality;
        } );
        return row;
    } );

    fs.writeFileSync("./synonimGrazingEntriesInDemoFarms.csv", papa.unparse( synonimComparisonTable ) );
};

