// testing new season approach. 
exports.revertTimezoneInDate = function revertTimezoneInDate(stringISODate) {
    let secondsFixed = ((new Date(stringISODate).getTime() / 1000) - 18000);
    let formattedDate = new Date(secondsFixed*1000);
    return formattedDate.toISOString();
};

/**
 * This function will compare source entries in the table against the paired recovered entities, delivering an object describing the results..
 * @param {} recoveredEntry
 * @param {} sourceEntry
 * @param {} irrelevantFields
 * @returns {} 
 */
function createComparisonArray({ recoveredEntry, sourceEntry, pkeyAttributeName, irrelevantFields = [] }) {
    let comparison = {};
    comparison.key = sourceEntry[pkeyAttributeName];
    comparison.attributes = [];
    Object.keys(sourceEntry)
        .forEach(key => {
            let comparisonObject = {};
            comparisonObject.attribute = key;
            comparisonObject.equality = !Array.isArray(sourceEntry[key]) ? recoveredEntry[key] == sourceEntry[key] : (recoveredEntry[key].every(e => sourceEntry[key].includes(e)) && sourceEntry[key].every(e => recoveredEntry[key].includes(e)));
            comparisonObject.sourceValue = sourceEntry[key];
            comparisonObject.recoveredValue = recoveredEntry[key];
            comparison.attributes.push(comparisonObject);
        });
    comparison.equal = comparison
        .attributes
        .filter(attr => !irrelevantFields.includes(attr.attribute))
        .every(att => att.equality)
        ;
    comparison.differences = comparison.attributes.filter(d => !d.equality);
    return comparison;
};


/**
 * This funtion will compare pairs of entities mentioned in the pkeysArray, belonging to the sourceData array, to see if they are synonims.
 * @param {} pkeysArray
 * @param {} sourceData
 * @param {} irrelevantFields
 * @returns {} 
 */
function checkPairEquality({ pkeysArray, pkeyAttributeName, sourceData, irrelevantFields = omitedFields }) {
    let firstEntryKey = pkeysArray[0];
    let otherEntryKeys = pkeysArray.filter(d => d != firstEntryKey);


    let firstEntry = sourceData.find(d => d[pkeyAttributeName] == firstEntryKey);
    let otherEntries = sourceData.filter(d => otherEntryKeys.includes(d[pkeyAttributeName]));

    let output = [];

    otherEntries.forEach(entry => {
        let comparison = createComparisonArray({
            recoveredEntry: firstEntry,
            sourceEntry: entry,
            irrelevantFields: irrelevantFields
        });;
        comparison.compared_with = firstEntryKey;
        output.push(comparison);
    });
    return output;
};

exports.createComparisonArray = createComparisonArray;
exports.checkPairEquality = checkPairEquality;
