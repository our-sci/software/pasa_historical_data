let recovered = require("../operation_registers/demo_farms/recovered_disturbance_table.json");
let source = require("../operation_registers/demo_farms/source_tillage_data.json");
let papa = require("papaparse");
let operationRegister = require("../operation_registers/demo_farms/tillage_upload.json");
let { revertTimezoneInDate, createComparisonArray, checkPairEquality } = require("./comparison_tools.js");

// add an array to tag detected source rows
recovered.forEach( recoveredRow => {
    recoveredRow.sourceRows = [];
} );
recovered.forEach( recoveredRow => {
        let season = recoveredRow.planting_date?.match(/20[0-9]{2}/);
        if (season) {
            recoveredRow.season = season[0];
        };
    // removing the timezone modifications
    recoveredRow.disturb_date = recoveredRow.disturb_date ? revertTimezoneInDate(recoveredRow.disturb_date) : undefined;
    recoveredRow.farmer_implement = recoveredRow.notes ? recoveredRow.notes.match(/(?<=Farmer implement name:\<\/bold\> ).*(?=\<\/blockquote\>)/) : undefined;
    recoveredRow.farmer_implement = recoveredRow.farmer_implement ? recoveredRow.farmer_implement[0].replace(/\<\/blockquote\>.*/, "") : undefined;
    recoveredRow.farmer_implement = recoveredRow.farmer_implement ? recoveredRow.farmer_implement.replace(/\s$/, "") : undefined;
} );

let pairUniqueDisturbLogs = source.map(sourceDisturbLog => {
    let recoveredDisturbLogs = recovered.filter(recoveredDisturbLog => {
        return recoveredDisturbLog.disturb_log_name == sourceDisturbLog.disturb_log_name && recoveredDisturbLog.field_id == sourceDisturbLog.field_id && recoveredDisturbLog.season == sourceDisturbLog.season;
    });
    recoveredDisturbLogs.forEach(recoveredRow => recoveredRow.sourceRows.push(sourceDisturbLog.disturbance_record_pkey));
    return {
        sourceDisturbLog: sourceDisturbLog,
        pairedDisturbLogs: recoveredDisturbLogs,
        field_id: sourceDisturbLog.field_id,
        disturbLogName: sourceDisturbLog.disturbLogName,
        amount_paired: recoveredDisturbLogs.length
    };
});

let tooManyPairs = pairUniqueDisturbLogs.filter(d => d.amount_paired > 1);

tooManyPairs.length;

let missingPairs = pairUniqueDisturbLogs.filter(d => d.amount_paired == 0);

missingPairs.length;

if (missingPairs.length > 0) {
    let example = missingPairs[0].sourceDisturbLog;
    let candidatesArray = recovered.filter(d => d.disturbLogName == example.disturbLogName);
    let candidate = candidatesArray[0];
}

// it seems all missing entries have no date
missingPairs.map( d => d.disturb_date );

// it seems the ambiguous pairings are due to non uniqueness of the log names

// ["disturbLogName", "field_id", "survey_year"].map(attr => candidate[attr] == example[attr]);


//// Recovered with more than 1 source candidate.
let omitedFields = [
    'disturbance_record_pkey',
    "created_at",
    "updated_at",
    "survey_year",
    "notes",
    "complete",
    "farm_id",
    // currently a constant, we only use "area percent"
    "area",
    "area_units",
    "paired",
    "planting",
    "plantings_amount",
    // we only use tillage_speed_mph
    "tillage_speed",
    // we only use tillage_dept_in
    "tillage_depth",
    "ss_response_id",
    "ss_record_id"
];

let omitedFieldsSynonims = [
    "disturbLog_record_pkey",
    "created_at",
    "updated_at",
    "survey_year",
    "term_date",
    "farmer_crop_name",
    "research_notes",
    "area",
    "area_units",
    "complete",
];


let ambiguousEntriesArray = recovered.filter(d => d.sourceRows.length > 1);

if (ambiguousEntriesArray.length > 0) {
    if (ambiguousEntriesArray.length > 0) {
        let exampleAmbiguous = ambiguousEntriesArray[2];
        let ambiguousSources = source.filter(d => exampleAmbiguous.sourceRows.includes(d.disturbance_record_pkey));
    };
}


// checking if source entries are similar to the paired recovered entries, field by field
let comparePairedEntries = pairUniqueDisturbLogs
    .filter(pair => pair.pairedDisturbLogs.length == 1)
    .map(pair => createComparisonArray({ recoveredEntry: pair.pairedDisturbLogs[0], sourceEntry: pair.sourceDisturbLog, pkeyAttributeName: "disturbance_record_pkey", irrelevantFields: omitedFields }))
    ;

let unequalAmount = comparePairedEntries.filter(d => !d.equal).length;
let equalAmount = comparePairedEntries.filter(d => d.equal).length;

console.log(`Equal entries (success): ${equalAmount}, Unequal entries (failures): ${unequalAmount}`);
console.log( `Missing pair: ${ missingPairs.length }, more than one pair: ${tooManyPairs.length}` );

let firstCheck = comparePairedEntries
    .filter(d => !d.equal)[0]
    ;

if (firstCheck) {
    firstCheck
        .differences
        .filter(d => !omitedFields.includes(d.attribute))
        .filter(d => ![...omitedFields].includes(d.attribute))
        ;
};

let fieldsWithDifferencesByEntry = comparePairedEntries
    .filter(d => !d.equal)
    .map(d => {

        let output = {};
        output.key = d.key;
        output.relevantDifferences = d
            .differences
            .filter(d => !omitedFields.includes(d.attribute))
            .filter(d => ![...omitedFields].includes(d.attribute))
            ;
        output.differentFields = output.relevantDifferences.map(d => d.attribute);
        return output;
    })
    ;

new Set(fieldsWithDifferencesByEntry.flatMap(d => d.differentFields));

///// Checking reasons for ambiguous entries

if (ambiguousEntriesArray.length > 0) {
    let allComparisons = ambiguousEntriesArray.map(d => checkPairEquality({ pkeysArray: d.sourceRows, pkeyAttributeName: "disturbance_record_pkey", sourceData: disturbLogs, irrelevantFields: omitedFields }));

    // compare to source data
    let disturbLogsSource = `${__dirname}/../../raw_data/third_version_tables/disturbance_records_2024-09-03.csv`;

    let disturbLogs = papa.parse(
        fs.readFileSync(disturbLogsSource, 'utf8'),
        {
            header: true,
            dynamicTyping: true,
            transform: (value) => { return value == 'NA' ? undefined : value; }
        })
        .data
        ;


    allComparisons.filter(d => !d[0].equal).length;

    let withYear = allComparisons.map(comp => {
        let year = comp[0].attributes.find(d => d.attribute == "survey_year");
        let output = comp[0];
        output.yearFirst = year.sourceValue;
        output.yearSecond = year.recoveredValue;
        return output;
    } );

    let synonimComparisonTable = allComparisons.map( d => {
        let row = {};
        row.source_record_pkey = d[0].key;
        row.compared_with = d[0].compared_with;
        row.different_fields = d[0].differences.map( d => d.attribute ).join(", ");
        d[0].attributes.forEach( attr =>  {
            row[attr.attribute] = attr.equality;
        } );
        return row;
    } );

    fs.writeFileSync("./synonimTillageEntriesInDemoFarms.csv", papa.unparse( synonimComparisonTable ) );
}

