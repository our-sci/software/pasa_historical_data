let recovered = require("../operation_registers/demo_farms/recovered_plantings_table.json");
let source = require("../operation_registers/demo_farms/source_planting_data.json");
let papa = require("papaparse");
let operationRegister = require("../operation_registers/demo_farms/plantings_upload.json");
let { revertTimezoneInDate, createComparisonArray, checkPairEquality } = require("./comparison_tools.js");

// submissions either have a termination or a harvest log, therefore always one of them will be missing.  We will clear the attribute here to allow them to coincide.
source.forEach( row => {
    if (row.harvest_date) {
        delete row.termination_log_name;
    } else if (row.term_date) {
        delete row.harvest_log_name;
    };
    if (!Array.isArray(row.crop_name)) {
        row.crop_name = [row.crop_name];
    }
});

// add an array to tag detected source rows
recovered.forEach(recoveredRow => {
    recoveredRow.sourceRows = [];
});
recovered.forEach(recoveredRow => {
    let season = recoveredRow.planting_date?.match(/20[0-9]{2}/);
    if (season) {
        recoveredRow.season = season[0];
    };
    if (!Array.isArray(recoveredRow.crop_name)) {
        recoveredRow.crop_name = [recoveredRow.crop_name];
    }
    // removing the timezone modifications
    recoveredRow.harvest_date = recoveredRow.harvest_date ? revertTimezoneInDate(recoveredRow.harvest_date) : undefined;
    recoveredRow.term_date = recoveredRow.term_date ? revertTimezoneInDate(recoveredRow.term_date) : undefined;
    recoveredRow.planting_date = recoveredRow.planting_date ? revertTimezoneInDate(recoveredRow.planting_date) : undefined;
} );

let pairUniquePlantings = source.map(sourcePlanting => {
    let recoveredPlantings = recovered.filter(recoveredPlanting => {
        return recoveredPlanting.plantingName == sourcePlanting.plantingName && recoveredPlanting.field_id == sourcePlanting.field_id && recoveredPlanting.season == sourcePlanting.season && (recoveredPlanting.termination_log_name == sourcePlanting.termination_log_name) && (recoveredPlanting.harvest_log_name == sourcePlanting.harvest_log_name);
    });
    recoveredPlantings.forEach(recoveredRow => recoveredRow.sourceRows.push(sourcePlanting.planting_record_pkey));
    return {
        sourcePlanting: sourcePlanting,
        pairedPlantings: recoveredPlantings,
        field_id: sourcePlanting.field_id,
        plantingName: sourcePlanting.plantingName,
        amount_paired: recoveredPlantings.length
    };
});

let tooManyPairs = pairUniquePlantings.filter(d => d.amount_paired > 1);

tooManyPairs.length;

let missingPairs = pairUniquePlantings.filter(d => d.amount_paired == 0);


missingPairs.length;

// // check one case

// operationRegister.encodedData.payloads['providentfarmsdemo.farmos.net'].find( d => d.attributes.name == 'red_clover 3/3/2021, 129' )
// recovered
//     .filter( d => d.plantingName == missingPairs[0].sourcePlanting.plantingName )
//     .map( rec =>  {
//         return {
//             termination: rec.termination_log_name == missingPairs[0].sourcePlanting.termination_log_name,
//             harvest: rec.harvest_log_name == missingPairs[0].sourcePlanting.harvest_log_name,
//             seeding: rec.seeding_log_name == missingPairs[0].sourcePlanting.seeding_log_name,
//             termination_value: missingPairs[0].sourcePlanting.termination_log_name,
//             harvest_value: missingPairs[0].sourcePlanting.harvest_log_name, plantingName: rec.plantingName,
//             seeding_value: missingPairs[0].sourcePlanting.seeding_log_name,
//         }; } );

if (missingPairs.length > 0) {
    let example = missingPairs[0].sourcePlanting;
    let candidatesArray = recovered.filter(d => d.plantingName == example.plantingName);
    let candidate = candidatesArray[0];
};

//// Recovered with more than 1 source candidate.

let ambiguousEntriesArray = recovered.filter(d => d.sourceRows.length > 1);

if (ambiguousEntriesArray.length > 0) {
    if (ambiguousEntriesArray.length > 0) {
        let exampleAmbiguous = ambiguousEntriesArray[2];
        let ambiguousSources = source.filter(d => exampleAmbiguous.sourceRows.includes(d.disturbance_record_pkey));
    };
}

let plantingsSource = `${__dirname}/../../raw_data/third_version_tables/planting_records_collapsed_2024-09-03.csv`;

let plantings = papa.parse(
    fs.readFileSync(plantingsSource, 'utf8'),
    {
        header: true,
        dynamicTyping: true,
        transform: (value) => { return value == 'NA' ? undefined : value; }
    })
    .data
    ;

let omitedFields = [
    "planting_record_pkey",
    "created_at",
    "updated_at",
    "survey_year",
    "notes",
    "complete",
    "farm_id",
    // currently a constant
    "area_units",
    "area",
    // farmer crop name is not sent, only listed as a note
    "farmer_crop_name",
    "mixture_id",
    "status"
];

// checking if source entries are similar to the paired recovered entries, field by field
let comparePairedEntries = pairUniquePlantings
    .filter(pair => pair.pairedPlantings.length == 1 )
    .map(pair => createComparisonArray({ recoveredEntry: pair.pairedPlantings[0], sourceEntry: pair.sourcePlanting, irrelevantFields: omitedFields, pkeyAttributeName: "planting_record_pkey" }) )
;

let firstCheck = comparePairedEntries.filter(d => !d.equal)[0];

let unequalAmount = comparePairedEntries.filter(d => !d.equal).length;
let equalAmount = comparePairedEntries.filter(d => d.equal).length;

console.log( `Equal entries (success): ${equalAmount}, Unequal entries (failures): ${unequalAmount}` );
console.log( `Missing pair: ${ missingPairs.length }, more than one pair: ${tooManyPairs.length}` );

if ( unequalAmount > 0 ) {
    let fieldsWithDifferencesByEntry = comparePairedEntries
        .filter(d => !d.equal)
        .map(d => {

            let output = {};
            output.key = d.key;
            output.relevantDifferences = d
                .differences
                .filter(d => !omitedFields.includes(d.attribute))
                .filter(d => ![...omitedFields].includes(d.attribute))
                ;
            output.differentFields = output.relevantDifferences.map(d => d.attribute);
            return output;
        })
        ;

    let fieldsToCheck = new Set(fieldsWithDifferencesByEntry.flatMap(d => d.differentFields));
    let freq = ( Array.from( fieldsToCheck ) )
        .map( field => {
            let entries = fieldsWithDifferencesByEntry.filter( d => d.differentFields.includes(field) );
            let output = {
                field: field,
                entries: entries,
                entriesAmount: entries.length,
            };
            return output;
        } );

    let scope = freq[0].entries[0];
    let sourceScope = source.find( d => d.planting_record_pkey == scope.key );
    let recoveredScope = recovered.filter( d => d.plantingName == sourceScope.plantingName );
    let operationScope = operationRegister.encodedData.payloads[sourceScope.farmDomain].find( d => d.attributes.name == sourceScope.plantingName );
 };


// if (ambiguousEntriesArray.length > 0) {
// let omitedFieldsSynonims = [
//     "planting_record_pkey",
//     "created_at",
//     "updated_at",
//     "survey_year",
//     "term_date",
//     "farmer_crop_name",
//     "mixture_id",
//     "research_notes",
//     "area",
//     "area_units",
//     "yield",
//     "yield_units",
//     "complete",
// ];
//     let allComparisons = ambiguousEntriesArray.map(d => checkPairEquality(d.sourceRows, plantings, omitedFields ) );
//     allComparisons.filter(d => !d[0].equal).length;
//     allComparisons.filter( d => !d[0].equal )[0][0].differences.filter( dif => !omitedFields.includes( dif.attribute ) );
//     let withYear = allComparisons.map(comp => {
//         let year = comp[0].attributes.find(d => d.attribute == "survey_year");
//     let output = comp[0];
//     output.yearFirst = year.sourceValue;
//     output.yearSecond = year.recoveredValue;
//     return output;
// } );

// let synonimComparisonTable = allComparisons.map( d => {
//     let row = {};
//     row.source_record_pkey = d[0].key;
//     row.compared_with = d[0].compared_with;
//     row.different_fields = d[0].differences.map( d => d.attribute ).join(", ");
//     d[0].attributes.forEach( attr =>  {
//         row[attr.attribute] = attr.equality;
//     } );
//     return row;
// } );

// fs.writeFileSync("./synonimEntriesInDemoFarms.csv", papa.unparse( synonimComparisonTable ) );
// }


// // analyzing yield

// let yieldCoincidence = pairUniquePlantings
//     .filter(d => d.amount_paired > 0)
//     .map(d => {
//         let output = {};
//         output.source = d.sourcePlanting.yield, output.recovered = d.pairedPlantings[0].yield;
//         output.equality = d.sourcePlanting.yield ? d.sourcePlanting.yield == d.pairedPlantings[0].yield : false;
//         return output;
//     });
// ;

// let bedWidthCoincidence = pairUniquePlantings
//     .filter( d => d.amount_paired > 0 )
//     .map( d => {
//         let output = {};
//         output.source = d.sourcePlanting.bed_width_ft, output.recovered = d.pairedPlantings[0].bed_width_ft;
//         output.equality = d.sourcePlanting.bed_width_ft ? d.sourcePlanting.bed_width_ft == d.pairedPlantings[0].bed_width_ft : false;
//         return output;
//     } );
// ;
