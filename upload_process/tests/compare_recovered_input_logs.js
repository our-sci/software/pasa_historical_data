let recovered = require("../operation_registers/demo_farms/recovered_inputs_table.json");
let source = require("../operation_registers/demo_farms/source_inputs_data.json");
let papa = require("papaparse");
let operationRegister = require("../operation_registers/demo_farms/inputs_upload.json");
let { revertTimezoneInDate, createComparisonArray, checkPairEquality } = require("./comparison_tools.js");

// add an array to tag detected source rows
recovered.forEach( recoveredRow => {
    recoveredRow.sourceRows = [];
} );
recovered.forEach( recoveredRow => {
        let season = recoveredRow.planting_date?.match(/20[0-9]{2}/);
        if (season) {
            recoveredRow.season = season[0];
        };
    // removing the timezone modifications
    recoveredRow.applied_date = recoveredRow.applied_date ? revertTimezoneInDate(recoveredRow.applied_date) : undefined;
    // extract farmer input from text
    recoveredRow.farmer_input = recoveredRow.notes ? recoveredRow.notes.match(/(?<=Farmer input name:\<\/bold\> )[^\<]*(?=\s)/) : undefined;
    recoveredRow.farmer_input = recoveredRow.farmer_input ? recoveredRow.farmer_input[0].replace(/\s$/, "") : undefined;
    // extract farmer notes from text
    recoveredRow.farmer_notes = recoveredRow.notes ? recoveredRow.notes.match(/(?<=Farmer notes:\<\/bold\> )[^\<]*(?=\s)/) : undefined;
    recoveredRow.farmer_notes = recoveredRow.farmer_notes ? recoveredRow.farmer_notes[0].replace(/\s$/, "") : undefined;
    // extract research notes from text
    recoveredRow.research_notes = recoveredRow.notes ? recoveredRow.notes.match(/(?<=Research notes:\<\/bold\> )[^\<]*(?=\s)/) : undefined;
    recoveredRow.research_notes = recoveredRow.research_notes ? recoveredRow.research_notes[0].replace(/\s$/, "") : undefined;
} );

let pairUniqueInputsLogs = source.map(sourceInputsLog => {
    let recoveredInputsLogs = recovered.filter(recoveredInputsLog => {
        return recoveredInputsLog.input_log_name == sourceInputsLog.input_log_name && recoveredInputsLog.field_id == sourceInputsLog.field_id;
    });
    recoveredInputsLogs.forEach(recoveredRow => recoveredRow.sourceRows.push(sourceInputsLog.organic_input_record_pkey));
    return {
        sourceInputsLog: sourceInputsLog,
        pairedInputsLogs: recoveredInputsLogs,
        field_id: sourceInputsLog.field_id,
        inputsLogName: sourceInputsLog.inputsLogName,
        amount_paired: recoveredInputsLogs.length
    };
});

let tooManyPairs = pairUniqueInputsLogs.filter(d => d.amount_paired > 1);

tooManyPairs.length;

let missingPairs = pairUniqueInputsLogs.filter(d => d.amount_paired == 0);

missingPairs.length;

let ambiguousEntriesArray = recovered.filter(d => d.sourceRows.length > 1);

if (ambiguousEntriesArray.length > 0) {
    let exampleAmbiguous = ambiguousEntriesArray[2];
    let ambiguousSources = source.filter(d => exampleAmbiguous.sourceRows.includes(d.organic_input_record_pkey));
};



let inputsLogsSource = `${__dirname}/../../raw_data/third_version_tables/orginput_records_2024-09-03.csv`;

let inputsLogs = papa.parse(
    fs.readFileSync(inputsLogsSource, 'utf8'),
    {
        header: true,
        dynamicTyping: true,
        transform: (value) => { return value == 'NA' ? undefined : value; }
    })
    .data
    ;

let omitedFields = [
    'organic_input_record_pkey',
    "created_at",
    "updated_at",
    "survey_year",
    "notes",
    "complete",
    "farm_id",
    // currently a constant, we only use "area percent"
    "area",
    "area_units",
    "paired",
    "planting",
    "planting_name",
    "plantings_amount",
    "season",
    // we only use quantity_lbs
    "quantity",
    "quantity_units",
    // only indicates if analysiss happened or not
    "analysis",
    "ss_record_id",
    "ss_response_id",
];

let omitedFieldsSynonims = [
    "inputsLog_record_pkey",
    "created_at",
    "updated_at",
    "survey_year",
    "term_date",
    "farmer_crop_name",
    "research_notes",
    "area",
    "area_units",
    "complete",
];

// checking if source entries are similar to the paired recovered entries, field by field
let comparePairedEntries = pairUniqueInputsLogs
    .filter(pair => pair.pairedInputsLogs.length == 1)
    .map(pair => createComparisonArray({ recoveredEntry: pair.pairedInputsLogs[0], sourceEntry: pair.sourceInputsLog, pkeyAttributeName: "organic_input_record_pkey", irrelevantFields: omitedFields }))
    ;

let unequalAmount = comparePairedEntries.filter(d => !d.equal).length;
let equalAmount = comparePairedEntries.filter(d => d.equal).length;

console.log(`Equal entries (success): ${equalAmount}, Unequal entries (failures): ${unequalAmount}`);
console.log(`Entries missing a pair: ${missingPairs.length}, entries with more than one pair: ${tooManyPairs.length}`);

let firstCheck = comparePairedEntries
    .filter(d => !d.equal)[0]
    ;

if (firstCheck) {
    firstCheck
        .differences
        .filter(d => !omitedFields.includes(d.attribute))
        .filter(d => ![...omitedFields].includes(d.attribute))
        ;
};

let fieldsWithDifferencesByEntry = comparePairedEntries
    .filter(d => !d.equal)
    .map(d => {

        let output = {};
        output.key = d.key;
        output.relevantDifferences = d
            .differences
            .filter(d => !omitedFields.includes(d.attribute))
            .filter(d => ![...omitedFields].includes(d.attribute))
            ;
        output.differentFields = output.relevantDifferences.map(d => d.attribute);
        return output;
    })
    ;

new Set( fieldsWithDifferencesByEntry.flatMap( d => d.differentFields ) );



///// Checking reasons for ambiguous entries

if (ambiguousEntriesArray.length > 0) {
    let allComparisons = ambiguousEntriesArray.map(d => checkPairEquality({ pkeysArray: d.sourceRows, pkeyAttributeName: "organic_input_record_pkey", sourceData: inputsLogs,  irrelevantFields: omitedFields } ) );

    if (allComparisons.filter(d => !d[0].equal).length > 0) {
        allComparisons.filter( d => !d[0].equal )[0][0].differences.filter( dif => !omitedFields.includes( dif.attribute ) );
    }

    let withYear = allComparisons.map(comp => {
        let year = comp[0].attributes.find(d => d.attribute == "survey_year");
        let output = comp[0];
        output.yearFirst = year.sourceValue;
        output.yearSecond = year.recoveredValue;
        return output;
    } );

    let synonimComparisonTable = allComparisons.map( d => {
        let row = {};
        row.source_record_pkey = d[0].key;
        row.compared_with = d[0].compared_with;
        row.different_fields = d[0].differences.map( d => d.attribute ).join(", ");
        d[0].attributes.forEach( attr =>  {
            row[attr.attribute] = attr.equality;
        } );
        return row;
    } );

    fs.writeFileSync("./synonimInputsEntriesInDemoFarms.csv", papa.unparse( synonimComparisonTable ) );
};
