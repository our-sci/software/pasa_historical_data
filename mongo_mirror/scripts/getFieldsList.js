const { declareSHBSMirror } = require('../src/mirror_declaration.js');
const  { processFarmBatch } = require("../src/index.js"); 
const fs = require("fs");
const papa = require("papaparse");


require('dotenv').config({ path: '../../.env' });
let mongoURI = "mongodb://127.0.0.1:27017";
let farmosKey = process.env.FARMOS_KEY;

// database and collection names
let targetDB = 'rfm_biodiversity';
let testingColl = 'pasa_shbs';
let simplifiedEntities = "simplified_entities";
let exampleFarm = "ourscitest.farmos.net";

let productionFarmsTable = JSON.parse( fs.readFileSync(`${__dirname}/../../upload_process/submitters/farmsStageAssignment.json`) )
;

let farmsArray = productionFarmsTable
    .filter( d => d.production )
    .map( d => d.farmDomain )
;

const shbsMirror = declareSHBSMirror( { mongoURI: mongoURI, aggregatorKey: farmosKey } );


let aggregationsArray = await shbsMirror.applyAggregations({ specificAggregation:"fields_table" });
let fieldsTable = aggregationsArray[0];
fs.writeFileSync(`${__dirname}/../../raw_data/fields_ontology.json`, JSON.stringify(fieldsTable));
