const { declareSHBSMirror } = require('../src/mirror_declaration.js');
const  { processFarmBatch } = require("../src/index.js"); 
const fs = require("fs");
const papa = require("papaparse");


require('dotenv').config({ path: '../../.env' });
let mongoURI = "mongodb://127.0.0.1:27017";
let farmosKey = process.env.FARMOS_KEY;

// database and collection names
let targetDB = 'rfm_biodiversity';
let testingColl = 'pasa_shbs';
let simplifiedEntities = "simplified_entities";
let exampleFarm = "ourscitest.farmos.net";

let farmStagesTable = JSON.parse( fs.readFileSync(`${__dirname}/../../upload_process/submitters/farmsStageAssignment.json`) );

// let registerEx = JSON.parse( fs.readFileSync(`${__dirname}/../../upload_process/operation_registers/production_main_batch/farms_upload_initial.json`) );

const shbsMirror = declareSHBSMirror( { mongoURI: mongoURI, aggregatorKey: farmosKey } );
const marylandFarmDomains = farmStagesTable.filter( d => d.stage == "production_maryland_batch" ).map( d => d.farmDomain );

let farmsTable = await shbsMirror.applyAggregations({ specificAggregation:"farms_table" });

let marylandUpload = farmsTable[0]
    .filter( d => marylandFarmDomains.includes(d.farmDomain) )
    .flatMap( d => {
        let land_asset = {
            entity: { data:  d.land_asset[0] },
            farmos_url: d.farmDomain,
            stage: 1,
            status: 202
        };
        let farm_organization = {
            entity: { data: d.farm_organization },
            farmos_url: d.farmDomain,
            stage: 0,
            status: 202
        };
        return [ land_asset, farm_organization ];
    } )
;

let storagePath = `${__dirname}/../../upload_process/operation_registers/production_main_batch/maryland_farms_upload.json`;

fs.writeFileSync( storagePath, JSON.stringify( marylandUpload ) );
