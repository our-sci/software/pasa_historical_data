const { MongoClient } = require("mongodb");
const { declareSHBSMirror } = require('../src/mirror_declaration.js');

require('dotenv').config({ path: '../../.env' });
let mongoURI = "mongodb://127.0.0.1:27017";
let farmosKey = process.env.FARMOS_KEY;

// database and collection names
let targetDB = 'rfm_biodiversity';
let testingColl = 'pasa_shbs';
let simplifiedEntities = "simplified_entities";
let exampleFarm = "ourscitest.farmos.net";

const shbsMirror = declareSHBSMirror( { mongoURI: mongoURI, aggregatorKey: farmosKey } );

// get example farm entities
let retrieve = shbsMirror.retrieveFarmEntities({farmDomain:exampleFarm});
// simplify our example farm
let simplification = shbsMirror.simplifyFarmEntities({ farmDomain: exampleFarm });
