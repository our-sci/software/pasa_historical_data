const { declareSHBSMirror } = require('../src/mirror_declaration.js');
const  { processFarmBatch } = require("../src/index.js"); 
const fs = require("fs");
const papa = require("papaparse");
const plantingsAggregation = require("./aggregations/plantings_recovery.js");
const disturbanceAggregation = require("./aggregations/disturbance_recovery.js");
const grazingAggregation = require("./aggregations/grazing_recovery.js");
const inputsAggregation = require("./aggregations/inputs_recovery.js");
const fieldsAggregation = require("./aggregations/fields_table.js");
const farmsAggregation = require("./aggregations/farms_table.js");


require('dotenv').config({ path: '../../.env' });
let mongoURI = "mongodb://127.0.0.1:27017";
let farmosKey = process.env.FARMOS_KEY;

// database and collection names
let targetDB = 'pasa_shbs_demo';
let simplifiedEntities = "simplified_entities";

let farmStagesTable = JSON.parse(fs.readFileSync(`${__dirname}/../../upload_process/submitters/farmsStageAssignment.json`));

let farmsArray = farmStagesTable
    .filter(d => d.stage == "demo_farms")
    .map(d => d.farmDomain)
    ;


// Aggregations we are still testing
let aggregationsArray = [
    {
        name: "farms_table",
        aggregationPipeline: farmsAggregation.aggregation,
        sourceCollection: "raw_entities",
        db: 'pasa_shbs_demo',
        order: 0,
        index: "id",
    },
    {
        name: "fields_table",
        aggregationPipeline: fieldsAggregation.aggregation,
        sourceCollection: "raw_entities",
        db: 'pasa_shbs_demo',
        order: 1,
        index: "id",
    }
];
let extraAggregations = [
    {
        name: "plantings_table",
        aggregationPipeline: plantingsAggregation.aggregation,
        sourceCollection: "raw_entities",
        db: 'pasa_shbs_demo',
        order: 2,
        index: "field_id",
    },
    {
        name: "disturbance_table",
        aggregationPipeline: disturbanceAggregation.aggregation,
        sourceCollection: "raw_entities",
        db: 'pasa_shbs_demo',
        order: 3,
        index: "field_id",
    },
    {
        name: "inputs_table",
        aggregationPipeline: inputsAggregation.aggregation,
        sourceCollection: "raw_entities",
        db: 'pasa_shbs_demo',
        order: 4,
        index: "field_id",
    },
    {
        name: "grazing_table",
        aggregationPipeline: grazingAggregation.aggregation,
        sourceCollection: "raw_entities",
        db: 'pasa_shbs_demo',
        order: 5,
        index: "field_id",
    }
];


const shbsMirror = declareSHBSMirror({ mongoURI: mongoURI, aggregatorKey: farmosKey, aggregations: [...aggregationsArray, ...extraAggregations] });
shbsMirror.databaseName = targetDB;
shbsMirror.aggregations.forEach(agg => agg.db = "pasa_shbs_demo");

// let operations = await processFarmBatch({ mirror: shbsMirror, mongoURI: mongoURI, farmDomainsArray: farmsArray });


let farmsTable = await shbsMirror.applyAggregations({ specificAggregation: "farms_table" });
let fieldsTable = await shbsMirror.applyAggregations({ specificAggregation: "fields_table" });
let plantingsTable = await shbsMirror.applyAggregations({ specificAggregation: "plantings_table" });
let disturbanceTable = await shbsMirror.applyAggregations({ specificAggregation: "disturbance_table" });
let grazingTable = await shbsMirror.applyAggregations({ specificAggregation: "grazing_table" });
let inputsTable = await shbsMirror.applyAggregations({ specificAggregation: "inputs_table" });

delete plantingsTable[0]["aggregationName"];
delete plantingsTable[0]["success"];
delete disturbanceTable[0]["aggregationName"];
delete disturbanceTable[0]["success"];
delete inputsTable[0]["aggregationName"];
delete inputsTable[0]["success"];
delete grazingTable[0]["aggregationName"];
delete grazingTable[0]["success"];

fs.writeFileSync(`${__dirname}/../../upload_process/operation_registers/demo_farms/recovered_plantings_table.json`, JSON.stringify(plantingsTable[0]));
fs.writeFileSync(`${__dirname}/../../upload_process/operation_registers/demo_farms/recovered_disturbance_table.json`, JSON.stringify(disturbanceTable[0]));
fs.writeFileSync(`${__dirname}/../../upload_process/operation_registers/demo_farms/recovered_inputs_table.json`, JSON.stringify(inputsTable[0]));
fs.writeFileSync( `${__dirname}/../../upload_process/operation_registers/demo_farms/recovered_grazing_table.json`, JSON.stringify(grazingTable[0]) );
