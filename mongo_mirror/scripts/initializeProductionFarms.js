const { declareSHBSMirror } = require('../src/mirror_declaration.js');
const  { processFarmBatch } = require("../src/index.js"); 
const fs = require("fs");
const papa = require("papaparse");


require('dotenv').config({ path: '../../.env' });
let mongoURI = "mongodb://127.0.0.1:27017";
let farmosKey = process.env.FARMOS_KEY;

// database and collection names
let targetDB = 'rfm_biodiversity';
let testingColl = 'pasa_shbs';
let simplifiedEntities = "simplified_entities";
let exampleFarm = "ourscitest.farmos.net";

let productionFarmsTable = JSON.parse( fs.readFileSync(`${__dirname}/../../upload_process/submitters/farmsStageAssignment.json`) )
;

let farmsArray = productionFarmsTable
    .filter( d => d.production )
    .map( d => d.farmDomain )
;

const shbsMirror = declareSHBSMirror( { mongoURI: mongoURI, aggregatorKey: farmosKey } );

// let operations = await processFarmBatch({ mirror: shbsMirror, mongoURI: mongoURI, farmDomainsArray: farmsArray});

let farmsTable = await shbsMirror.applyAggregations({ specificAggregation:"farms_table" });

let originalTable = papa.parse( fs.readFileSync(`${__dirname}/../../raw_data/production_farms_corrected.csv`, "utf8"), {header:true} ).data;

let farmsControlTable = farmsTable[0]
    .filter( d => farmsArray.includes( d.farmDomain ) )
    .map( row => {
        let originalEntry = originalTable.find( d => d['Account: Research ID'] == row.farm_organization?.attributes.pasa_farm_id);
        let output = {
            "farmOS ID": row.farmDomain,
            "Account: Research ID": row.farm_organization?.attributes.pasa_farm_id,
            "Account: Account Name": row.farm_organization?.attributes.name,
            amount_of_farm_organizations: row.amount_farm_organization,
            amount_of_land_properties: row.amount_property_entity,
            comment: ""
        };
        if ( originalEntry ) {
            let matches_original_table = output["farmOS ID"] == originalEntry["farmOS ID"] &&
                output["Account: Account Name"] == originalEntry["Account: Account Name"];
            output.matches_original_table = matches_original_table;
        } else {
            output.matches_original_table = false;
        }
        return output;
    } );

let unsentFarms = originalTable.filter( d => !farmsControlTable.map(e => e["farmOS ID"]).includes( d["farmOS ID"] ) );

let unexplainedErrors = farmsControlTable
    .filter( d => !d.matches_original_table )
    .map( d => d['farmOS ID'] );

let doubleSpace = farmsControlTable.filter(d => unexplainedErrors.includes( d["farmOS ID"] ) );

fs.writeFileSync( `${__dirname}/../summaries/farm_creation_control-${new Date().toISOString().split("T")[0]}.csv`, papa.unparse(farmsControlTable) );

let fieldsTable = await shbsMirror.applyAggregations({ specificAggregation:"fields_table" });

let originalFieldsTable = papa.parse( fs.readFileSync(`${__dirname}/../../raw_data/second_version_tables/shbs_enrolled_fields.2024-07-02.csv`, "utf8"), {header:true} ).data;
let fieldsMissingGeometry = JSON.parse( fs.readFileSync(`${__dirname}/../../upload_process/operation_registers/production_main_batch/fields_missing_geometry.json`) );

let fieldsControlTable = fieldsTable[0]
    .filter( d => farmsArray.includes( d.farmos_url ) )
    .map( row => {
        let originalEntry = originalFieldsTable.find( d => d.field_id == row.field_id );
        row.originalEntry = originalEntry;
        if (originalEntry) {
            row.paired = true;
            row.matches = ( originalEntry.farm_id == row.farm_id ) && ( originalEntry.field_name == row.field_name ) && ( originalEntry.farm_name == row.farm_name );
            originalEntry.farmos_url = row.farmos_url;
        } else {
            row.paired = false;
        };
        row.missing_geometry = fieldsMissingGeometry.findIndex( d => d.field_id == row.field_id ) != -1;
        row.missing_farmos_url = false;
        return row;
} );

let processedFarms = productionFarmsTable.map( d => d.farmDomain );

let allFieldIDsInFarmOS = fieldsControlTable
    .map( d => d.field_id )
    .filter( d => d )
;

let notSentFields = originalFieldsTable
    .filter( d => !allFieldIDsInFarmOS.includes(d.field_id) )
    .filter( d => !fieldsMissingGeometry.find( e => d.field_id == d.field_id ) )
    .map( d => {
        let entryInFarmsTable = originalTable.find( a => a['Account: Research ID'] == d.farm_id );
        d.farmos_url =  entryInFarmsTable? entryInFarmsTable['farmOS ID'] : undefined;
        d.is_maryland = /^MD/.test(d.farm_id);
        if (!d.farmos_url) {
            d.missing_farmos_url = true;
        }
        return d;
    } )
;
notSentFields.map( d => originalTable.find( a => a['Account: Research ID'] == d.farm_id ) ).filter( d => d );

let allFieldControlEntries = [
    ... fieldsControlTable,
    ... notSentFields
];


new Set( allFieldControlEntries.filter( d => !d.paired ).map( d => d.farmos_url ) );

allFieldControlEntries.forEach( d => delete d.originalEntry );
fs.writeFileSync( `${__dirname}/../summaries/field_creation_control-${new Date().toISOString().split("T")[0]}.csv`, papa.unparse(allFieldControlEntries) );
