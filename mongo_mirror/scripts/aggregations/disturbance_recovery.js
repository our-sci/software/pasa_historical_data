exports.aggregation = [
  {
    '$match': {
      'type': 'log--activity', 
      'attributes.name': {
        '$regex': 'Disturb:'
      }
    }
  }, {
    '$addFields': {
      'quantity_ids': {
        '$map': {
          'input': '$relationships.quantity.data', 
          'as': 'rel', 
          'in': '$$rel.id'
        }
      }, 
      'land_asset_ids': {
        '$map': {
          'input': '$relationships.location.data', 
          'as': 'rel', 
          'in': '$$rel.id'
        }
      }, 
      'implement_ids': {
        '$map': {
          'input': '$relationships.equipment.data', 
          'as': 'rel', 
          'in': '$$rel.id'
        }
      }
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'quantity_ids', 
      'foreignField': 'id', 
      'as': 'quantities'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'land_asset_ids', 
      'foreignField': 'id', 
      'as': 'land_assets'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'implement_ids', 
      'foreignField': 'id', 
      'as': 'equipment'
    }
  }, {
    '$addFields': {
      'depth_quantity': {
        '$filter': {
          'input': '$quantities', 
          'as': 'quantity', 
          'cond': {
            '$eq': [
              '$$quantity.attributes.label', 'Tillage depth'
            ]
          }
        }
      }, 
      'speed_quantity': {
        '$filter': {
          'input': '$quantities', 
          'as': 'quantity', 
          'cond': {
            '$eq': [
              '$$quantity.attributes.label', 'Tillage speed'
            ]
          }
        }
      }, 
      'area_quantity': {
        '$filter': {
          'input': '$quantities', 
          'as': 'quantity', 
          'cond': {
            '$eq': [
              '$$quantity.attributes.label', 'Area affected'
            ]
          }
        }
      }, 
      'farm_property_land_asset': {
        '$max': {
          '$max': '$land_assets.relationships.parent.data.id'
        }
      }
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'farm_property_land_asset', 
      'foreignField': 'id', 
      'as': 'farm_property_land_asset'
    }
  }, {
    '$project': {
      'field_id': 1, 
      'field_name': {
        '$max': '$land_assets.attributes.name'
      }, 
      'survey_year': 1, 
      'farm_name': {
        '$max': '$farm_property_land_asset.attributes.name'
      }, 
      'implement_type': {
        '$max': '$equipment.attributes.name'
      }, 
      'disturb_date': '$attributes.timestamp', 
      'tillage_depth_in': {
        '$toDouble': {
          '$max': '$depth_quantity.attributes.value.decimal'
        }
      }, 
      'tillage_speed_mph': {
        '$toDouble': {
          '$max': '$speed_quantity.attributes.value.decimal'
        }
      }, 
      'area_percent': {
        '$toDouble': {
          '$max': '$area_quantity.attributes.value.decimal'
        }
      }, 
      'area_units': 1, 
      'field_id': {
        '$max': '$land_assets.attributes.pasa_field_id'
      }, 
      'field_name': {
        '$max': '$land_assets.attributes.name'
      }, 
      'notes': '$attributes.notes.value', 
      'farmDomain': 1, 
      'disturb_log_name': '$attributes.name'
    }
  }
];
