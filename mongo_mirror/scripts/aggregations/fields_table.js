exports.aggregation = [
  {
    '$match': {
      'type': 'asset--land', 
      'attributes.land_type': 'field'
    }
  }, {
    '$project': {
      'farmos_url': '$farmDomain', 
      'field_name': '$attributes.name', 
      'pasa_field_id': '$attributes.pasa_field_id', 
      'farm_organization_id': {
        '$let': {
          'vars': {
            'rel': {
              '$max': '$relationships.farm.data'
            }
          }, 
          'in': '$$rel.id'
        }
      }
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'farm_organization_id', 
      'foreignField': 'id', 
      'as': 'farm_organization_entity'
    }
  }, {
    '$addFields': {
      'amount_of_organization_entities': {
        '$size': '$farm_organization_entity'
      }, 
      'farm_organization_entity': {
        '$max': '$farm_organization_entity'
      }
    }
  }, {
    '$project': {
      'farmos_url': 1, 
      'field_name': 1, 
      'field_id': '$pasa_field_id', 
      'farm_name': '$farm_organization_entity.attributes.name', 
      'farm_id': '$farm_organization_entity.attributes.pasa_farm_id'
    }
  }
];
