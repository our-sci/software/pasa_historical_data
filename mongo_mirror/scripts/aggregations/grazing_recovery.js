exports.aggregation = [
  {
    '$match': {
      'type': 'log--activity', 
      'attributes.name': {
        '$regex': new RegExp('Grazing start\:')
      },
      "attributes.created": { $gt: "2025-02-04T07:00:00+00:00" }
    }
  }, {
    '$addFields': {
      'quantity_ids': {
        '$map': {
          'input': '$relationships.quantity.data', 
          'as': 'rel', 
          'in': '$$rel.id'
        }
      }, 
      'land_asset_ids': {
        '$map': {
          'input': '$relationships.location.data', 
          'as': 'rel', 
          'in': '$$rel.id'
        }
      }, 
      'animal_asset_ids': {
        '$map': {
          'input': '$relationships.asset.data', 
          'as': 'rel', 
          'in': '$$rel.id'
        }
      }
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'quantity_ids', 
      'foreignField': 'id', 
      'as': 'quantities'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'land_asset_ids', 
      'foreignField': 'id', 
      'as': 'land_assets'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'animal_asset_ids', 
      'foreignField': 'id', 
      'as': 'animal_assets'
    }
  }, {
    '$addFields': {
      'quantities_processed': {
        '$arrayToObject': {
          '$map': {
            'input': '$quantities', 
            'as': 'quant', 
            'in': {
              'k': '$$quant.attributes.label', 
              'v': '$$quant.attributes.value.decimal'
            }
          }
        }
      }, 
      'farm_property_land_asset': {
        '$max': {
          '$max': '$land_assets.relationships.parent.data.id'
        }
      }
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'farm_property_land_asset', 
      'foreignField': 'id', 
      'as': 'farm_property_land_asset'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'animal_assets.relationships.animal_type.data.id', 
      'foreignField': 'id', 
      'as': 'animal_type_taxonomy_term'
    }
  }, {
    '$project': {
      'field_id': {
        '$max': '$land_assets.attributes.pasa_field_id'
      }, 
      'field_name': {
        '$max': '$land_assets.attributes.name'
      }, 
      'farm_name': {
        '$max': '$farm_property_land_asset.attributes.name'
      }, 
      'survey_year': 1, 
      'animal_asset_name': {
        '$max': '$animal_assets.attributes.name'
      }, 
      'animal_type': {
        '$max': '$animal_type_taxonomy_term.attributes.name'
      }, 
      'graze_start_date': '$attributes.timestamp', 
      'area': {
        '$toDouble': {
          '$max': '$area_quantity.attributes.value.decimal'
        }
      }, 
      'animal_number': {
        '$toDouble': '$quantities_processed.herd size'
      }, 
      'area_percent': {
        '$toDouble': '$quantities_processed.area percentage'
      }, 
      'avg_weight_lbs': {
        '$toDouble': '$quantities_processed.average animal weight'
      }, 
      'subsections': {
        '$toDouble': '$quantities_processed.involved subsections'
      }, 
      'field_name': {
        '$max': '$land_assets.attributes.name'
      }, 
      'notes': '$attributes.notes.value', 
      'farmDomain': 1, 
      'grazing_start_log_name': '$attributes.name'
    }
  }
];
