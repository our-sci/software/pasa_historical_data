// This is a mongo query language aggregation that will get the initial dataset back from the FarmOS encoded collection of simplified entities that is stored within the mirror.
exports.aggregation = [
  {
    '$match': {
      'type': 'log--activity'
    }
  }, {
    '$lookup': {
      'from': 'simplified_entities', 
      'localField': 'asset_land_id', 
      'foreignField': '_id', 
      'as': 'land_asset_entity'
    }
  }, {
    '$lookup': {
      'from': 'simplified_entities', 
      'localField': 'asset_plant_id', 
      'foreignField': '_id', 
      'as': 'plant_asset_entity'
    }
  }, {
    '$addFields': {
      'land_asset_entity': {
        '$max': '$land_asset_entity'
      }, 
      'plant_asset_entity': {
        '$max': '$plant_asset_entity'
      }, 
      'planting_name': '$asset_plant_name', 
      'disturb_date': '$timestamp', 
      'survey_year': '$plant_asset_entity.season'
    }
  }, {
    '$addFields': {
      'quantitiesObject': {
        '$arrayToObject': {
          '$map': {
            'input': '$standard_quantities', 
            'as': 'entry', 
            'in': {
              'k': '$$entry.label', 
              'v': {
                '$divide': [
                  '$$entry.value.numerator', '$$entry.value.denominator'
                ]
              }
            }
          }
        }
      }, 
      'landIdTagsObject': {
        '$arrayToObject': {
          '$map': {
            'input': '$land_asset_entity.id_tag', 
            'as': 'tag', 
            'in': {
              'k': '$$tag.location', 
              'v': '$$tag.id'
            }
          }
        }
      }, 
      'farm_name': '$asset_land_name'
    }
  }, {
    '$replaceRoot': {
      'newRoot': {
        '$mergeObjects': [
          '$$ROOT', '$quantitiesObject', '$landIdTagsObject'
        ]
      }
    }
  }, {
    '$addFields': {
      'tillage_depth_in': '$Tillage depth', 
      'area_percent': '$Area amended'
    }
  }, {
    '$project': {
      'complete': 1, 
      'field_id': 1, 
      'farm_id': 1, 
      'survey_year': 1, 
      'farm_name': 1, 
      'field_name': 1, 
      'field_area_acres': 1, 
      'disturb_date': 1, 
      'implement_type': 1, 
      'farmer_implement': 1, 
      'tillage_depth_in': 1, 
      'tillage_speed_mph': 1, 
      'area_percent': 1, 
      'bed_width_ft': 1, 
      'planting_name': 1
    }
  }
];
