exports.aggregation = [
  {
    '$match': {
      'type': 'log--input', 
      'attributes.name': {
        '$regex': new RegExp('Input')
      }
    }
  }, {
    '$addFields': {
      'quantity_ids': {
        '$map': {
          'input': '$relationships.quantity.data', 
          'as': 'rel', 
          'in': '$$rel.id'
        }
      }, 
      'land_asset_ids': {
        '$map': {
          'input': '$relationships.location.data', 
          'as': 'rel', 
          'in': '$$rel.id'
        }
      }, 
      'equipment_asset_ids': {
        '$map': {
          'input': '$relationships.equipment.data', 
          'as': 'rel', 
          'in': '$$rel.id'
        }
      }, 
      'category_ids': {
        '$map': {
          'input': '$relationships.category.data', 
          'as': 'rel', 
          'in': '$$rel.id'
        }
      }
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'quantity_ids', 
      'foreignField': 'id', 
      'as': 'quantities'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'land_asset_ids', 
      'foreignField': 'id', 
      'as': 'land_assets'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'equipment_asset_ids', 
      'foreignField': 'id', 
      'as': 'equipment'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'category_ids', 
      'foreignField': 'id', 
      'as': 'categories'
    }
  }, {
    '$addFields': {
      'quantities_processed': {
        '$arrayToObject': {
          '$map': {
            'input': '$quantities', 
            'as': 'quant', 
            'in': {
              'k': '$$quant.attributes.label', 
              'v': '$$quant.attributes.value.decimal'
            }
          }
        }
      }, 
      'categories_amount': {
        '$size': '$categories'
      }, 
      'farm_property_land_asset': {
        '$max': {
          '$max': '$land_assets.relationships.parent.data.id'
        }
      }
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'farm_property_land_asset', 
      'foreignField': 'id', 
      'as': 'farm_property_land_asset'
    }
  }, {
    '$project': {
      'field_id': {
        '$max': '$land_assets.attributes.pasa_field_id'
      }, 
      'field_name': {
        '$max': '$land_assets.attributes.name'
      }, 
      'farm_name': {
        '$max': '$farm_property_land_asset.attributes.name'
      }, 
      'survey_year': 1, 
      'applied_date': '$attributes.timestamp', 
      'area_percent': {
        '$toDouble': {
          '$max': '$quantities_processed.Area affected'
        }
      }, 
      'input_type': {
        '$max': '$categories.attributes.name'
      }, 
      'quantity_lbs': {
        '$toDouble': '$quantities_processed.Mass'
      }, 
      'field_name': {
        '$max': '$land_assets.attributes.name'
      }, 
      'notes': '$attributes.notes.value', 
      'farmDomain': 1, 
      'input_log_name': '$attributes.name'
    }
  }
];
