exports.aggregation = [
  {
    '$group': {
      '_id': '$farmDomain', 
      'entities': {
        '$addToSet': '$$ROOT'
      }
    }
  }, {
    '$project': {
      'farm_organization': {
        '$filter': {
          'input': '$entities', 
          'as': 'ent', 
          'cond': {
            '$eq': [
              '$$ent.type', 'organization--farm'
            ]
          }
        }
      }, 
      'land_asset': {
        '$filter': {
          'input': '$entities', 
          'as': 'ent', 
          'cond': {
            '$eq': [
              '$$ent.type', 'asset--land'
            ]
          }
        }
      }, 
      'farmDomain': '$_id'
    }
  }, {
    '$addFields': {
      'farm_id': {
        '$let': {
          'vars': {
            'org': {
              '$max': [
                '$farm_organization'
              ]
            }
          }, 
          'in': '$$org.attributes.pasa_farm_id'
        }
      }, 
      'farm_organization_entity_uid': {
        '$let': {
          'vars': {
            'org': {
              '$max': [
                '$farm_organization'
              ]
            }
          }, 
          'in': '$$org.id'
        }
      }, 
      'property_entity': {
        '$let': {
          'vars': {
            'property': {
              '$filter': {
                'input': '$land_asset', 
                'as': 'land', 
                'cond': {
                  '$eq': [
                    '$$land.attributes.land_type', 'property'
                  ]
                }
              }
            }
          }, 
          'in': '$$property'
        }
      }, 
      'property_entity_uid': {
        '$let': {
          'vars': {
            'property': {
              '$max': {
                '$filter': {
                  'input': '$land_asset', 
                  'as': 'land', 
                  'cond': {
                    '$eq': [
                      '$$land.attributes.land_type', 'property'
                    ]
                  }
                }
              }
            }
          }, 
          'in': '$$property.id'
        }
      }
    }
  }, {
    '$addFields': {
      'farm_organization': {
        '$max': '$farm_organization'
      }, 
      'amount_farm_organization': {
        '$size': '$farm_organization'
      }, 
      'property_entity': {
        '$max': '$property_entity'
      }, 
      'amount_property_entity': {
        '$size': '$property_entity'
      }
    }
  }
];
