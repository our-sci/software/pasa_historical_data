exports.aggregation = [
  {
    '$match': {
      'type': 'asset--plant'
    }
  }, {
    '$addFields': {
      'field': {
        '$let': {
          'vars': {
            'field': {
              '$max': '$relationships.location.data'
            }
          }, 
          'in': '$$field.id'
        }
      }, 
      'season': {
        '$let': {
          'vars': {
            'season': {
              '$max': '$relationships.season.data'
            }
          }, 
          'in': '$$season.id'
        }
      }, 
      'species': '$relationships.plant_type.data.id', 
      'plant_asset_status': '$attributes.status', 
      'plant_asset_id': '$attributes.id'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'field', 
      'foreignField': 'id', 
      'as': 'field'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'field.relationships.parent.data.id', 
      'foreignField': 'id', 
      'as': 'farm_property'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'season', 
      'foreignField': 'id', 
      'as': 'season'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'let': {
        'plantingId': '$id', 
        'currentFarmDomain': '$farmDomain'
      }, 
      'pipeline': [
        {
          '$match': {
            'type': 'log--seeding', 
            '$expr': [
              {
                '$and': [
                  {
                    '$eq': [
                      '$type', 'log--seeding'
                    ]
                  }, {
                    '$eq': [
                      '$farmDomain', '$$currentFarmDomain'
                    ]
                  }
                ]
              }
            ]
          }
        }, {
          '$addFields': {
            'asset_id': {
              '$max': '$relationships.asset.data.id'
            }, 
            'belongs': {
              '$eq': [
                {
                  '$max': '$relationships.asset.data.id'
                }, '$$plantingId'
              ]
            }
          }
        }, {
          '$match': {
            'belongs': true
          }
        }, {
          '$sort': {
            'attributes.name': 1
          }
        }
      ], 
      'as': 'seeding_log'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'let': {
        'plantingId': '$id', 
        'currentFarmDomain': '$farmDomain'
      }, 
      'pipeline': [
        {
          '$match': {
            'type': 'log--harvest', 
            '$expr': [
              {
                '$and': [
                  {
                    '$eq': [
                      '$type', 'log--harvest'
                    ]
                  }, {
                    '$eq': [
                      '$farmDomain', '$$currentFarmDomain'
                    ]
                  }
                ]
              }
            ]
          }
        }, {
          '$addFields': {
            'asset_id': {
              '$max': '$relationships.asset.data.id'
            }, 
            'belongs': {
              '$eq': [
                {
                  '$max': '$relationships.asset.data.id'
                }, '$$plantingId'
              ]
            }
          }
        }, {
          '$match': {
            'belongs': true
          }
        }, {
          '$sort': {
            'attributes.name': 1
          }
        }, {
          '$sort': {
            'attributes.name': 1
          }
        }
      ], 
      'as': 'harvest_log'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'let': {
        'plantingId': '$id', 
        'currentFarmDomain': '$farmDomain'
      }, 
      'pipeline': [
        {
          '$match': {
            'type': 'log--activity', 
            '$expr': [
              {
                '$and': [
                  {
                    '$eq': [
                      '$type', 'log--activity'
                    ]
                  }, {
                    '$eq': [
                      '$farmDomain', '$$currentFarmDomain'
                    ]
                  }, {
                    '$eq': [
                      '$id', '$$plantingId'
                    ]
                  }, {
                    '$eq': [
                      '$attributes.name', 'termination'
                    ]
                  }
                ]
              }
            ]
          }
        }, {
          '$addFields': {
            'asset_id': {
              '$max': '$relationships.asset.data.id'
            }, 
            'belongs': {
              '$eq': [
                {
                  '$max': '$relationships.asset.data.id'
                }, '$$plantingId'
              ]
            }
          }
        }, {
          '$match': {
            'belongs': true
          }
        }, {
          '$sort': {
            'attributes.name': 1
          }
        }
      ], 
      'as': 'termination_activity_log'
    }
  }, {
    '$addFields': {
      'finalizationLog': {
        '$concatArrays': [
          '$termination_activity_log', '$harvest_log'
        ]
      }, 
      'seeding_log': '$seeding_log'
    }
  }, {
    '$unwind': {
      'path': '$finalizationLog', 
      'includeArrayIndex': 'finalization_index'
    }
  }, {
    '$addFields': {
      'seeding_log': {
        '$arrayElemAt': [
          '$seeding_log', '$finalization_index'
        ]
      }
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'let': {
        'currentFarmDomain': '$farmDomain'
      }, 
      'pipeline': [
        {
          '$addFields': {
            'matches': {
              '$eq': [
                '$$currentFarmDomain', '$farmDomain'
              ]
            }
          }
        }, {
          '$match': {
            'type': 'taxonomy_term--plant_type', 
            'attributes.name': 'pasa_species', 
            'matches': true
          }
        }, {
          '$sort': {
            'attributes.drupal_internal_tid': -1
          }
        }
      ], 
      'as': 'pasa_species_tax'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'let': {
        'currentFarmDomain': '$farmDomain'
      }, 
      'pipeline': [
        {
          '$addFields': {
            'matches': {
              '$eq': [
                '$$currentFarmDomain', '$farmDomain'
              ]
            }
          }
        }, {
          '$match': {
            'type': 'taxonomy_term--plant_type', 
            'attributes.name': 'farmer_species', 
            'matches': true
          }
        }
      ], 
      'as': 'farmer_species_tax'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'let': {
        'currentFarmDomain': '$farmDomain'
      }, 
      'pipeline': [
        {
          '$addFields': {
            'matches': {
              '$eq': [
                '$$currentFarmDomain', '$farmDomain'
              ]
            }
          }
        }, {
          '$match': {
            'type': 'taxonomy_term--plant_type', 
            'attributes.name': 'pasa_crop_type', 
            'matches': true
          }
        }
      ], 
      'as': 'crop_type_tax'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'let': {
        'speciesArray': '$species', 
        'currentFarmDomain': '$farmDomain', 
        'pasa_species_id': {
          '$map': {
            'input': '$pasa_species_tax', 
            'as': 'entry', 
            'in': '$$entry.id'
          }
        }, 
        'farmer_species_id': {
          '$map': {
            'input': '$farmer_species_tax', 
            'as': 'entry', 
            'in': '$$entry.id'
          }
        }, 
        'crop_type_id': {
          '$map': {
            'input': '$crop_type_tax', 
            'as': 'entry', 
            'in': '$$entry.id'
          }
        }
      }, 
      'pipeline': [
        {
          '$addFields': {
            'matches': {
              '$eq': [
                '$$currentFarmDomain', '$farmDomain'
              ]
            }, 
            'is_pasa_species': {
              '$in': [
                {
                  '$max': '$relationships.parent.data.id'
                }, '$$pasa_species_id'
              ]
            }, 
            'is_farmer_species': {
              '$in': [
                {
                  '$max': '$relationships.parent.data.id'
                }, '$$farmer_species_id'
              ]
            }, 
            'is_crop_type': {
              '$in': [
                {
                  '$max': '$relationships.parent.data.id'
                }, '$$crop_type_id'
              ]
            }, 
            'belongs': {
              '$in': [
                '$id', '$$speciesArray'
              ]
            }
          }
        }, {
          '$match': {
            'matches': true, 
            'belongs': true, 
            'type': 'taxonomy_term--plant_type'
          }
        }
      ], 
      'as': 'species'
    }
  }, {
    '$addFields': {
      'pasa_species': {
        '$filter': {
          'input': '$species', 
          'as': 'entry', 
          'cond': '$$entry.is_pasa_species'
        }
      }, 
      'farmer_species': {
        '$max': {
          '$filter': {
            'input': '$species', 
            'as': 'entry', 
            'cond': '$$entry.is_farmer_species'
          }
        }
      }, 
      'crop_type': {
        '$filter': {
          'input': '$species', 
          'as': 'entry', 
          'cond': '$$entry.is_crop_type'
        }
      }, 
      'other_species': {
        '$max': {
          '$filter': {
            'input': '$species', 
            'as': 'entry', 
            'cond': {
              '$and': [
                {
                  '$not': '$$entry.is_farmer_species'
                }, {
                  '$not': '$$entry.is_pasa_species'
                }
              ]
            }
          }
        }
      }
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'seeding_log.relationships.quantity.data.id', 
      'foreignField': 'id', 
      'as': 'seeding_log_quantities'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'finalizationLog.relationships.quantity.data.id', 
      'foreignField': 'id', 
      'as': 'finalization_log_quantities'
    }
  }, {
    '$addFields': {
      'finalization_quantities_processed': {
        '$arrayToObject': {
          '$reduce': {
            'input': {
              '$map': {
                'input': '$finalization_log_quantities', 
                'as': 'quant', 
                'in': [
                  {
                    'k': '$$quant.attributes.label', 
                    'v': '$$quant.attributes.value.decimal'
                  }, {
                    'k': {
                      '$concat': [
                        '$$quant.attributes.label', '_unit_id'
                      ]
                    }, 
                    'v': {
                      '$max': '$$quant.relationships.units.data.id'
                    }
                  }
                ]
              }
            }, 
            'initialValue': [], 
            'in': {
              '$concatArrays': [
                '$$value', '$$this'
              ]
            }
          }
        }
      }, 
      'seeding_quantities_processed': {
        '$arrayToObject': {
          '$reduce': {
            'input': {
              '$map': {
                'input': '$seeding_log_quantities', 
                'as': 'quant', 
                'in': [
                  {
                    'k': '$$quant.attributes.label', 
                    'v': '$$quant.attributes.value.decimal'
                  }, {
                    'k': {
                      '$concat': [
                        '$$quant.attributes.label', '_unit_id'
                      ]
                    }, 
                    'v': {
                      '$max': '$$quant.relationships.units.data.id'
                    }
                  }
                ]
              }
            }, 
            'initialValue': [], 
            'in': {
              '$concatArrays': [
                '$$value', '$$this'
              ]
            }
          }
        }
      }
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'finalization_quantities_processed.yield_unit_id', 
      'foreignField': 'id', 
      'as': 'yield_unit'
    }
  }, {
    '$lookup': {
      'from': 'raw_entities', 
      'localField': 'seeding_quantities_processed.area percent_unit_id', 
      'foreignField': 'id', 
      'as': 'area_unit'
    }
  }, {
    '$addFields': {
      'yield_units': {
        '$max': '$yield_unit.attributes.name'
      }, 
      'area_unit': {
        '$max': '$area_unit.attributes.name'
      }, 
      'harvest_log_name': {
        '$arrayElemAt': [
          '$harvest_log.attributes.name', '$finalization_index'
        ]
      }, 
      'seeding_log_name': '$seeding_log.attributes.name', 
      'termination_log_name': {
        '$arrayElemAt': [
          '$termination_activity_log.attributes.name', '$finalization_index'
        ]
      }
    }
  }, {
    '$addFields': {
      'finalization_log_merger': {
        '$cond': {
          'if': {
            '$eq': [
              '$termination_log_name', '$finalizationLog.attributes.name'
            ]
          }, 
          'then': 'termination_log', 
          'else': 'harvest_log'
        }
      }
    }
  }, {
    '$addFields': {
      'term_date': {
        '$cond': {
          'if': {
            '$eq': [
              '$finalization_log_merger', 'termination_log'
            ]
          }, 
          'then': {
            '$max': '$finalizationLog.attributes.timestamp'
          }, 
          'else': null
        }
      }, 
      'termination_log_name': {
        '$cond': {
          'if': {
            '$eq': [
              '$finalization_log_merger', 'termination_log'
            ]
          }, 
          'then': {
            '$max': '$finalizationLog.attributes.name'
          }, 
          'else': null
        }
      }, 
      'harvest_date': {
        '$cond': {
          'if': {
            '$eq': [
              '$finalization_log_merger', 'harvest_log'
            ]
          }, 
          'then': {
            '$max': '$finalizationLog.attributes.timestamp'
          }, 
          'else': null
        }
      }, 
      'harvest_log_name': {
        '$cond': {
          'if': {
            '$eq': [
              '$finalization_log_merger', 'harvest_log'
            ]
          }, 
          'then': {
            '$max': '$finalizationLog.attributes.name'
          }, 
          'else': null
        }
      }
    }
  }, {
    '$project': {
      'farm_name': {
        '$max': '$farm_property.attributes.name'
      }, 
      'field_id': {
        '$max': '$field.attributes.pasa_field_id'
      }, 
      'field_name': {
        '$max': '$field.attributes.name'
      }, 
      'plantingName': '$attributes.name', 
      'season': {
        '$max': '$season.attributes.name'
      }, 
      'planting_date': '$seeding_log.attributes.timestamp', 
      'term_date': 1, 
      'harvest_date': 1, 
      'crop_type': '$crop_type.attributes.name', 
      'crop_name': '$pasa_species.attributes.name', 
      'farmer_crop_name': '$farmer_species.attributes.name', 
      'mixture_id': 1, 
      'area_percent': {
        '$toDouble': '$seeding_quantities_processed.area percent'
      }, 
      'area_units': '%', 
      'bed_width_ft': {
        '$toDouble': '$bed_width.attributes.value.decimal'
      }, 
      'yield': {
        '$toDouble': '$finalization_quantities_processed.yield'
      }, 
      'yield_units': 1, 
      'farmer_notes': 1, 
      'research_notes': 1, 
      'farmDomain': 1, 
      'status': '$plant_asset_status', 
      'plant_asset_status': '$plant_asset_status', 
      'harvest_log_name': 1, 
      'termination_log_name': 1, 
      'seeding_log_name': 1
    }
  }
];
