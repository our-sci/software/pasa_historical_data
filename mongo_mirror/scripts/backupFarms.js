const { Mirror } = require("mirror_farmos");
const { MongoClient } = require("mongodb");
const papa = require("papaparse");

require('dotenv').config({ path: '../../.env' });
let mongoURI = "mongodb://127.0.0.1:27017";
let farmosAPIKey = process.env.FARMOS_KEY;

// database and collection names
let targetDB = 'pasa_backup';
let rawEntities = 'raw_entities';
let simplifiedEntities = "simplified_entities";

let aggregatorURL = "https://surveystack.farmos.group/api/v2";

let aggregationsArray = [
    // simnplify the already dereferenced biodiversity aggregation entities into flat data structures, purged of unneeded fields.
];

function declareBackupMirror( { mongoURI, aggregatorKey } ) {
    let mirror = new Mirror({
        name: "pasa_backup",
        mongoURI:mongoURI,
        aggregatorKey:aggregatorKey,
        aggregatorURL: aggregatorURL,
        databaseName:targetDB,
        rawEntitiesCollection: rawEntities,
        simplifiedEntitiesCollection:simplifiedEntities,
        farmOperationDetailsColletion:'farm_details',
        aggregations: aggregationsArray,
        sourcesToClone: undefined,
        // we defined simplifiers for all the new task related entities, otherwise, it is the standard set.
        // simplifiers: rfmSimplifiers
    });
    return mirror;
};
;

let targetFarms = [
    'cedarmeadowfarm.farmos.net',
    'libertygardens.farmos.net',
    'newmorningfarm.farmos.net',
    'patchworkfarm.farmos.net',
    'beechgrovefarm.farmos.net',
    'willowrunfarmstead.farmos.net',
    'bucknelluniversity.farmos.net'
];

let backupMirror = declareBackupMirror( {mongoURI: mongoURI, aggregatorKey: farmosAPIKey} );


// Store entities into mirror
let operations = [];
targetFarms.forEach( async farmURL => {
    let process = await backupMirror.retrieveFarmEntities({farmDomain:farmURL, initialize:true});
    operations.push(process);
} );



// Retrieve Entities
async function getAllLocalEntities( mongoKey, db, coll ) {
    let output = {};
    const client = new MongoClient(mongoKey, { useUnifiedTopology: true });
    try {
        await client.connect();
        output = await client
            .db(db)
            .collection(coll)
            .find()
            .toArray()
        ;
    } catch(e) {
        output.error = e;
    } finally {
        await client.close();
        return output
        ;
    }
    ;
};

let backup = getAllLocalEntities( mongoURI, targetDB, "raw_entities" );


backup.then( data => {
    fs.writeFileSync(`${__dirname}/../../raw_data/deleted_farms_backup.json`, JSON.stringify( data ));

    let farmsInformation = targetFarms.map( farmName => {
        let farmData = data.filter( d => d.farmDomain == farmName );
        let output = {
            farmDomain: farmName,
            entitiesAmount: farmData.length,
            amountForEachType: {}
        };
        let entityTypes = Array.from( new Set( farmData.map( d => d.type ) ) ).sort();
        entityTypes.forEach( typeName => {
            output.amountForEachType[typeName] = farmData.filter( d => d.type == typeName ).length;
        } );
        return output;
    } );
    fs.writeFileSync(`${__dirname}/../../raw_data/deleted_farms_backup_description.json`, JSON.stringify( farmsInformation ));

    let flatCSV = targetFarms.flatMap( farmName => {
        let farmData = data.filter( d => d.farmDomain == farmName );
        let output = [];
        let entityTypes = Array.from( new Set( farmData.map( d => d.type ) ) ).sort();
        entityTypes.forEach( typeName => {
            output.push( { farm: farmName, type: typeName, amount: farmData.filter( d => d.type == typeName ).length } );
        } );

        return output;
    } );
    fs.writeFileSync(`${__dirname}/../../raw_data/deleted_farms_backup_description.csv`, papa.unparse( flatCSV ));
} );


