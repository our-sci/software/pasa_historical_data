const { MongoClient } = require("mongodb");
const asyncHandler = require('express-async-handler');
const { declareSHBSMirror } = require('./mirror_declaration.js');

// database and collection names
let targetDB = 'pasa_shbs';

async function processStagingFarms(farmDomainsArray, mirror) {
    let output;
    console.log(`begginging staging operation at ${new Date}`);
    const client = new MongoClient(mongoURI);
    try {
        await client.connect();
        // let dropOld = await client
        //     .db(targetDB)
        //     .dropDatabase()
        // ;
        console.log("about to mirror");
        console.log(farmDomainsArray);
        let mirroringOperations = farmDomainsArray.map( farm => {
            console.log(`mirroring ${farm}`);
            let mirroringOp = mirror.mirrorFarm({ farmDomain:farm });
            return mirroringOp;
        } );

        let results = await Promise.all(mirroringOperations);
        output = results;
    } catch(e) {
        output = { error: e };
    } finally {
        await client.close();
        console.log(`Finished staging operation at ${new Date}.`);
    }
    return output;
};


async function checkApiKey({ key, client, db=targetDB, collection="keys" }) {
    let output = {
        errors: []
    };
    try {
        let keyEntry = await client
            .db(db)
            .collection(collection)
            .findOne( { "hash": key } )
        ;
        if (keyEntry) {
            delete keyEntry.hash;
            output.valid = true;
        } else {
            output.valid = false;
        };
        output.key = keyEntry;
    } catch(e) {
        output.errors.push(e);
        output.valid = false;
    } finally {
        if (!output.valid) {            
            output.errors.push("key wasn't found in register");
        };
        return output;
    };
};

function addEndpoints( { expressApp, mongoURI, farmosKey, rootPath, keysCollection="keys", db=targetDB } ) {

    const shbsMirror = declareSHBSMirror( { mongoURI: mongoURI, aggregatorKey: farmosKey } );

    // refresh a farm (retrieve all new data, simplify, aggregate)
    expressApp.post( rootPath.concat( "update_farm" ), asyncHandler( async (req,res) => {
        const { farmDomain, key } = req.body;

        let output = {
            errors: []
        };
        const client = new MongoClient(mongoURI);

        try {
            let validKey = await checkApiKey({key:key, client: client, collection:keysCollection});
            if (!validKey.valid) {
                output.errors.push( { error: "Invalid API key" } );
                output.valid = false;
            } else if (validKey.valid) {
                let operation = await shbsMirror.mirrorFarm({ farmDomain:farmDomain });
                output.status = operation;
                output.valid = true;
            };
        } catch(e)  {
            output.errors.push(e);
        } finally {
            await client.close();
            res.json(output);
        };
    } ) )
    ;
    // retrieve biodiversity logs for a farm
    expressApp.post( rootPath.concat( "farm_biodiversity_logs" ), asyncHandler( async (req,res) => {
        const { farmDomain, key } = req.body;

        let output = {
            errors: []
        };
        const client = new MongoClient(mongoURI);

        try {
            let validKey = await checkApiKey({key:key, client: client, collection:keysCollection});
            if (!validKey.valid) {
                output.errors.push( { error: "Invalid API key" } );
                output.valid = false;
            } else if (validKey.valid) {
                let farmData = await retrieveBiodiversityLogs( {farmDomain: farmDomain, mongoURI: mongoURI } );
                output.logs = farmData;
                output.valid = true;
            };
        } catch(e)  {
            output.errors.push(e);
        } finally {
            await client.close();
            res.json(output);
        };
    } ) )
    ;
    // retrieve task lists for a farm
    expressApp.post( rootPath.concat( "simple_farm_checklist" ), asyncHandler( async (req,res) => {
        const { farmDomain, key } = req.body;

        let output = {
            errors: []
        };
        const client = new MongoClient(mongoURI);

        try {
            let validKey = await checkApiKey({key:key, client: client, collection:keysCollection});
            if (!validKey.valid) {
                output.errors.push( { error: "Invalid API key" } );
                output.valid = false;
            } else if (validKey.valid) {
                let farmData = await retrieveTaskLists( {farmDomain: farmDomain, mongoURI: mongoURI } );
                output.logs = farmData;
                output.valid = true;
            };
        } catch(e)  {
            output.errors.push(e);
        } finally {
            await client.close();
            res.json(output);
        };
    } ) )
    ;

};

exports.addEndpoints = addEndpoints;
exports.processStagingFarms = processStagingFarms;
exports.checkApiKey = checkApiKey;
