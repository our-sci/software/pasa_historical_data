const { MongoClient } = require("mongodb");
const { biodiversityMirror } = require('./mirror_declaration.js');

async function processFarmBatch({ farmDomainsArray, mirror, mongoURI }) {
    let output;
    const client = new MongoClient(mongoURI);
    try {
        await client.connect();
        let results = [];
        let index = 0;
        while (index < farmDomainsArray.length) {
            let farm = farmDomainsArray[index];
            console.log(`mirroring ${farm}`);
            let mirroringOp = await mirror.mirrorFarm({ farmDomain:farm });
            results.push( mirroringOp );
            index++;
        }
        ;
        output = results;
    } catch(e) {
        output = { error: e };
    } finally {
        await client.close();
        console.log(`Finished staging operation at ${new Date}.`);
    }
    return output;
};

exports.processFarmBatch = processFarmBatch;

