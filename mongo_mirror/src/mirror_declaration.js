const { Mirror } = require("mirror_farmos");
const farmsTable = require("../scripts/aggregations/farms_table.js");
const fieldsTable = require("../scripts/aggregations/fields_table.js");
const tillageTable = require("../scripts/aggregations/tillage_recovery_aggregation.js");

// database and collection names
let targetDB = 'pasa_shbs';
let rawEntities = 'raw_entities';
let simplifiedEntities = "simplified_entities";

let aggregatorURL = "https://surveystack.farmos.group/api/v2";

let aggregationsArray = [
    {
        name: "farms_table",
        aggregationPipeline: farmsTable.aggregation,
        sourceCollection:"raw_entities",
        order:0,
        index:"id",
    },
    {
        name: "fields_table",
        aggregationPipeline: fieldsTable.aggregation,
        sourceCollection:"raw_entities",
        order:1,
        index:"id",
    }
];

function declareSHBSMirror( { mongoURI, aggregatorKey, aggregations = aggregationsArray } ) {
    let mirror = new Mirror({
        name: "pasa_shbs",
        mongoURI:mongoURI,
        aggregatorKey:aggregatorKey,
        aggregatorURL: aggregatorURL,
        databaseName:targetDB,
        rawEntitiesCollection: rawEntities,
        simplifiedEntitiesCollection:simplifiedEntities,
        farmOperationDetailsColletion:'farm_details',
        aggregations: aggregations,
        sourcesToClone: undefined,
        // we defined simplifiers for all the new task related entities, otherwise, it is the standard set.
        // simplifiers: rfmSimplifiers
    });
    mirror.defaultEntityTypes.push( "organization--farm" );
    return mirror;
};
;

exports.declareSHBSMirror = declareSHBSMirror;
