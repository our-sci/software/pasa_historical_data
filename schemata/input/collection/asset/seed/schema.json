{
 "$id": "https://ourscitest.farmos.net/api/asset/seed/resource/schema?farm_id=1",
 "title": "Seed asset",
 "type": "object",
 "properties": {
  "id": {
   "type": "string",
   "format": "uuid"
  },
  "type": {
   "const": "asset--seed"
  },
  "meta": {
   "type": "object"
  },
  "attributes": {
   "description": "Entity attributes",
   "type": "object",
   "properties": {
    "name": {
     "type": "string",
     "title": "Name",
     "maxLength": 255,
     "description": "The name of the asset."
    },
    "status": {
     "type": "string",
     "title": "Status",
     "maxLength": 255,
     "oneOf": [
      {
       "const": "active",
       "title": "Active"
      },
      {
       "const": "archived",
       "title": "Archived"
      }
     ],
     "description": "Indicates the status of the asset.",
     "default": "active"
    },
    "archived": {
     "type": "string",
     "title": "Timestamp",
     "format": "date-time",
     "description": "The time the asset was archived."
    },
    "data": {
     "type": "string",
     "title": "Data"
    },
    "notes": {
     "type": "object",
     "properties": {
      "value": {
       "type": "string",
       "title": "Text"
      },
      "format": {
       "type": "string",
       "title": "Text format"
      }
     },
     "required": [
      "value"
     ],
     "title": "Notes"
    },
    "flag": {
     "type": "array",
     "title": "Flags",
     "description": "Add flags to enable better sorting and filtering of records.",
     "items": {
      "type": "string",
      "title": "Text value",
      "anyOf": [
       {
        "const": "biodynamic",
        "title": "Biodynamic"
       },
       {
        "const": "greenhouse",
        "title": "Greenhouse"
       },
       {
        "const": "hydroponic",
        "title": "Hydroponic"
       },
       {
        "const": "monitor",
        "title": "Monitor"
       },
       {
        "const": "non_gmo",
        "title": "Non-GMO"
       },
       {
        "const": "notorganic",
        "title": "Not Organic"
       },
       {
        "const": "organic",
        "title": "Organic"
       },
       {
        "const": "organic_not_cert",
        "title": "Organic (Not Certified)"
       },
       {
        "const": "priority",
        "title": "Priority"
       },
       {
        "const": "regenerative",
        "title": "Regenerative"
       },
       {
        "const": "review",
        "title": "Needs review"
       },
       {
        "const": "transitionalorganic",
        "title": "Transitional Organic"
       },
       {
        "const": "transitionalregen",
        "title": "Transitional to regenerative"
       }
      ]
     }
    },
    "id_tag": {
     "type": "array",
     "title": "ID tags",
     "description": "List any identification tags that this asset has. Use the fields below to describe the type, location, and ID of each.",
     "items": {
      "type": "object",
      "properties": {
       "id": {
        "type": "string",
        "title": "ID of the tag"
       },
       "type": {
        "type": "string",
        "title": "Type of the tag"
       },
       "location": {
        "type": "string",
        "title": "Location of the tag"
       }
      }
     }
    },
    "inventory": {
     "type": "array",
     "title": "Current inventory",
     "items": {
      "type": "object",
      "properties": {
       "measure": {
        "type": "string",
        "title": "Measure of the inventory"
       },
       "value": {
        "type": "string",
        "title": "Value of the inventory"
       },
       "units": {
        "type": "string",
        "title": "Units of the inventory"
       }
      }
     }
    },
    "geometry": {
     "type": "object",
     "properties": {
      "value": {
       "type": "string",
       "title": "Geometry"
      },
      "geo_type": {
       "type": "string",
       "title": "Geometry Type"
      },
      "lat": {
       "type": "number",
       "title": "Centroid Latitude"
      },
      "lon": {
       "type": "number",
       "title": "Centroid Longitude"
      },
      "left": {
       "type": "number",
       "title": "Left Bounding"
      },
      "top": {
       "type": "number",
       "title": "Top Bounding"
      },
      "right": {
       "type": "number",
       "title": "Right Bounding"
      },
      "bottom": {
       "type": "number",
       "title": "Bottom Bounding"
      },
      "geohash": {
       "type": "string",
       "title": "Geohash"
      },
      "latlon": {
       "type": "string",
       "title": "LatLong Pair"
      }
     },
     "title": "Current geometry"
    },
    "intrinsic_geometry": {
     "type": "object",
     "properties": {
      "value": {
       "type": "string",
       "title": "Geometry"
      },
      "geo_type": {
       "type": "string",
       "title": "Geometry Type"
      },
      "lat": {
       "type": "number",
       "title": "Centroid Latitude"
      },
      "lon": {
       "type": "number",
       "title": "Centroid Longitude"
      },
      "left": {
       "type": "number",
       "title": "Left Bounding"
      },
      "top": {
       "type": "number",
       "title": "Top Bounding"
      },
      "right": {
       "type": "number",
       "title": "Right Bounding"
      },
      "bottom": {
       "type": "number",
       "title": "Bottom Bounding"
      },
      "geohash": {
       "type": "string",
       "title": "Geohash"
      },
      "latlon": {
       "type": "string",
       "title": "LatLong Pair"
      }
     },
     "title": "Intrinsic geometry",
     "description": "Add geometry data to this asset to describe its intrinsic location. This will only be used if the asset is fixed."
    },
    "is_location": {
     "type": "boolean",
     "title": "Is location",
     "description": "If this asset is a location, then other assets can be moved to it."
    },
    "is_fixed": {
     "type": "boolean",
     "title": "Is fixed",
     "description": "If this asset is fixed, then it can have an intrinsic geometry. If the asset will move around, then it is not fixed and geometry will be determined by movement logs."
    },
    "surveystack_id": {
     "type": "string",
     "title": "Surveystack ID",
     "maxLength": 255
    },
    "quick": {
     "type": "array",
     "title": "Quick form",
     "description": "References the quick form that was used to create this record.",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    }
   },
   "required": [
    "name",
    "status"
   ],
   "additionalProperties": false
  },
  "relationships": {
   "description": "Entity relationships",
   "properties": {
    "termination": {
     "type": "object",
     "properties": {
      "data": {
       "title": "Terminated",
       "type": "object",
       "required": [
        "id",
        "type"
       ],
       "properties": {
        "id": {
         "type": "string",
         "title": "Resource ID",
         "format": "uuid",
         "maxLength": 128
        },
        "type": {
         "type": "string"
        }
       }
      }
     }
    },
    "convention": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Convention",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "file": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Files",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "image": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Images",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "farm": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Farm",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "group": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Group membership",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "location": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Current location",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "owner": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Owners",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "parent": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Parents",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "plant_type": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Crop/variety",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "season": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Season",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    }
   },
   "type": "object",
   "required": [
    "plant_type"
   ],
   "additionalProperties": false
  }
 },
 "required": [
  "id",
  "attributes"
 ]
}