{
 "$id": "https://ourscitest.farmos.net/api/profile/common/resource/schema?farm_id=1",
 "title": "Common profile",
 "type": "object",
 "properties": {
  "id": {
   "type": "string",
   "format": "uuid"
  },
  "type": {
   "const": "profile--common"
  },
  "meta": {
   "type": "object"
  },
  "attributes": {
   "description": "Entity attributes",
   "type": "object",
   "properties": {
    "revision_timestamp": {
     "type": "string",
     "title": "Revision create time",
     "format": "date-time",
     "description": "The time that the current revision was created."
    },
    "revision_log": {
     "type": "string",
     "title": "Revision log message",
     "description": "Briefly describe the changes you have made.",
     "default": ""
    },
    "title": {
     "type": "string",
     "title": "Title",
     "maxLength": 255,
     "description": "The title of the profile entity."
    },
    "status": {
     "type": "boolean",
     "title": "Status",
     "description": "A boolean indicating whether the profile is enabled.",
     "default": true
    },
    "surveystack_id": {
     "type": "string",
     "title": "Surveystack ID",
     "maxLength": 255
    },
    "animals_detail": {
     "type": "string",
     "title": "Total Number of Animals, by Type",
     "description": "How many of each type of animal do you have?\r\n\r\nkeys: animal species\r\nvalue: number count"
    },
    "animals_total": {
     "type": "integer",
     "title": "Total Number of Animals",
     "description": "Total animal count in your Farm Operation"
    },
    "area": {
     "type": "object",
     "properties": {
      "value": {
       "type": "string",
       "title": "Geometry"
      },
      "geo_type": {
       "type": "string",
       "title": "Geometry Type"
      },
      "lat": {
       "type": "number",
       "title": "Centroid Latitude"
      },
      "lon": {
       "type": "number",
       "title": "Centroid Longitude"
      },
      "left": {
       "type": "number",
       "title": "Left Bounding"
      },
      "top": {
       "type": "number",
       "title": "Top Bounding"
      },
      "right": {
       "type": "number",
       "title": "Right Bounding"
      },
      "bottom": {
       "type": "number",
       "title": "Bottom Bounding"
      },
      "geohash": {
       "type": "string",
       "title": "Geohash"
      },
      "latlon": {
       "type": "string",
       "title": "LatLong Pair"
      }
     },
     "title": "General Farm Outline",
     "description": "Draw a circle to encompass the land in your farm."
    },
    "area_community": {
     "type": "object",
     "properties": {
      "value": {
       "type": "string",
       "title": "Geometry"
      },
      "geo_type": {
       "type": "string",
       "title": "Geometry Type"
      },
      "lat": {
       "type": "number",
       "title": "Centroid Latitude"
      },
      "lon": {
       "type": "number",
       "title": "Centroid Longitude"
      },
      "left": {
       "type": "number",
       "title": "Left Bounding"
      },
      "top": {
       "type": "number",
       "title": "Top Bounding"
      },
      "right": {
       "type": "number",
       "title": "Right Bounding"
      },
      "bottom": {
       "type": "number",
       "title": "Bottom Bounding"
      },
      "geohash": {
       "type": "string",
       "title": "Geohash"
      },
      "latlon": {
       "type": "string",
       "title": "LatLong Pair"
      }
     },
     "title": "Producer Community Outline",
     "description": "Outline the area with producers like you: a similar bioregion, people who produce similar things, people you actively work/discuss/compete with, etc."
    },
    "area_total_hectares": {
     "type": "number",
     "title": "Farm Size, Owned and Rented Land",
     "description": "Total farm size including all owned and rented land, in hectares (2.47 acres per hectare)"
    },
    "average_annual_rainfall": {
     "type": "integer",
     "title": "Average Annual Rainfall",
     "description": "What is the average annual rainfall for your operation?\r\n\r\nStored in millimeters"
    },
    "average_annual_temperature": {
     "type": "number",
     "title": "Average Annual Temperature",
     "description": "What is the average annual temperature for your operation? (Stored in celsius)"
    },
    "bio": {
     "type": "string",
     "title": "Personal Bio or Description",
     "description": "Tell us a little about yourself and/or operation, such as why you farm, what your favorite thing about farming is, or your farming story/connection. (150 words maximum)"
    },
    "certifications_current": {
     "type": "string",
     "title": "Current Certifications",
     "description": "Do you have any currently active Certifications?"
    },
    "certifications_current_detail": {
     "type": "array",
     "title": "Current Certifications Detail",
     "description": "Select currently currently active certification(s).",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    },
    "certifications_future": {
     "type": "string",
     "title": "Future Certifications",
     "description": "Do you want any (other) certifications or plan to have them in place in the future?"
    },
    "certifications_future_detail": {
     "type": "array",
     "title": "Future Certifications Detail",
     "description": "Select future certification(s)",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    },
    "climate_zone": {
     "type": "string",
     "title": "Climate Zone",
     "description": "Koppen climate zones. These will be used to help connect farmers to others operating in similar climate zones.\r\n\r\nhttps://en.wikipedia.org/wiki/K%C3%B6ppen_climate_classification"
    },
    "conditions_detail": {
     "type": "string",
     "title": "Conditions Detail",
     "description": "Additional climate, soil and environmental conditions details around the general farm area."
    },
    "county": {
     "type": "string",
     "title": "County",
     "maxLength": 255,
     "description": "County is useful to anonymize your location"
    },
    "email": {
     "type": "string",
     "title": "Contact Email Address",
     "format": "email",
     "description": "Email address for Farm Organization (farm owner, manager, etc.)"
    },
    "equity_practices": {
     "type": "array",
     "title": "Equity Practices",
     "description": "Include any equity practices the operation uses.",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    },
    "farm_leadership_experience": {
     "type": "integer",
     "title": "Experience of Farm Leadership",
     "description": "How much experience does the farm leadership have in years?"
    },
    "flexible": {
     "type": "string",
     "title": "Flexible",
     "description": "A  JSON structure for any other data that needs to be passed"
    },
    "goal_1": {
     "type": "string",
     "title": "Most Important Goal",
     "description": "Select the most important monitoring goal in your Farm Operation"
    },
    "goal_2": {
     "type": "string",
     "title": "2nd Most Important Goal",
     "description": "Select the 2nd most important monitoring goal in your Farm Operation"
    },
    "goal_3": {
     "type": "string",
     "title": "3rd Most Important Goal",
     "description": "Select the 3rd most important monitoring goal in your Farm Operation"
    },
    "hardiness_zone": {
     "type": "string",
     "title": "Hardiness Zone",
     "description": "USDA Hardiness Zone"
    },
    "immediate_data_source": {
     "type": "string",
     "title": "Immediate Data Source",
     "maxLength": 255,
     "description": "The service or source that a specific instance of a farm profile has come from."
    },
    "indigenous_territory": {
     "type": "array",
     "title": "Indigenous Territory",
     "description": "Indigenous communities that have a relationship to the land",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    },
    "interest": {
     "type": "array",
     "title": "Interests and Opportunities",
     "description": "What interests you / what opportunities are you looking for?",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    },
    "land_other": {
     "type": "array",
     "title": "Other Land Information",
     "description": "Choose any of the following that apply to land used by your Farm Operation",
     "items": {
      "type": "string",
      "title": "Text value"
     }
    },
    "land_other_detail": {
     "type": "string",
     "title": "Other Land Information by percent",
     "description": "Percentage of the land that is rented, native title, cooperatively owned, in land trusts, etc"
    },
    "land_type_detail": {
     "type": "string",
     "title": "Land Use Detail, by Type",
     "description": "The breakdown of how your land is used.\r\n\r\nKeys include:\r\nagroforestry, berries, grains_other, native_habitat, orchard_vine, pasture, vegetables, orchard_vine, rangeland"
    },
    "location_address_line1": {
     "type": "string",
     "title": "Address (line 1)",
     "maxLength": 255,
     "description": "Farm Location: Address line 1"
    },
    "location_address_line2": {
     "type": "string",
     "title": "Address (line 2)",
     "maxLength": 255,
     "description": "Farm Location: Address line 2"
    },
    "location_administrative_area": {
     "type": "string",
     "title": "State / Province",
     "maxLength": 255,
     "description": "Farm Location: State / Province"
    },
    "location_country_code": {
     "type": "string",
     "title": "Country",
     "maxLength": 255,
     "description": "Farm Location: Country"
    },
    "location_locality": {
     "type": "string",
     "title": "City",
     "maxLength": 255,
     "description": "Farm Location: City"
    },
    "location_postal_code": {
     "type": "string",
     "title": "Postal Code",
     "maxLength": 255,
     "description": "Farm Location: Postal Code"
    },
    "management_plans_current": {
     "type": "string",
     "title": "Current Management Plans",
     "description": "Do you have any currently active Management Plans?"
    },
    "management_plans_current_detail": {
     "type": "array",
     "title": "Current Management Plans Detail",
     "description": "Select the currently active management plan(s)",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    },
    "management_plans_future": {
     "type": "string",
     "title": "Future Management Plans",
     "description": "Do you want any (other) management plans or plan to have them in place in the future?"
    },
    "management_plans_future_detail": {
     "type": "array",
     "title": "Future Management Plans Detail",
     "description": "Select future management plan(s)",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    },
    "motivations": {
     "type": "array",
     "title": "Motivations",
     "description": "What are your primary motivations for farming?",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    },
    "name": {
     "type": "string",
     "title": "Contact Name",
     "maxLength": 255,
     "description": "Name of the primary contact for the Farm Organization"
    },
    "organization": {
     "type": "string",
     "title": "Organization Name",
     "maxLength": 255,
     "description": "Name of the Farm or Farm Organization"
    },
    "organization_id": {
     "type": "string",
     "title": "Organization ID",
     "maxLength": 255,
     "description": "Organization ID  (e.g., Australia - ABN, US - EIN, European - VAT, International - DUNS, etc.)"
    },
    "phone": {
     "type": "string",
     "title": "Contact Phone",
     "maxLength": 255,
     "description": "Contact Phone Number for the Farm Organization (owner, manager, etc.)"
    },
    "preferred": {
     "type": "string",
     "title": "Preferred Contact Method",
     "description": "Preferred contact method (please select only 1)"
    },
    "products_animals": {
     "type": "array",
     "title": "Animal Types / Products",
     "description": "What types of animals do you have?",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    },
    "products_categories": {
     "type": "array",
     "title": "Land Use / Products",
     "description": "How is your land used / what products do you produce?",
     "items": {
      "type": "string",
      "title": "Text value"
     },
     "minItems": 1
    },
    "products_categories_other": {
     "type": "array",
     "title": "Other Land Use / Products",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    },
    "products_detail": {
     "type": "array",
     "title": "Products Detail",
     "description": "What do you produce?",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    },
    "products_value_added": {
     "type": "array",
     "title": "Products, Value Added",
     "description": "What value added products do you provide? Examples: Making yarn from wool; making cheese from milk; making bailed hay from hay-grass; processing cuts of meat",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    },
    "records_software": {
     "type": "array",
     "title": "Farm Management Software",
     "description": "Which farm management software do you use?",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    },
    "records_system": {
     "type": "array",
     "title": "Farm Records System",
     "description": "How do you keep farm management records?",
     "items": {
      "type": "string",
      "title": "Text value"
     },
     "minItems": 1
    },
    "role": {
     "type": "string",
     "title": "Role",
     "maxLength": 255,
     "description": " What best describes your role(s) in this farm organization? "
    },
    "schema_version": {
     "type": "string",
     "title": "Schema Version",
     "maxLength": 255,
     "description": "Not Displayed. What version of the farm profile schema that the data set is defined by."
    },
    "social": {
     "type": "string",
     "title": "Social Media Links",
     "maxLength": 255,
     "description": "Link(s) to your social media"
    },
    "types": {
     "type": "array",
     "title": "Type(s)",
     "description": "What type(s) of farm operation do you have?",
     "items": {
      "type": "string",
      "title": "Text value"
     },
     "minItems": 1
    },
    "unique_id": {
     "type": "string",
     "title": "Unique Id",
     "maxLength": 255,
     "description": "At least a UUID for the farm, not displayed."
    },
    "units": {
     "type": "string",
     "title": "Units Preference",
     "description": "Select Metric or Imperial"
    },
    "website": {
     "type": "string",
     "title": "Farm Website",
     "maxLength": 255,
     "description": "Website or link for the farm"
    }
   },
   "required": [
    "title",
    "animals_total",
    "area_total_hectares",
    "certifications_current",
    "management_plans_current",
    "name",
    "products_categories",
    "records_system",
    "types"
   ],
   "additionalProperties": false
  },
  "relationships": {
   "description": "Entity relationships",
   "properties": {
    "bundle": {
     "type": "object",
     "properties": {
      "data": {
       "title": "Profile type",
       "type": "object",
       "required": [
        "id",
        "type"
       ],
       "properties": {
        "id": {
         "type": "string",
         "title": "Resource ID",
         "format": "uuid",
         "maxLength": 128
        },
        "type": {
         "type": "string"
        }
       }
      }
     }
    },
    "revision_uid": {
     "type": "object",
     "properties": {
      "data": {
       "title": "Revision user",
       "type": "object",
       "required": [
        "id",
        "type"
       ],
       "properties": {
        "id": {
         "type": "string",
         "title": "Resource ID",
         "format": "uuid",
         "maxLength": 128
        },
        "type": {
         "type": "string"
        }
       }
      }
     }
    }
   },
   "type": "object",
   "additionalProperties": false
  }
 },
 "required": [
  "id",
  "attributes"
 ]
}