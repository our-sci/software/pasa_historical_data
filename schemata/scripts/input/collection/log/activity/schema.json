{
 "$id": "https://ourscitest.farmos.net/api/log/activity/resource/schema?farm_id=1",
 "title": "Activity log",
 "type": "object",
 "properties": {
  "id": {
   "type": "string",
   "format": "uuid"
  },
  "type": {
   "const": "log--activity"
  },
  "meta": {
   "type": "object"
  },
  "attributes": {
   "description": "Entity attributes",
   "type": "object",
   "properties": {
    "name": {
     "type": "string",
     "title": "Name",
     "maxLength": 255,
     "description": "The name of the log. Leave this blank to automatically generate a name.",
     "default": ""
    },
    "timestamp": {
     "type": "string",
     "title": "Timestamp",
     "format": "date-time",
     "description": "Timestamp of the event being logged."
    },
    "status": {
     "type": "string",
     "title": "Status",
     "maxLength": 255,
     "oneOf": [
      {
       "const": "done",
       "title": "Done"
      },
      {
       "const": "pending",
       "title": "Pending"
      }
     ],
     "description": "Indicates the status of the log.",
     "default": "pending"
    },
    "is_termination": {
     "type": "boolean",
     "title": "Is termination",
     "description": "If this log is a termination, then all assets referenced by it will be marked as terminated and archived at the time the log takes place. The log must be complete in order for the movement to take effect."
    },
    "data": {
     "type": "string",
     "title": "Data"
    },
    "notes": {
     "type": "object",
     "properties": {
      "value": {
       "type": "string",
       "title": "Text"
      },
      "format": {
       "type": "string",
       "title": "Text format"
      }
     },
     "required": [
      "value"
     ],
     "title": "Notes"
    },
    "flag": {
     "type": "array",
     "title": "Flags",
     "description": "Add flags to enable better sorting and filtering of records.",
     "items": {
      "type": "string",
      "title": "Text value",
      "anyOf": [
       {
        "const": "biodynamic",
        "title": "Biodynamic"
       },
       {
        "const": "greenhouse",
        "title": "Greenhouse"
       },
       {
        "const": "hydroponic",
        "title": "Hydroponic"
       },
       {
        "const": "monitor",
        "title": "Monitor"
       },
       {
        "const": "non_gmo",
        "title": "Non-GMO"
       },
       {
        "const": "notorganic",
        "title": "Not Organic"
       },
       {
        "const": "organic",
        "title": "Organic"
       },
       {
        "const": "organic_not_cert",
        "title": "Organic (Not Certified)"
       },
       {
        "const": "priority",
        "title": "Priority"
       },
       {
        "const": "regenerative",
        "title": "Regenerative"
       },
       {
        "const": "review",
        "title": "Needs review"
       },
       {
        "const": "transitionalorganic",
        "title": "Transitional Organic"
       },
       {
        "const": "transitionalregen",
        "title": "Transitional to regenerative"
       }
      ]
     }
    },
    "is_group_assignment": {
     "type": "boolean",
     "title": "Is group assignment",
     "description": "If this log is a group assignment, any referenced assets will become members of the groups referenced below."
    },
    "geometry": {
     "type": "object",
     "properties": {
      "value": {
       "type": "string",
       "title": "Geometry"
      },
      "geo_type": {
       "type": "string",
       "title": "Geometry Type"
      },
      "lat": {
       "type": "number",
       "title": "Centroid Latitude"
      },
      "lon": {
       "type": "number",
       "title": "Centroid Longitude"
      },
      "left": {
       "type": "number",
       "title": "Left Bounding"
      },
      "top": {
       "type": "number",
       "title": "Top Bounding"
      },
      "right": {
       "type": "number",
       "title": "Right Bounding"
      },
      "bottom": {
       "type": "number",
       "title": "Bottom Bounding"
      },
      "geohash": {
       "type": "string",
       "title": "Geohash"
      },
      "latlon": {
       "type": "string",
       "title": "LatLong Pair"
      }
     },
     "title": "Geometry",
     "description": "Add geometry data to this log to describe where it took place."
    },
    "is_movement": {
     "type": "boolean",
     "title": "Is movement",
     "description": "If this log is a movement, then all assets referenced by it will be located in the referenced locations and/or geometry at the time the log takes place. The log must be complete in order for the movement to take effect."
    },
    "surveystack_id": {
     "type": "string",
     "title": "Surveystack ID",
     "maxLength": 255
    },
    "quick": {
     "type": "array",
     "title": "Quick form",
     "description": "References the quick form that was used to create this record.",
     "items": {
      "type": "string",
      "title": "Text value",
      "maxLength": 255
     }
    }
   },
   "required": [
    "timestamp",
    "status"
   ],
   "additionalProperties": false
  },
  "relationships": {
   "description": "Entity relationships",
   "properties": {
    "convention": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Convention",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "file": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Files",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "image": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Images",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "group": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Groups",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "location": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Location",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "asset": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Assets",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "category": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Log category",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "quantity": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Quantity",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "owner": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Owners",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    },
    "equipment": {
     "type": "object",
     "properties": {
      "data": {
       "type": "array",
       "title": "Equipment used",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     }
    }
   },
   "type": "object",
   "additionalProperties": false
  }
 },
 "required": [
  "id",
  "attributes"
 ]
}