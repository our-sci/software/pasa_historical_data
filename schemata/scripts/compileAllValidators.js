// compiles static AJV validators for all schemas obtained from a farm.
const {compileConventionsValidator, compileGeneralValidator, storeDeDrupalizedSchemas} = require('convention_builder');
const fs = require('fs');

let farmosSchemata = JSON.parse( fs.readFileSync(`${ __dirname }/../input/farmos.json`) );


fs.mkdirSync( "output/validators", { recursive: true }, console.error );

// build general validator
compileGeneralValidator(farmosSchemata);
compileConventionsValidator();
